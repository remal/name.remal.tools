package name.remal.xml.spi;

import static com.google.common.collect.Lists.newArrayList;
import static name.remal.Services.loadServices;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import lombok.SneakyThrows;
import name.remal.gradle_plugins.api.AutoService;
import org.apache.xerces.jaxp.DocumentBuilderFactoryImpl;

@AutoService(DocumentBuilderFactory.class)
@SuppressFBWarnings("IICU_INCORRECT_INTERNAL_CLASS_USE")
public class ConfigurableDocumentBuilderFactory extends WrappedDocumentBuilderFactory {

    private static final List<DocumentBuilderFactoryConfigurer> DOCUMENT_BUILDER_FACTORY_CONFIGURERS = newArrayList(loadServices(DocumentBuilderFactoryConfigurer.class));

    @SneakyThrows
    public ConfigurableDocumentBuilderFactory() {
        super(new DocumentBuilderFactoryImpl());
        for (DocumentBuilderFactoryConfigurer configurer : DOCUMENT_BUILDER_FACTORY_CONFIGURERS) {
            configurer.configure(this);
        }
    }

    private static final List<DocumentBuilderConfigurer> DOCUMENT_BUILDER_CONFIGURERS = newArrayList(loadServices(DocumentBuilderConfigurer.class));

    @Override
    @SneakyThrows
    public DocumentBuilder newDocumentBuilder() throws ParserConfigurationException {
        DocumentBuilder documentBuilder = super.newDocumentBuilder();
        for (DocumentBuilderConfigurer configurer : DOCUMENT_BUILDER_CONFIGURERS) {
            configurer.configure(documentBuilder);
        }
        return documentBuilder;
    }

}
