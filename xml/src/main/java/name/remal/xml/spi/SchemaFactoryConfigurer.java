package name.remal.xml.spi;

import javax.xml.validation.SchemaFactory;
import org.jetbrains.annotations.NotNull;

public interface SchemaFactoryConfigurer {

    void configure(@NotNull SchemaFactory schemaFactory) throws Throwable;

}
