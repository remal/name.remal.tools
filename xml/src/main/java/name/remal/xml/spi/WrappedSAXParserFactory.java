package name.remal.xml.spi;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

class WrappedSAXParserFactory extends SAXParserFactory {

    @NotNull
    private final SAXParserFactory delegate;

    public WrappedSAXParserFactory(@NotNull SAXParserFactory delegate) {
        this.delegate = delegate;
    }

    @Override
    public SAXParser newSAXParser() throws ParserConfigurationException, SAXException {
        return delegate.newSAXParser();
    }

    @Override
    public void setNamespaceAware(boolean awareness) {
        delegate.setNamespaceAware(awareness);
    }

    @Override
    public void setValidating(boolean validating) {
        delegate.setValidating(validating);
    }

    @Override
    public boolean isNamespaceAware() {
        return delegate.isNamespaceAware();
    }

    @Override
    public boolean isValidating() {
        return delegate.isValidating();
    }

    @Override
    public void setFeature(String name, boolean value) throws ParserConfigurationException, SAXNotRecognizedException, SAXNotSupportedException {
        delegate.setFeature(name, value);
    }

    @Override
    public boolean getFeature(String name) throws ParserConfigurationException, SAXNotRecognizedException, SAXNotSupportedException {
        return delegate.getFeature(name);
    }

    @Override
    public Schema getSchema() {
        return delegate.getSchema();
    }

    @Override
    public void setSchema(Schema schema) {
        delegate.setSchema(schema);
    }

    @Override
    public void setXIncludeAware(boolean state) {
        delegate.setXIncludeAware(state);
    }

    @Override
    public boolean isXIncludeAware() {
        return delegate.isXIncludeAware();
    }

}
