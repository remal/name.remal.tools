package name.remal.xml.spi;

import static com.google.common.collect.Lists.newArrayList;
import static name.remal.Services.loadServices;

import java.util.List;
import javax.xml.xpath.XPathFactory;
import lombok.SneakyThrows;
import name.remal.gradle_plugins.api.AutoService;
import net.sf.saxon.xpath.XPathFactoryImpl;

@AutoService(XPathFactory.class)
public class ConfigurableXPathFactory extends WrappedXPathFactory {

    private static final List<XPathFactoryConfigurer> X_PATH_FACTORY_CONFIGURERS = newArrayList(loadServices(XPathFactoryConfigurer.class));

    @SneakyThrows
    public ConfigurableXPathFactory() {
        super(new XPathFactoryImpl());
        for (XPathFactoryConfigurer configurer : X_PATH_FACTORY_CONFIGURERS) {
            configurer.configure(this);
        }
    }

}
