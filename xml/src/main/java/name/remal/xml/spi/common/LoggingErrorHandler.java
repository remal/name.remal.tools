package name.remal.xml.spi.common;

import static name.remal.log.LogUtils.logError;
import static name.remal.log.LogUtils.logWarn;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import name.remal.gradle_plugins.api.AutoService;
import name.remal.xml.spi.DocumentBuilderConfigurer;
import name.remal.xml.spi.TransformerFactoryConfigurer;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class LoggingErrorHandler implements ErrorHandler, ErrorListener {

    private static final String loggerName = LoggingErrorHandler.class.getName();

    @Override
    public void warning(@NotNull TransformerException exception) throws TransformerException {
        logWarn(loggerName, exception);
    }

    @Override
    public void error(@NotNull TransformerException exception) throws TransformerException {
        logError(loggerName, exception);
    }

    @Override
    public void fatalError(TransformerException exception) throws TransformerException {
        throw exception;
    }

    @Override
    public void warning(@NotNull SAXParseException exception) throws SAXException {
        logWarn(loggerName, exception);
    }

    @Override
    public void error(@NotNull SAXParseException exception) throws SAXException {
        logError(loggerName, exception);
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        throw exception;
    }

    @AutoService({DocumentBuilderConfigurer.class, TransformerFactoryConfigurer.class})
    public static class Configurer implements DocumentBuilderConfigurer, TransformerFactoryConfigurer {

        private static final LoggingErrorHandler INSTANCE = new LoggingErrorHandler();

        @Override
        public int getOrder() {
            return Integer.MIN_VALUE;
        }

        @Override
        public void configure(@NotNull DocumentBuilder documentBuilder) throws Throwable {
            documentBuilder.setErrorHandler(INSTANCE);
        }

        @Override
        public void configure(@NotNull TransformerFactory transformerFactory) throws Throwable {
            transformerFactory.setErrorListener(INSTANCE);
        }
    }

}
