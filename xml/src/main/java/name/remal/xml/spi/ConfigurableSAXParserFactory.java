package name.remal.xml.spi;

import static com.google.common.collect.Lists.newArrayList;
import static name.remal.Services.loadServices;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.List;
import javax.xml.parsers.SAXParserFactory;
import lombok.SneakyThrows;
import name.remal.gradle_plugins.api.AutoService;
import org.apache.xerces.jaxp.SAXParserFactoryImpl;

@AutoService(SAXParserFactory.class)
@SuppressFBWarnings("IICU_INCORRECT_INTERNAL_CLASS_USE")
public class ConfigurableSAXParserFactory extends WrappedSAXParserFactory {

    private static final List<SAXParserFactoryConfigurer> SAX_PARSER_FACTORY_CONFIGURERS = newArrayList(loadServices(SAXParserFactoryConfigurer.class));

    @SneakyThrows
    public ConfigurableSAXParserFactory() {
        super(new SAXParserFactoryImpl());
        for (SAXParserFactoryConfigurer configurer : SAX_PARSER_FACTORY_CONFIGURERS) {
            configurer.configure(this);
        }
    }

}
