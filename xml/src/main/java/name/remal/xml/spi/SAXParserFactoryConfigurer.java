package name.remal.xml.spi;

import javax.xml.parsers.SAXParserFactory;
import org.jetbrains.annotations.NotNull;

public interface SAXParserFactoryConfigurer {

    void configure(@NotNull SAXParserFactory saxParserFactory) throws Throwable;

}
