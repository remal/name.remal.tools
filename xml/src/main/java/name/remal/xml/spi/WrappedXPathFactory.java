package name.remal.xml.spi;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathFactoryConfigurationException;
import javax.xml.xpath.XPathFunctionResolver;
import javax.xml.xpath.XPathVariableResolver;
import org.jetbrains.annotations.NotNull;

class WrappedXPathFactory extends XPathFactory {

    @NotNull
    private final XPathFactory delegate;

    public WrappedXPathFactory(@NotNull XPathFactory delegate) {
        this.delegate = delegate;
    }

    @Override
    public boolean isObjectModelSupported(String objectModel) {
        return delegate.isObjectModelSupported(objectModel);
    }

    @Override
    public void setFeature(String name, boolean value) throws XPathFactoryConfigurationException {
        delegate.setFeature(name, value);
    }

    @Override
    public boolean getFeature(String name) throws XPathFactoryConfigurationException {
        return delegate.getFeature(name);
    }

    @Override
    public void setXPathVariableResolver(XPathVariableResolver resolver) {
        delegate.setXPathVariableResolver(resolver);
    }

    @Override
    public void setXPathFunctionResolver(XPathFunctionResolver resolver) {
        delegate.setXPathFunctionResolver(resolver);
    }

    @Override
    public XPath newXPath() {
        return delegate.newXPath();
    }

}
