package name.remal.xml.spi;

import static com.google.common.collect.Lists.newArrayList;
import static name.remal.Services.loadServices;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.List;
import lombok.SneakyThrows;

@SuppressFBWarnings("IICU_INCORRECT_INTERNAL_CLASS_USE")
public class ConfigurableSchemaFactory extends WrappedSchemaFactory {

    private static final List<SchemaFactoryConfigurer> SCHEMA_FACTORY_CONFIGURERS = newArrayList(loadServices(SchemaFactoryConfigurer.class));

    @SneakyThrows
    public ConfigurableSchemaFactory() {
        super(new org.apache.xerces.jaxp.validation.XMLSchemaFactory());
        for (SchemaFactoryConfigurer configurer : SCHEMA_FACTORY_CONFIGURERS) {
            configurer.configure(this);
        }
    }

}
