package name.remal.xml.spi;

import java.io.File;
import java.net.URL;
import javax.xml.transform.Source;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.jetbrains.annotations.NotNull;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

class WrappedSchemaFactory extends SchemaFactory {

    @NotNull
    private final SchemaFactory delegate;

    public WrappedSchemaFactory(@NotNull SchemaFactory delegate) {
        this.delegate = delegate;
    }

    @Override
    public boolean isSchemaLanguageSupported(String schemaLanguage) {
        return delegate.isSchemaLanguageSupported(schemaLanguage);
    }

    @Override
    public boolean getFeature(String name) throws SAXNotRecognizedException, SAXNotSupportedException {
        return delegate.getFeature(name);
    }

    @Override
    public void setFeature(String name, boolean value) throws SAXNotRecognizedException, SAXNotSupportedException {
        delegate.setFeature(name, value);
    }

    @Override
    public void setProperty(String name, Object object) throws SAXNotRecognizedException, SAXNotSupportedException {
        delegate.setProperty(name, object);
    }

    @Override
    public Object getProperty(String name) throws SAXNotRecognizedException, SAXNotSupportedException {
        return delegate.getProperty(name);
    }

    @Override
    public void setErrorHandler(ErrorHandler errorHandler) {
        delegate.setErrorHandler(errorHandler);
    }

    @Override
    public ErrorHandler getErrorHandler() {
        return delegate.getErrorHandler();
    }

    @Override
    public void setResourceResolver(LSResourceResolver resourceResolver) {
        delegate.setResourceResolver(resourceResolver);
    }

    @Override
    public LSResourceResolver getResourceResolver() {
        return delegate.getResourceResolver();
    }

    @Override
    public Schema newSchema(Source schema) throws SAXException {
        return delegate.newSchema(schema);
    }

    @Override
    public Schema newSchema(File schema) throws SAXException {
        return delegate.newSchema(schema);
    }

    @Override
    public Schema newSchema(URL schema) throws SAXException {
        return delegate.newSchema(schema);
    }

    @Override
    public Schema newSchema(Source[] schemas) throws SAXException {
        return delegate.newSchema(schemas);
    }

    @Override
    public Schema newSchema() throws SAXException {
        return delegate.newSchema();
    }

}
