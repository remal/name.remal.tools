package name.remal.xml.spi;

import javax.xml.parsers.DocumentBuilder;
import name.remal.Ordered;
import org.jetbrains.annotations.NotNull;

public interface DocumentBuilderConfigurer extends Ordered<DocumentBuilderConfigurer> {

    void configure(@NotNull DocumentBuilder documentBuilder) throws Throwable;

}
