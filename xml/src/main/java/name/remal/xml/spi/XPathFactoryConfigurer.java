package name.remal.xml.spi;

import javax.xml.xpath.XPathFactory;
import org.jetbrains.annotations.NotNull;

public interface XPathFactoryConfigurer {

    void configure(@NotNull XPathFactory xPathFactory) throws Throwable;

}
