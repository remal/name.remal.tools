package name.remal.xml.spi;

import javax.xml.transform.TransformerFactory;
import org.jetbrains.annotations.NotNull;

public interface TransformerFactoryConfigurer {

    void configure(@NotNull TransformerFactory transformerFactory) throws Throwable;

}
