package name.remal.xml.spi;

import static com.google.common.collect.Lists.newArrayList;
import static name.remal.Services.loadServices;

import java.util.List;
import javax.xml.transform.TransformerFactory;
import lombok.SneakyThrows;
import name.remal.gradle_plugins.api.AutoService;
import net.sf.saxon.TransformerFactoryImpl;

@AutoService(TransformerFactory.class)
public class ConfigurableTransformerFactory extends WrappedTransformerFactory {

    private static final List<TransformerFactoryConfigurer> TRANSFORMER_FACTORY_CONFIGURERS = newArrayList(loadServices(TransformerFactoryConfigurer.class));

    @SneakyThrows
    public ConfigurableTransformerFactory() {
        super(new TransformerFactoryImpl());
        for (TransformerFactoryConfigurer configurer : TRANSFORMER_FACTORY_CONFIGURERS) {
            configurer.configure(this);
        }
    }

}
