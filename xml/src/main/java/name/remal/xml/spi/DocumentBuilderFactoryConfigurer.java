package name.remal.xml.spi;

import javax.xml.parsers.DocumentBuilderFactory;
import name.remal.Ordered;
import org.jetbrains.annotations.NotNull;

public interface DocumentBuilderFactoryConfigurer extends Ordered<DocumentBuilderFactoryConfigurer> {

    void configure(@NotNull DocumentBuilderFactory documentBuilderFactory) throws Throwable;

}
