package name.remal.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static javax.annotation.meta.When.ALWAYS;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.annotation.Nonnull;
import javax.annotation.meta.TypeQualifierNickname;

@Nonnull(when = ALWAYS)
@TypeQualifierNickname
@Target({METHOD, PARAMETER, FIELD})
@Retention(RUNTIME)
@Documented
public @interface NonNull {
}
