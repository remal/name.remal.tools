package name.remal;

import static org.assertj.core.api.Assertions.assertThat;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.junit.Test;

@SuppressFBWarnings("SACM_STATIC_ARRAY_CREATED_IN_METHOD")
public class ArrayUtilsTest {

    @Test
    public void testPrimitiveArraysConcat() {
        int[] array1 = new int[]{1, 2};
        int[] array2 = new int[]{3, 4, 5};
        assertThat(ArrayUtils.concat(array1, array2)).containsExactly(1, 2, 3, 4, 5);
    }

    @Test
    public void testArraysConcat() {
        String[] array1 = new String[]{"1", "2"};
        String[] array2 = new String[]{"3", "4", "5"};
        assertThat(ArrayUtils.concat(array1, array2)).containsExactly("1", "2", "3", "4", "5");
    }

    @Test
    public void testStartsWith() {
        assertThat(ArrayUtils.startsWith(new int[]{1, 2, 3})).isTrue();
        assertThat(ArrayUtils.startsWith(new int[]{1, 2, 3}, 1)).isTrue();
        assertThat(ArrayUtils.startsWith(new int[]{1, 2, 3}, 1, 2)).isTrue();
        assertThat(ArrayUtils.startsWith(new int[]{1, 2, 3}, 1, 2, 3)).isTrue();
        assertThat(ArrayUtils.startsWith(new int[]{1, 2, 3}, 1, 2, 3, 4)).isFalse();
        assertThat(ArrayUtils.startsWith(new int[]{1, 2, 3}, 2, 3)).isFalse();

        assertThat(ArrayUtils.startsWith(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{2, 3}})).isFalse();
        assertThat(ArrayUtils.startsWith(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{1, 2}})).isTrue();
        assertThat(ArrayUtils.startsWith(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{3, 4}})).isFalse();
        assertThat(ArrayUtils.startsWith(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{5, 6}})).isFalse();
    }

    @Test
    public void testEndsWith() {
        assertThat(ArrayUtils.endsWith(new int[]{1, 2, 3})).isTrue();
        assertThat(ArrayUtils.endsWith(new int[]{1, 2, 3}, 3)).isTrue();
        assertThat(ArrayUtils.endsWith(new int[]{1, 2, 3}, 2, 3)).isTrue();
        assertThat(ArrayUtils.endsWith(new int[]{1, 2, 3}, 1, 2, 3)).isTrue();
        assertThat(ArrayUtils.endsWith(new int[]{1, 2, 3}, 0, 1, 2, 3)).isFalse();
        assertThat(ArrayUtils.endsWith(new int[]{1, 2, 3}, 1, 2)).isFalse();

        assertThat(ArrayUtils.endsWith(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{2, 3}})).isFalse();
        assertThat(ArrayUtils.endsWith(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{1, 2}})).isFalse();
        assertThat(ArrayUtils.endsWith(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{3, 4}})).isFalse();
        assertThat(ArrayUtils.endsWith(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{5, 6}})).isTrue();
    }

    @Test
    public void testIndexOf() {
        assertThat(ArrayUtils.indexOf(new int[]{1, 2, 3})).isEqualTo(0);
        assertThat(ArrayUtils.indexOf(new int[]{1, 2, 3}, 2)).isEqualTo(1);
        assertThat(ArrayUtils.indexOf(new int[]{1, 2, 3}, 2, 3)).isEqualTo(1);
        assertThat(ArrayUtils.indexOf(new int[]{1, 2, 3}, 1, 2)).isEqualTo(0);
        assertThat(ArrayUtils.indexOf(new int[]{1, 2, 3}, 1, 2, 3)).isEqualTo(0);
        assertThat(ArrayUtils.indexOf(new int[]{1, 2, 3}, 0, 1, 2, 3)).isEqualTo(-1);

        assertThat(ArrayUtils.indexOf(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{2, 3}})).isEqualTo(-1);
        assertThat(ArrayUtils.indexOf(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{1, 2}})).isEqualTo(0);
        assertThat(ArrayUtils.indexOf(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{3, 4}})).isEqualTo(1);
        assertThat(ArrayUtils.indexOf(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{5, 6}})).isEqualTo(2);
    }

    @Test
    public void testContains() {
        assertThat(ArrayUtils.contains(new int[]{1, 2, 3})).isTrue();
        assertThat(ArrayUtils.contains(new int[]{1, 2, 3}, 2)).isTrue();
        assertThat(ArrayUtils.contains(new int[]{1, 2, 3}, 2, 3)).isTrue();
        assertThat(ArrayUtils.contains(new int[]{1, 2, 3}, 1, 2)).isTrue();
        assertThat(ArrayUtils.contains(new int[]{1, 2, 3}, 1, 2, 3)).isTrue();
        assertThat(ArrayUtils.contains(new int[]{1, 2, 3}, 0, 1, 2, 3)).isFalse();

        assertThat(ArrayUtils.contains(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{2, 3}})).isFalse();
        assertThat(ArrayUtils.contains(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{1, 2}})).isTrue();
        assertThat(ArrayUtils.contains(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{3, 4}})).isTrue();
        assertThat(ArrayUtils.contains(new int[][]{new int[]{1, 2}, new int[]{3, 4}, new int[]{5, 6}}, new int[][]{new int[]{5, 6}})).isTrue();
    }

}
