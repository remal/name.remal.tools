package name.remal.version;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.assertj.core.api.Assertions;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

@SuppressFBWarnings("OBJECT_DESERIALIZATION")
public class VersionRangeTest {

    private static void expectVersionRangeParsingException(@NotNull String string) {
        try {
            VersionRange.parse(string);
            fail(VersionRangeParsingException.class.getSimpleName() + " expected");
        } catch (VersionRangeParsingException ignored) {
            // OK
        } catch (Throwable throwable) {
            throw new AssertionError(throwable);
        }

        assertNull(VersionRange.parseOrNull(string));
    }

    @Test
    public void testParsing() {
        assertSame(VersionRange.ANY_VERSION, VersionRange.parse("+"));


        expectVersionRangeParsingException("");
        expectVersionRangeParsingException(" ");
        expectVersionRangeParsingException("\t \n");
        expectVersionRangeParsingException(".+");
        expectVersionRangeParsingException("1+.2");
        expectVersionRangeParsingException("1.[2+");
        expectVersionRangeParsingException("1.(2+");
        expectVersionRangeParsingException("1].2+");
        expectVersionRangeParsingException("1).2+");
        expectVersionRangeParsingException("1.2-suffix+");
        expectVersionRangeParsingException("[1.2-suffix,1.3]");
        expectVersionRangeParsingException("[1.2,1.3-suffix]");
        assertEquals(
            VersionRange.create(
                Version.create(1),
                true,
                null,
                false
            ),
            VersionRange.parse("1+")
        );
        assertEquals(
            VersionRange.create(
                Version.create(1, 2),
                true,
                Version.create(2),
                false
            ),
            VersionRange.parse("1.2+")
        );
        assertEquals(
            VersionRange.create(
                Version.create(1, 2),
                false,
                Version.create(1, 3),
                false
            ),
            VersionRange.parse("1.2.+")
        );


        expectVersionRangeParsingException("[1.2");
        expectVersionRangeParsingException("(1.2");
        expectVersionRangeParsingException("1.[2");
        expectVersionRangeParsingException("1.(2");
        expectVersionRangeParsingException("1].2");
        expectVersionRangeParsingException("1).2");
        expectVersionRangeParsingException("1.2]");
        expectVersionRangeParsingException("1.2)");
        expectVersionRangeParsingException("[1.2]");
        expectVersionRangeParsingException("[1.2,1.1]");
        expectVersionRangeParsingException("[1.2;1.1]");
        assertEquals(
            VersionRange.lessThan(Version.create(1, 3)),
            VersionRange.parse("[ ,1.3)")
        );
        assertEquals(
            VersionRange.create(
                Version.create(1, 2),
                true,
                Version.create(1, 3),
                false
            ),
            VersionRange.parse("[1.2 ,1.3)")
        );
        assertEquals(
            VersionRange.create(
                Version.create(1, 2),
                true,
                Version.create(1, 3),
                false
            ),
            VersionRange.parse("[1.2 ;1.3)")
        );
        assertEquals(
            VersionRange.greaterThan(Version.create(1, 2)),
            VersionRange.parse("(1.2, ]")
        );
        assertEquals(
            VersionRange.create(
                Version.create(1, 2),
                false,
                Version.create(1, 3),
                true
            ),
            VersionRange.parse("(1.2, 1.3]")
        );
        assertEquals(
            VersionRange.create(
                Version.create(1, 2),
                false,
                Version.create(1, 3),
                true
            ),
            VersionRange.parse("(1.2; 1.3]")
        );


        assertSame(VersionRange.ANY_VERSION, VersionRange.create(null, false, null, false));
        assertSame(VersionRange.ANY_VERSION, VersionRange.create(null, false, null, true));
        assertSame(VersionRange.ANY_VERSION, VersionRange.create(null, true, null, false));
        assertSame(VersionRange.ANY_VERSION, VersionRange.create(null, true, null, true));
        assertSame(VersionRange.ANY_VERSION, VersionRange.parse("[ ,]"));
        assertSame(VersionRange.ANY_VERSION, VersionRange.parse("[, )"));
        assertSame(VersionRange.ANY_VERSION, VersionRange.parse("( , ]"));
        assertSame(VersionRange.ANY_VERSION, VersionRange.parse("( ,)"));
        assertSame(VersionRange.ANY_VERSION, VersionRange.parse("[; ]"));
        assertSame(VersionRange.ANY_VERSION, VersionRange.parse("[ ; )"));
        assertSame(VersionRange.ANY_VERSION, VersionRange.parse("( ;]"));
        assertSame(VersionRange.ANY_VERSION, VersionRange.parse("(; )"));


        assertEquals(VersionRange.create(Version.create(1, 2)), VersionRange.parse("1.2"));
    }

    @Test
    public void testShortFactoryMethods() {
        Version version = Version.create(1, 1);
        assertEquals(
            VersionRange.create(version, false, null, true),
            VersionRange.greaterThan(version)
        );
        assertEquals(
            VersionRange.create(version, true, null, true),
            VersionRange.greaterThanOrEqual(version)
        );
        assertEquals(
            VersionRange.create(null, true, version, false),
            VersionRange.lessThan(version)
        );
        assertEquals(
            VersionRange.create(null, true, version, true),
            VersionRange.lessThanOrEqual(version)
        );
    }

    @Test
    public void testToString() {
        assertEquals("+", VersionRange.ANY_VERSION.toString());
        assertEquals("1.2", VersionRange.parse("1.2").toString());
        assertEquals("1.2+", VersionRange.parse("1.2+").toString());
        assertEquals("1.2+", VersionRange.parse("[1.2,2)").toString());
        assertEquals("1.2.+", VersionRange.parse("1.2.+").toString());
        assertEquals("1.2.+", VersionRange.parse("(1.2; 1.3)").toString());
        assertEquals("(1.2,1.4]", VersionRange.parse("(1.2 ,1.4]").toString());
        assertEquals("[1.2,1.4)", VersionRange.parse("[1.2; 1.4)").toString());
    }

    @Test
    public void testContainsVersion() {
        Version version = Version.parse("1.2");
        assertTrue(VersionRange.ANY_VERSION.contains(VersionRange.ANY_VERSION));
        assertTrue(VersionRange.parse("[1.2,1.2)").contains(version));
        assertTrue(VersionRange.parse("(1.2,1.2]").contains(version));
        assertFalse(VersionRange.parse("(1.2,1.2)").contains(version));
        assertTrue(VersionRange.parse("[1.1,1.3)").contains(version));
        assertTrue(VersionRange.parse("[1.2,1.3)").contains(version));
        assertFalse(VersionRange.parse("(1.2,1.3)").contains(version));
        assertTrue(VersionRange.parse("(1.1,1.2]").contains(version));
        assertFalse(VersionRange.parse("(1.3,1.3)").contains(version));
        assertTrue(VersionRange.parse("(,1.3)").contains(version));
        assertTrue(VersionRange.parse("(1.1,)").contains(version));
    }

    @Test
    public void testContainsVersionRange() {
        {
            VersionRange versionRange = VersionRange.parse("1.2.+");
            assertTrue(VersionRange.create(null, true, null, true).contains(versionRange));
            assertTrue(VersionRange.parse("(1.2,1.3)").contains(versionRange));
            assertTrue(VersionRange.parse("(1.2,1.3]").contains(versionRange));
            assertFalse(VersionRange.parse("[1.2.0,1.3)").contains(versionRange));
            assertFalse(VersionRange.parse("(1.2.0,1.3]").contains(versionRange));
            assertTrue(VersionRange.parse("(,1.3)").contains(versionRange));
            assertFalse(VersionRange.parse("[1.2.0,)").contains(versionRange));
        }

        {
            VersionRange versionRange = VersionRange.parse("1.2+");
            assertTrue(VersionRange.create(null, true, null, true).contains(versionRange));
            assertFalse(VersionRange.parse("(1.2,2)").contains(versionRange));
            assertTrue(VersionRange.parse("[1.2,2]").contains(versionRange));
            assertFalse(VersionRange.parse("[1.2.0,2)").contains(versionRange));
            assertFalse(VersionRange.parse("(1.2.0,2]").contains(versionRange));
            assertTrue(VersionRange.parse("(,2)").contains(versionRange));
            assertFalse(VersionRange.parse("[1.2.0,)").contains(versionRange));
        }
    }

    @Test
    public void serializationTest() throws Throwable {
        VersionRange versionRange = VersionRange.parse("1.2+");

        final byte[] bytes;
        try (ByteArrayOutputStream bytesOut = new ByteArrayOutputStream()) {
            try (ObjectOutputStream objOut = new ObjectOutputStream(bytesOut)) {
                objOut.writeObject(versionRange);
            }
            bytes = bytesOut.toByteArray();
        }

        final VersionRange deserializedVersionRange;
        try (ByteArrayInputStream bytesIn = new ByteArrayInputStream(bytes)) {
            try (ObjectInputStream objIn = new ObjectInputStream(bytesIn)) {
                deserializedVersionRange = (VersionRange) objIn.readObject();
            }
        }

        assertEquals(versionRange, deserializedVersionRange);
    }

    @Test
    public void testClone() {
        VersionRange versionRange = VersionRange.parse("1.2+");
        VersionRange versionRangeClone = versionRange.clone();
        Assertions.assertThat(versionRangeClone)
            .isEqualTo(versionRange)
            .isNotSameAs(versionRange);
    }

}
