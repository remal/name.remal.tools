package name.remal.version;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import name.remal.lambda.VoidFunction0;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;


@SuppressFBWarnings("OBJECT_DESERIALIZATION")
public class VersionTest {

    @Test
    public void testSuffixDelimiterValidation() {
        for (int codePoint = Character.MIN_CODE_POINT; codePoint <= Character.MAX_CODE_POINT; ++codePoint) {
            char suffixDelimiter = (char) codePoint;
            final boolean isValid;
            if ('.' == suffixDelimiter) {
                isValid = true;
            } else if ('_' == suffixDelimiter) {
                isValid = true;
            } else if ('-' == suffixDelimiter) {
                isValid = true;
            } else if ('+' == suffixDelimiter) {
                isValid = true;
            } else {
                isValid = false;
            }

            if (isValid) {
                Version.create(1, suffixDelimiter, "suffix");

            } else {
                try {
                    Version.create(1, suffixDelimiter, "suffix");
                    fail("%s expected", IllegalArgumentException.class.getSimpleName());
                } catch (IllegalArgumentException ignored) {
                    // OK
                } catch (Throwable e) {
                    fail("%s expected, but %s has been thrown", IllegalArgumentException.class.getSimpleName(), e.getClass().getName());
                }
            }
        }
    }

    @Test
    public void testToString() {
        assertThat(Version.create(new int[]{1, 2, 3, 4, 5, 0}, "a-b").toString()).isEqualTo("1.2.3.4.5.0-a-b");
        assertThat(Version.create(new int[]{1, 2, 3, 4, 5, 0}, '_', "a-b").toString()).isEqualTo("1.2.3.4.5.0_a-b");
        assertThat(Version.create(new int[]{1, 2, 3, 4, 5, 0}, '.', "a-b").toString()).isEqualTo("1.2.3.4.5.0.a-b");
    }

    @Test
    public void testGetters() {
        Version version = Version.create(new int[]{1, 2, 3, 4, 5, 0}, "a-b");

        assertThat(version.getNumbersCount()).isEqualTo(6);
        assertThat(version.getNumberOrNull(0)).isNotNull().isEqualTo(1);
        assertThat(version.getNumberOr0(0)).isEqualTo(1);
        assertThat(version.getNumberOrNull(1)).isNotNull().isEqualTo(2);
        assertThat(version.getNumberOr0(1)).isEqualTo(2);
        assertThat(version.getNumberOrNull(2)).isNotNull().isEqualTo(3);
        assertThat(version.getNumberOr0(2)).isEqualTo(3);
        assertThat(version.getNumberOrNull(3)).isNotNull().isEqualTo(4);
        assertThat(version.getNumberOr0(3)).isEqualTo(4);
        assertThat(version.getNumberOrNull(4)).isNotNull().isEqualTo(5);
        assertThat(version.getNumberOr0(4)).isEqualTo(5);
        assertThat(version.getNumberOrNull(5)).isNotNull().isEqualTo(0);
        assertThat(version.getNumberOr0(5)).isEqualTo(0);
        assertThat(version.getNumberOrNull(6)).isNull();
        assertThat(version.getNumberOr0(6)).isEqualTo(0);

        assertThat(version.getMajor()).isEqualTo(1);
        assertThat(version.getMinor()).isNotNull().isEqualTo(2);
        assertThat(version.getPatch()).isNotNull().isEqualTo(3);
        assertThat(version.getBuild()).isNotNull().isEqualTo(4);
        assertThat(version.getSuffixDelimiter()).isEqualTo('-');
        assertThat(version.getSuffix()).isEqualTo("a-b");
    }

    @Test
    public void testWithoutMethods() {
        Version version = Version.create(new int[]{1, 2, 3, 4, 5, 0}, '.', "a-b");
        assertThat(version.withDefaultSuffixDelimiter()).isEqualTo(Version.create(new int[]{1, 2, 3, 4, 5, 0}, '-', "a-b"));
        assertThat(version.withoutSuffix()).isEqualTo(Version.create(new int[]{1, 2, 3, 4, 5, 0}, '.', ""));
        assertThat(version.withoutBuild()).isEqualTo(Version.create(new int[]{1, 2, 3}, '.', "a-b"));
        assertThat(version.withoutPatch()).isEqualTo(Version.create(new int[]{1, 2}, '.', "a-b"));
        assertThat(version.withoutMinor()).isEqualTo(Version.create(new int[]{1}, '.', "a-b"));

        assertThat(version.withoutNumber(5)).isEqualTo(Version.create(new int[]{1, 2, 3, 4, 5}, '.', "a-b"));
        assertThat(version.withoutNumber(4)).isEqualTo(Version.create(new int[]{1, 2, 3, 4}, '.', "a-b"));
        assertThat(version.withoutNumber(3)).isEqualTo(Version.create(new int[]{1, 2, 3}, '.', "a-b"));
        assertThat(version.withoutNumber(2)).isEqualTo(Version.create(new int[]{1, 2}, '.', "a-b"));
        assertThat(version.withoutNumber(1)).isEqualTo(Version.create(new int[]{1}, '.', "a-b"));
    }

    @Test
    public void testNumbersComparison() {
        Version v1_2 = Version.create(1, 2);
        Version v1_15 = Version.create(1, 15);
        assertThat(v1_2).isLessThan(v1_15);

        Version v1_2_0 = Version.create(1, 2, 0);
        assertThat(v1_2).isLessThan(v1_2_0);
    }

    @Test
    public void testSuffixComparison() {
        assertThat(Version.create(1, "")).isLessThan(Version.create(1, "suffix"));

        assertThat(Version.create(1)).isGreaterThan(Version.create(1, "SNAPSHOT"));
        assertThat(Version.create(1)).isLessThan(Version.create(1, '.', "RELEASE"));

        Map<String, Integer> orders = new HashMap<>();
        orders.put("sp", 2);
        orders.put("r", 1);
        orders.put("release", 1);
        orders.put("ga", 1);
        orders.put("final", 1);
        orders.put("", 0);
        orders.put("snapshot", -1);
        orders.put("nightly", -2);
        orders.put("rc", -3);
        orders.put("cr", -3);
        orders.put("milestone", -4);
        orders.put("m", -4);
        orders.put("beta", -5);
        orders.put("b", -5);
        orders.put("alpha", -6);
        orders.put("a", -6);
        orders.put("dev", -7);
        orders.put("pr", -7);
        for (Entry<String, Integer> entry1 : orders.entrySet()) {
            for (Entry<String, Integer> entry2 : orders.entrySet()) {
                Version v1 = Version.create(1, "0" + randomCase(entry1.getKey()) + "123");
                Version v2 = Version.create(1, "0" + randomCase(entry2.getKey()) + "123");
                if (entry1.getValue() < entry2.getValue()) {
                    assertThat(v1).isLessThan(v2);
                } else if (entry1.getValue() > entry2.getValue()) {
                    assertThat(v1).isGreaterThan(v2);
                }
            }
        }
    }

    @Test
    public void testParsing() {
        assertThat(Version.parse("11.22.33")).isEqualTo(Version.create(11, 22, 33));
        assertThat(Version.parse("11.22.33-suffix")).isEqualTo(Version.create(11, 22, 33, "suffix"));
        assertThat(Version.parse("11.22.33.suffix")).isEqualTo(Version.create(11, 22, 33, '.', "suffix"));
        assertThat(Version.parse("11.22.33_suffix")).isEqualTo(Version.create(11, 22, 33, '_', "suffix"));
        assertThat(Version.parse("11.22.33+suffix")).isEqualTo(Version.create(11, 22, 33, '+', "suffix"));

        expectVersionParsingException(() -> Version.parse("gg"));
        expectVersionParsingException(() -> Version.parse("1."));
        expectVersionParsingException(() -> Version.parse("-1"));
    }

    private static void expectVersionParsingException(@NotNull VoidFunction0 action) {
        try {
            action.invoke();
            fail("%s expected", VersionParsingException.class.getSimpleName());
        } catch (VersionParsingException ignored) {
            // OK
        } catch (Throwable e) {
            fail("%s expected, but %s has been thrown", IllegalArgumentException.class.getSimpleName(), e.getClass().getName());
        }
    }

    @Test
    public void serializationTest() throws Throwable {
        Version version = Version.create(1, 2, "suffix");

        final byte[] bytes;
        try (ByteArrayOutputStream bytesOut = new ByteArrayOutputStream()) {
            try (ObjectOutputStream objOut = new ObjectOutputStream(bytesOut)) {
                objOut.writeObject(version);
            }
            bytes = bytesOut.toByteArray();
        }

        final Version deserializedVersion;
        try (ByteArrayInputStream bytesIn = new ByteArrayInputStream(bytes)) {
            try (ObjectInputStream objIn = new ObjectInputStream(bytesIn)) {
                deserializedVersion = (Version) objIn.readObject();
            }
        }

        assertThat(deserializedVersion).isEqualTo(version);
    }

    @Test
    public void testClone() {
        Version version = Version.create(1, 2, "suffix");
        Version versionClone = version.clone();
        assertThat(versionClone)
            .isEqualTo(version)
            .isNotSameAs(version);
    }

    @Test
    public void testDeduplication() {
        int[][] numbersArray = new int[][]{
            new int[]{0},
            new int[]{1},

            new int[]{0, 0},
            new int[]{0, 1},
            new int[]{1, 0},

            new int[]{0, 0, 0},
            new int[]{0, 0, 1},
            new int[]{0, 1, 0},
            new int[]{1, 0, 0},

            new int[]{0, 0, 0, 0},
            new int[]{0, 0, 0, 1},
            new int[]{0, 0, 1, 0},
            new int[]{0, 1, 0, 0},
            new int[]{1, 0, 0, 0},
        };
        for (int[] numbers : numbersArray) {
            Version version = Version.create(numbers);
            for (int i = 0; i < numbers.length; ++i) {
                assertThat(version.getNumberOrNull(i)).isNotNull().isEqualTo(numbers[i]);
            }
            assertThat(version).isSameAs(Version.create(numbers));
        }
    }

    @Test
    public void testIncrement() {
        assertThat(Version.create(1, 2, 3).incrementNumber(0)).isEqualTo(Version.create(2, 2, 3));
        assertThat(Version.create(1, 2, 3).incrementNumber(1)).isEqualTo(Version.create(1, 3, 3));
        assertThat(Version.create(1, 2, 3).incrementNumber(2)).isEqualTo(Version.create(1, 2, 4));
        assertThat(Version.create(1, 2, 3).incrementNumber(3)).isEqualTo(Version.create(1, 2, 3, 1));
        assertThat(Version.create(1, 2, 3).incrementNumber(4)).isEqualTo(Version.create(1, 2, 3, 0, 1));
    }


    @NotNull
    private static String randomCase(@NotNull String string) {
        if (string.length() == 0) {
            return "";
        } else if (string.length() == 1) {
            return Math.random() >= 0.5 ? string.toUpperCase() : string.toLowerCase();
        } else {
            StringBuilder sb = new StringBuilder(string.length());
            for (int i = 0; i < string.length(); ++i) {
                char ch = string.charAt(i);
                sb.append(Math.random() >= 0.5 ? Character.toUpperCase(ch) : Character.toLowerCase(ch));
            }
            return sb.toString();
        }
    }

}
