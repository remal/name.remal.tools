package name.remal;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import org.junit.Test;

public class URLUtilsTest {

    @Test
    public void withProtocol() throws Throwable {
        assertEquals(
            new URL("https://user@example.com:8080/path?query#ref"),
            URLUtils.withProtocol(new URL("http://user@example.com:8080/path?query#ref"), "https")
        );
    }

    @Test
    public void withAuthority() throws Throwable {
        assertEquals(
            new URL("http://u@host:81/path?query#ref"),
            URLUtils.withAuthority(new URL("http://user@example.com:8080/path?query#ref"), "u@host:81")
        );
    }

    @Test
    public void withUserInfo() throws Throwable {
        assertEquals(
            new URL("http://other-user@example.com:8080/path?query#ref"),
            URLUtils.withUserInfo(new URL("http://user@example.com:8080/path?query#ref"), "other-user")
        );
        assertEquals(
            new URL("http://example.com:8080/path?query#ref"),
            URLUtils.withUserInfo(new URL("http://user@example.com:8080/path?query#ref"), "")
        );
        assertEquals(
            new URL("http://example.com:8080/path?query#ref"),
            URLUtils.withUserInfo(new URL("http://user@example.com:8080/path?query#ref"), null)
        );
    }

    @Test
    public void withPort() throws Throwable {
        assertEquals(
            new URL("http://user@example.com:81/path?query#ref"),
            URLUtils.withPort(new URL("http://user@example.com:8080/path?query#ref"), 81)
        );
        assertEquals(
            new URL("http://user@example.com/path?query#ref"),
            URLUtils.withPort(new URL("http://user@example.com:8080/path?query#ref"), -1)
        );
    }

    @Test
    public void withDefaultPort() throws Throwable {
        assertEquals(
            new URL("http://user@example.com/path?query#ref"),
            URLUtils.withDefaultPort(new URL("http://user@example.com:8080/path?query#ref"))
        );
    }

    @Test
    public void withPath() throws Throwable {
        assertEquals(
            new URL("http://user@example.com:8080/gg?query#ref"),
            URLUtils.withPath(new URL("http://user@example.com:8080/path?query#ref"), "/gg")
        );
    }

    @Test
    public void withQuery() throws Throwable {
        assertEquals(
            new URL("http://user@example.com:8080/path?q#ref"),
            URLUtils.withQuery(new URL("http://user@example.com:8080/path?query#ref"), "q")
        );
        assertEquals(
            new URL("http://user@example.com:8080/path#ref"),
            URLUtils.withQuery(new URL("http://user@example.com:8080/path?query#ref"), "")
        );
        assertEquals(
            new URL("http://user@example.com:8080/path#ref"),
            URLUtils.withQuery(new URL("http://user@example.com:8080/path?query#ref"), null)
        );
    }

    @Test
    public void withRef() throws Throwable {
        assertEquals(
            new URL("http://user@example.com:8080/path?query#r"),
            URLUtils.withRef(new URL("http://user@example.com:8080/path?query#ref"), "r")
        );
        assertEquals(
            new URL("http://user@example.com:8080/path?query"),
            URLUtils.withRef(new URL("http://user@example.com:8080/path?query#ref"), "")
        );
        assertEquals(
            new URL("http://user@example.com:8080/path?query"),
            URLUtils.withRef(new URL("http://user@example.com:8080/path?query#ref"), null)
        );
    }

}
