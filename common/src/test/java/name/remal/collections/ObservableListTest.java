package name.remal.collections;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.junit.Test;

@SuppressWarnings("OverwrittenKey")
public class ObservableListTest {

    @Test
    public void observableListTest() {
        List<String> list = new ArrayList<>();
        List<String> events = new ArrayList<>();
        ObservableList<String> observableList = ObservableCollections.observableList(list);
        HandlerRegistration addedHandlerRegistration = observableList.registerElementAddedHandler(element -> events.add("added:" + element));
        HandlerRegistration removedHandlerRegistration = observableList.registerElementRemovedHandler(element -> events.add("removed:" + element));

        observableList.remove("0");
        observableList.add("1");
        observableList.add("2");
        observableList.remove("1");
        observableList.add("3");
        observableList.add("4");
        observableList.add("4");

        Iterator<String> iterator = observableList.iterator();
        iterator.next();
        iterator.next();
        iterator.remove();

        ListIterator<String> listIterator = observableList.listIterator();
        listIterator.next();
        listIterator.next();
        listIterator.previous();
        listIterator.remove();

        assertThat(list).containsExactly(
            "2",
            "4"
        );
        assertThat(events).containsExactly(
            "added:1",
            "added:2",
            "removed:1",
            "added:3",
            "added:4",
            "added:4",
            "removed:3",
            "removed:4"
        );

        addedHandlerRegistration.deregister();
        observableList.add("element");
        observableList.remove("2");
        assertThat(list).containsExactly(
            "4",
            "element"
        );
        assertThat(events).containsExactly(
            "added:1",
            "added:2",
            "removed:1",
            "added:3",
            "added:4",
            "added:4",
            "removed:3",
            "removed:4",
            "removed:2"
        );

        removedHandlerRegistration.deregister();
        observableList.remove("element");
        assertThat(list).containsExactly(
            "4"
        );
        assertThat(events).containsExactly(
            "added:1",
            "added:2",
            "removed:1",
            "added:3",
            "added:4",
            "added:4",
            "removed:3",
            "removed:4",
            "removed:2"
        );
    }

    @Test
    public void serialization() throws Throwable {
        ObservableList<String> observableList = ObservableCollections.observableList(new ArrayList<String>());
        observableList.add("1");
        observableList.add("1");
        observableList.add("2");

        final byte[] bytes;
        try (ByteArrayOutputStream bytesOut = new ByteArrayOutputStream()) {
            try (ObjectOutputStream objOut = new ObjectOutputStream(bytesOut)) {
                objOut.writeObject(observableList);
            }
            bytes = bytesOut.toByteArray();
        }
        final Object deserialized;
        try (ByteArrayInputStream bytesIn = new ByteArrayInputStream(bytes)) {
            try (ObjectInputStream objIn = new ObjectInputStream(bytesIn)) {
                deserialized = objIn.readObject();
            }
        }
        assertThat(deserialized).isEqualTo(observableList).isNotSameAs(observableList);
    }

}
