package name.remal.collections;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

@SuppressWarnings("OverwrittenKey")
public class ObservableMapTest {

    @NotNull
    private static <K, V> Entry<K, V> entry(K key, V value) {
        return new Entry<K, V>() {
            @Override
            public K getKey() {
                return key;
            }

            @Override
            public V getValue() {
                return value;
            }

            @Override
            public V setValue(V value) {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Test
    public void observableMapTest() {
        Map<String, String> map = new LinkedHashMap<>();
        List<String> events = new ArrayList<>();
        ObservableMap<String, String> observableMap = ObservableCollections.observableMap(map);
        observableMap.registerEntryAddedHandler((key, value) -> events.add("added:" + key + ':' + value));
        observableMap.registerEntryRemovedHandler((key, value) -> events.add("removed:" + key + ':' + value));

        observableMap.remove("0");
        observableMap.put("1", "v1");
        observableMap.remove("1");
        observableMap.put("1", "v1");
        observableMap.put("1", "v1");
        observableMap.put("1", "v1.1");
        observableMap.put("2", "v2");
        observableMap.put("3", "v3");
        observableMap.put("4", "v4");
        observableMap.put("5", "v5");
        observableMap.put("6", "v6");
        assertThat(map).containsExactly(
            entry("1", "v1.1"),
            entry("2", "v2"),
            entry("3", "v3"),
            entry("4", "v4"),
            entry("5", "v5"),
            entry("6", "v6")
        );
        assertThat(events).containsExactly(
            "added:1:v1",
            "removed:1:v1",
            "added:1:v1",
            "removed:1:v1",
            "added:1:v1.1",
            "added:2:v2",
            "added:3:v3",
            "added:4:v4",
            "added:5:v5",
            "added:6:v6"
        );

        {
            observableMap.keySet().remove("1");
            assertThat(map).containsExactly(
                entry("2", "v2"),
                entry("3", "v3"),
                entry("4", "v4"),
                entry("5", "v5"),
                entry("6", "v6")
            );
            assertThat(events).containsExactly(
                "added:1:v1",
                "removed:1:v1",
                "added:1:v1",
                "removed:1:v1",
                "added:1:v1.1",
                "added:2:v2",
                "added:3:v3",
                "added:4:v4",
                "added:5:v5",
                "added:6:v6",
                "removed:1:v1.1"
            );

            Iterator iterator = observableMap.keySet().iterator();
            iterator.next();
            iterator.remove();
            assertThat(map).containsExactly(
                entry("3", "v3"),
                entry("4", "v4"),
                entry("5", "v5"),
                entry("6", "v6")
            );
            assertThat(events).containsExactly(
                "added:1:v1",
                "removed:1:v1",
                "added:1:v1",
                "removed:1:v1",
                "added:1:v1.1",
                "added:2:v2",
                "added:3:v3",
                "added:4:v4",
                "added:5:v5",
                "added:6:v6",
                "removed:1:v1.1",
                "removed:2:v2"
            );
        }

        {
            observableMap.values().remove("v3");
            assertThat(map).containsExactly(
                entry("4", "v4"),
                entry("5", "v5"),
                entry("6", "v6")
            );
            assertThat(events).containsExactly(
                "added:1:v1",
                "removed:1:v1",
                "added:1:v1",
                "removed:1:v1",
                "added:1:v1.1",
                "added:2:v2",
                "added:3:v3",
                "added:4:v4",
                "added:5:v5",
                "added:6:v6",
                "removed:1:v1.1",
                "removed:2:v2",
                "removed:3:v3"
            );

            Iterator iterator = observableMap.values().iterator();
            iterator.next();
            iterator.remove();
            assertThat(map).containsExactly(
                entry("5", "v5"),
                entry("6", "v6")
            );
            assertThat(events).containsExactly(
                "added:1:v1",
                "removed:1:v1",
                "added:1:v1",
                "removed:1:v1",
                "added:1:v1.1",
                "added:2:v2",
                "added:3:v3",
                "added:4:v4",
                "added:5:v5",
                "added:6:v6",
                "removed:1:v1.1",
                "removed:2:v2",
                "removed:3:v3",
                "removed:4:v4"
            );
        }

        {
            Iterator iterator = observableMap.values().iterator();
            iterator.next();
            iterator.remove();
            assertThat(map).containsExactly(
                entry("6", "v6")
            );
            assertThat(events).containsExactly(
                "added:1:v1",
                "removed:1:v1",
                "added:1:v1",
                "removed:1:v1",
                "added:1:v1.1",
                "added:2:v2",
                "added:3:v3",
                "added:4:v4",
                "added:5:v5",
                "added:6:v6",
                "removed:1:v1.1",
                "removed:2:v2",
                "removed:3:v3",
                "removed:4:v4",
                "removed:5:v5"
            );
        }

        observableMap.clear();
        assertThat(map).isEmpty();
        assertThat(events).containsExactly(
            "added:1:v1",
            "removed:1:v1",
            "added:1:v1",
            "removed:1:v1",
            "added:1:v1.1",
            "added:2:v2",
            "added:3:v3",
            "added:4:v4",
            "added:5:v5",
            "added:6:v6",
            "removed:1:v1.1",
            "removed:2:v2",
            "removed:3:v3",
            "removed:4:v4",
            "removed:5:v5",
            "removed:6:v6"
        );
    }

    @Test
    public void serialization() throws Throwable {
        ObservableMap<String, String> observableMap = ObservableCollections.observableMap(new LinkedHashMap<>());
        observableMap.put("1", "2");
        observableMap.put("3", "4");

        final byte[] bytes;
        try (ByteArrayOutputStream bytesOut = new ByteArrayOutputStream()) {
            try (ObjectOutputStream objOut = new ObjectOutputStream(bytesOut)) {
                objOut.writeObject(observableMap);
            }
            bytes = bytesOut.toByteArray();
        }
        final Object deserialized;
        try (ByteArrayInputStream bytesIn = new ByteArrayInputStream(bytes)) {
            try (ObjectInputStream objIn = new ObjectInputStream(bytesIn)) {
                deserialized = objIn.readObject();
            }
        }
        assertThat(deserialized).isEqualTo(observableMap).isNotSameAs(observableMap);
    }

}
