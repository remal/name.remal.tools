package name.remal.collections;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.junit.Test;

@SuppressWarnings("OverwrittenKey")
public class ObservableSetTest {

    @Test
    public void observableSetTest() {
        Set<String> set = new LinkedHashSet<>();
        List<String> events = new ArrayList<>();
        ObservableSet<String> observableSet = ObservableCollections.observableSet(set);
        HandlerRegistration addedHandlerRegistration = observableSet.registerElementAddedHandler(element -> events.add("added:" + element));
        HandlerRegistration removedHandlerRegistration = observableSet.registerElementRemovedHandler(element -> events.add("removed:" + element));

        observableSet.remove("0");
        observableSet.add("1");
        observableSet.add("2");
        observableSet.remove("1");
        observableSet.add("3");
        observableSet.add("4");
        observableSet.add("4");

        Iterator<String> iterator = observableSet.iterator();
        iterator.next();
        iterator.next();
        iterator.remove();

        assertThat(set).containsExactly(
            "2",
            "4"
        );
        assertThat(events).containsExactly(
            "added:1",
            "added:2",
            "removed:1",
            "added:3",
            "added:4",
            "removed:3"
        );

        addedHandlerRegistration.deregister();
        observableSet.add("element");
        observableSet.remove("2");
        assertThat(set).containsExactly(
            "4",
            "element"
        );
        assertThat(events).containsExactly(
            "added:1",
            "added:2",
            "removed:1",
            "added:3",
            "added:4",
            "removed:3",
            "removed:2"
        );

        removedHandlerRegistration.deregister();
        observableSet.remove("element");
        assertThat(set).containsExactly(
            "4"
        );
        assertThat(events).containsExactly(
            "added:1",
            "added:2",
            "removed:1",
            "added:3",
            "added:4",
            "removed:3",
            "removed:2"
        );
    }

    @Test
    public void serialization() throws Throwable {
        ObservableSet<String> observableSet = ObservableCollections.observableSet(new LinkedHashSet<>());
        observableSet.add("1");
        observableSet.add("1");
        observableSet.add("2");

        final byte[] bytes;
        try (ByteArrayOutputStream bytesOut = new ByteArrayOutputStream()) {
            try (ObjectOutputStream objOut = new ObjectOutputStream(bytesOut)) {
                objOut.writeObject(observableSet);
            }
            bytes = bytesOut.toByteArray();
        }
        final Object deserialized;
        try (ByteArrayInputStream bytesIn = new ByteArrayInputStream(bytes)) {
            try (ObjectInputStream objIn = new ObjectInputStream(bytesIn)) {
                deserialized = objIn.readObject();
            }
        }
        assertThat(deserialized).isEqualTo(observableSet).isNotSameAs(observableSet);
    }

}
