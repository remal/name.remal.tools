package name.remal;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import org.junit.Test;

public class URIUtilsTest {

    @Test
    public void withScheme() throws Throwable {
        assertEquals(new URI("https://example.com"), URIUtils.withScheme(new URI("http://example.com"), "https"));
        assertEquals(new URI("//example.com"), URIUtils.withScheme(new URI("http://example.com"), ""));
        assertEquals(new URI("//example.com"), URIUtils.withScheme(new URI("http://example.com"), null));
        assertEquals(new URI("mailto:mail@example.com"), URIUtils.withScheme(new URI("mail:mail@example.com"), "mailto"));
        assertEquals(new URI("mail@example.com"), URIUtils.withScheme(new URI("mail:mail@example.com"), ""));
        assertEquals(new URI("mail@example.com"), URIUtils.withScheme(new URI("mail:mail@example.com"), null));
    }

    @Test
    public void withSchemeSpecificPart() throws Throwable {
        assertEquals(new URI("http:ssp#fragment"), URIUtils.withSchemeSpecificPart(new URI("http://example.com#fragment"), "ssp"));
        assertEquals(new URI("mail:ssp#fragment"), URIUtils.withSchemeSpecificPart(new URI("mail:mail@example.com#fragment"), "ssp"));
    }

    @Test
    public void withAuthority() throws Throwable {
        assertEquals(new URI("http://authority"), URIUtils.withAuthority(new URI("http://example.com"), "authority"));
        assertEquals(new URI("/path"), URIUtils.withAuthority(new URI("//example.com/path"), ""));
        assertEquals(new URI("/path"), URIUtils.withAuthority(new URI("//example.com/path"), null));
        assertEquals(new URI("mail://authority"), URIUtils.withAuthority(new URI("mail:mail@example.com"), "authority"));
    }

    @Test
    public void withUserInfo() throws Throwable {
        assertEquals(new URI("http://user@example.com"), URIUtils.withUserInfo(new URI("http://example.com"), "user"));
        assertEquals(new URI("//user@example.com"), URIUtils.withUserInfo(new URI("//example.com"), "user"));
    }

    @Test
    public void withHost() throws Throwable {
        assertEquals(new URI("http://user@host/path"), URIUtils.withHost(new URI("http://user@example.com/path"), "host"));
        assertEquals(new URI("//user@host/path"), URIUtils.withHost(new URI("//user@example.com/path"), "host"));
    }

    @Test
    public void withPort() throws Throwable {
        assertEquals(new URI("http://user@example.com/path"), URIUtils.withPort(new URI("http://user@example.com:8080/path"), -1));
        assertEquals(new URI("//user@example.com/path"), URIUtils.withPort(new URI("//user@example.com:8080/path"), -1));
        assertEquals(new URI("http://user@example.com:8081/path"), URIUtils.withPort(new URI("http://user@example.com:8080/path"), 8081));
        assertEquals(new URI("//user@example.com:8081/path"), URIUtils.withPort(new URI("//user@example.com:8080/path"), 8081));
    }

    @Test
    public void withDefaultPort() throws Throwable {
        assertEquals(new URI("http://user@example.com/path"), URIUtils.withDefaultPort(new URI("http://user@example.com:8080/path")));
        assertEquals(new URI("//user@example.com/path"), URIUtils.withDefaultPort(new URI("//user@example.com:8080/path")));
    }

    @Test
    public void withPath() throws Throwable {
        assertEquals(new URI("http://example.com/path"), URIUtils.withPath(new URI("http://example.com"), "/path"));
        assertEquals(new URI("//example.com/path"), URIUtils.withPath(new URI("//example.com"), "/path"));
        assertEquals(new URI("/path"), URIUtils.withPath(new URI("/gg"), "/path"));
        assertEquals(new URI("path"), URIUtils.withPath(new URI("/gg"), "path"));
    }

    @Test
    public void withQuery() throws Throwable {
        assertEquals(new URI("http://example.com/path?query"), URIUtils.withQuery(new URI("http://example.com/path"), "query"));
        assertEquals(new URI("//example.com/path?query"), URIUtils.withQuery(new URI("//example.com/path?q"), "query"));
        assertEquals(new URI("http://example.com?query"), URIUtils.withQuery(new URI("http://example.com?q"), "query"));
        assertEquals(new URI("//example.com?query"), URIUtils.withQuery(new URI("//example.com"), "query"));
        assertEquals(new URI("/path?query"), URIUtils.withQuery(new URI("/path"), "query"));
        assertEquals(new URI("path?query"), URIUtils.withQuery(new URI("path"), "query"));
    }

    @Test
    public void withFragment() throws Throwable {
        assertEquals(new URI("http://example.com/path?query#fragment"), URIUtils.withFragment(new URI("http://example.com/path?query"), "fragment"));
        assertEquals(new URI("//example.com/path?query#fragment"), URIUtils.withFragment(new URI("//example.com/path?query#f"), "fragment"));
        assertEquals(new URI("http://example.com#fragment"), URIUtils.withFragment(new URI("http://example.com#f"), "fragment"));
        assertEquals(new URI("//example.com#fragment"), URIUtils.withFragment(new URI("//example.com"), "fragment"));
        assertEquals(new URI("/path#fragment"), URIUtils.withFragment(new URI("/path"), "fragment"));
        assertEquals(new URI("path#fragment"), URIUtils.withFragment(new URI("path"), "fragment"));
        System.out.println(new URI("http://user:pass@example/com/").toURL().getHost());
    }

}
