package name.remal.annotation;

import static java.util.Collections.singletonMap;
import static name.remal.annotation.AnnotationUtils.getMetaAnnotations;
import static name.remal.annotation.AnnotationUtils.newAnnotationInstance;
import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableMap;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

public class AnnotationUtils_getMetaAnnotationsTest extends BaseMetaAnnotationsTest {

    @Test
    public void repeatableAnnotationsAreHandledEvenIfNotAnnotatedWithRepeatable() {
        assertThat(getMetaAnnotations(RepeatableAnnotationClass.class, RepeatableAnnotation.class))
            .containsExactly(newRepeatableAnnotation(1), newRepeatableAnnotation(2), newRepeatableAnnotation(3));
    }

    @Test
    public void metaAnnotationsAreHandled() {
        assertThat(getMetaAnnotations(MetaAnnotationClass.class, MetaAnnotation.class))
            .containsExactly(
                newMetaAnnotation(10),
                newMetaAnnotation("value"),
                newMetaAnnotation(-1),
                newMetaAnnotation("super")
            );
    }

    @Test
    public void inheritedAnnotationAreHandledCorrectlyOnClasses() {
        assertThat(getMetaAnnotations(ChildClass.class, MetaAnnotation.class))
            .containsExactly(
                newMetaAnnotation("child"),
                newMetaAnnotation("parent")
            );
    }

    @Test
    public void inheritedAnnotationAreHandledCorrectlyOnMethods() throws Throwable {
        assertThat(getMetaAnnotations(ChildClass.METHOD, MetaAnnotation.class))
            .containsExactly(
                newMetaAnnotation("child"),
                newMetaAnnotation("parent")
            );
    }

    @Test
    @SuppressFBWarnings("CLI_CONSTANT_LIST_INDEX")
    public void inheritedAnnotationAreHandledCorrectlyOnParameters() throws Throwable {
        assertThat(getMetaAnnotations(ChildClass.METHOD.getParameters()[1], MetaAnnotation.class))
            .containsExactly(
                newMetaAnnotation("child"),
                newMetaAnnotation("parent")
            );
    }

    @Test
    public void metaAnnotationsOnAnnotationWithValueAreHandledCorrectly() {
        assertThat(getMetaAnnotations(MetaAnnotationWithValuesAliasContainer.class, MetaAnnotation.class))
            .containsExactly(newMetaAnnotation(100, "container"));
    }


    @NotNull
    private static MetaAnnotation newMetaAnnotation(int integer) {
        return newAnnotationInstance(MetaAnnotation.class, singletonMap("integer", integer));
    }

    @NotNull
    private static MetaAnnotation newMetaAnnotation(@NotNull String string) {
        return newAnnotationInstance(MetaAnnotation.class, singletonMap("string", string));
    }

    @NotNull
    private static MetaAnnotation newMetaAnnotation(int integer, @NotNull String string) {
        return newAnnotationInstance(MetaAnnotation.class, ImmutableMap.of(
            "integer", integer,
            "string", string
        ));
    }


    @NotNull
    private static RepeatableAnnotation newRepeatableAnnotation(int value) {
        return newAnnotationInstance(RepeatableAnnotation.class, singletonMap("value", value));
    }

}
