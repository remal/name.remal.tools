package name.remal.annotation;

import static java.lang.String.format;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static name.remal.ArrayUtils.arrayToString;
import static name.remal.annotation.AnnotationUtils.getAttributeMethods;
import static name.remal.annotation.AnnotationUtils.newAnnotationInstance;
import static org.assertj.core.api.Assertions.assertThat;

import java.lang.annotation.Retention;
import java.lang.reflect.Method;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

public class AnnotationUtils_newAnnotationInstanceTest {

    private final AllTypesAnnotation synthesizedAnnotation = newAnnotationInstance(AllTypesAnnotation.class, null);

    private final AllTypesAnnotation nativeAnnotation = AnnotatedClass.class.getDeclaredAnnotation(AllTypesAnnotation.class);

    @Test
    public void synthesizedAnnotationEqualsToNative() {
        assertThat(synthesizedAnnotation).isEqualTo(nativeAnnotation);
    }

    @Test
    public void synthesizedAnnotationsHashCodeEqualsToNativesOne() {
        assertThat(synthesizedAnnotation.hashCode()).isEqualTo(nativeAnnotation.hashCode());
    }

    @Test
    public void synthesizedAnnotationReturnsItsClassByCallingAnnotationType() {
        assertThat(synthesizedAnnotation.annotationType()).isEqualTo(AllTypesAnnotation.class);
    }

    @Test
    public void synthesizedAnnotationsStringRepresentationIsInSpecificFormat() throws Throwable {
        assertThat(synthesizedAnnotation.toString())
            .isNotBlank()
            .startsWith("@" + synthesizedAnnotation.annotationType().getName() + "(")
            .endsWith(")");

        for (Method method : getAttributeMethods(synthesizedAnnotation.annotationType())) {
            assertThat(synthesizedAnnotation.toString()).contains(format("%s=%s", method.getName(), arrayToString(method.invoke(synthesizedAnnotation))));
        }
    }

    @AllTypesAnnotation
    private interface AnnotatedClass {
    }

    @Retention(RUNTIME)
    private @interface AllTypesAnnotation {

        byte byteValue() default 1;

        @NotNull byte[] byteArray() default {1};

        byte charValue() default '2';

        @NotNull char[] charArray() default {'2'};

        short shortValue() default 3;

        @NotNull short[] shortArray() default {3};

        int intValue() default 4;

        @NotNull int[] intArray() default {4};

        long longValue() default 5;

        @NotNull long[] longArray() default {5};

        boolean booleanValue() default true;

        @NotNull boolean[] booleanArray() default {true};

        @NotNull String stringValue() default "string";

        @NotNull String[] stringArray() default {""};

        @NotNull TestAnnotation annotationValue() default @TestAnnotation;

        @NotNull TestAnnotation[] annotationArray() default {@TestAnnotation};

        @NotNull TestEnum enumValue() default TestEnum.ONE;

        @NotNull TestEnum[] enumArray() default {TestEnum.TWO};

        @NotNull Class classValue() default AllTypesAnnotation.class;

        @NotNull Class[] classArray() default {AllTypesAnnotation.class};

    }

    @Retention(RUNTIME)
    private @interface TestAnnotation {
    }

    private enum TestEnum {ONE, TWO}

}
