package name.remal.annotation;

import static java.lang.annotation.RetentionPolicy.CLASS;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;
import org.jetbrains.annotations.NotNull;

@SuppressFBWarnings("COM_COPIED_OVERRIDDEN_METHOD")
@SuppressWarnings("EmptyMethod")
public abstract class BaseMetaAnnotationsTest {

    @AllTypesAnnotation(
        byteValue = 1,
        booleanValue = true,
        charValue = '1',
        shortValue = 2,
        intValue = 3,
        longValue = 4,
        floatValue = 5,
        doubleValue = 6,
        stringValue = "a",
        enumValue = RUNTIME,
        annotationValue = @Retention(RUNTIME),
        byteValues = {10, 10},
        booleanValues = {true, true},
        charValues = {'2', '2'},
        shortValues = {20, 20},
        intValues = {30, 30},
        longValues = {40, 40},
        floatValues = {50, 50},
        doubleValues = {60, 60},
        stringValues = {"b", "b"},
        enumValues = {CLASS, CLASS},
        annotationValues = {@Retention(CLASS), @Retention(CLASS)}
    )
    protected static class AllTypesAnnotationClass {
    }

    @Retention(RUNTIME)
    protected @interface AllTypesAnnotation {

        byte byteValue();

        boolean booleanValue();

        char charValue();

        short shortValue();

        int intValue();

        long longValue();

        float floatValue();

        double doubleValue();

        @NotNull String stringValue();

        @NotNull RetentionPolicy enumValue();

        @NotNull Retention annotationValue();

        @NotNull byte[] byteValues();

        @NotNull boolean[] booleanValues();

        @NotNull char[] charValues();

        @NotNull short[] shortValues();

        @NotNull int[] intValues();

        @NotNull long[] longValues();

        @NotNull float[] floatValues();

        @NotNull double[] doubleValues();

        @NotNull String[] stringValues();

        @NotNull RetentionPolicy[] enumValues();

        @NotNull Retention[] annotationValues();

    }


    @MetaAnnotationIntAlias(10)
    @MetaAnnotationStringAlias("value")
    @MetaAnnotationAliases
    protected static class MetaAnnotationClass {
    }

    @MetaAnnotationStringAlias("parent")
    protected static class ParentClass {
        @MetaAnnotationStringAlias("parent")
        public void method(String param1, @MetaAnnotationStringAlias("parent") String param2) {
        }
    }

    @MetaAnnotationStringAlias("child")
    protected static class ChildClass extends ParentClass {
        protected static final Method METHOD;

        static {
            try {
                METHOD = ChildClass.class.getDeclaredMethod("method", String.class, String.class);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        @MetaAnnotationStringAlias("child")
        public void method(String param1, @MetaAnnotationStringAlias("child") String param2) {
        }
    }

    @Inherited
    @Retention(RUNTIME)
    protected @interface MetaAnnotation {
        int integer() default 0;

        @NotNull String string() default "";
    }


    @MetaAnnotationIntAlias(Integer.MAX_VALUE)
    @MetaAnnotationStringAlias("")
    @Inherited
    @Retention(RUNTIME)
    protected @interface MetaAnnotationAliases {
        @AnnotationAttributeAlias(annotationClass = MetaAnnotationIntAlias.class, attributeName = "value")
        int integer() default -1;

        @NotNull
        @AnnotationAttributeAlias(annotationClass = MetaAnnotationStringAlias.class, attributeName = "value")
        String string() default "super";
    }

    @MetaAnnotation
    @Inherited
    @Retention(RUNTIME)
    protected @interface MetaAnnotationIntAlias {
        @AnnotationAttributeAlias(annotationClass = MetaAnnotation.class, attributeName = "integer")
        int value();
    }

    @MetaAnnotation
    @Inherited
    @Retention(RUNTIME)
    protected @interface MetaAnnotationStringAlias {
        @NotNull
        @AnnotationAttributeAlias(annotationClass = MetaAnnotation.class, attributeName = "string")
        String value();
    }

    @MetaAnnotation(integer = 100)
    @Retention(RUNTIME)
    protected @interface MetaAnnotationWithValuesAlias {
        @NotNull
        @AnnotationAttributeAlias(annotationClass = MetaAnnotation.class, attributeName = "string")
        String value();
    }

    @MetaAnnotationWithValuesAlias("container")
    protected static class MetaAnnotationWithValuesAliasContainer {
    }


    @RepeatableAnnotation(1)
    @RepeatableAnnotationList({@RepeatableAnnotation(1), @RepeatableAnnotation(2), @RepeatableAnnotation(3)})
    protected static class RepeatableAnnotationClass {
    }

    @Retention(RUNTIME)
    protected @interface RepeatableAnnotation {
        int value();
    }

    @Retention(RUNTIME)
    protected @interface RepeatableAnnotationList {
        @NotNull RepeatableAnnotation[] value();
    }


    @InheritedAnnotation("parent")
    protected static class InheritedAnnotationParentClass {
    }

    @InheritedAnnotation("interface")
    protected interface InheritedAnnotationInterface {
    }

    @InheritedAnnotation("child")
    protected static class InheritedAnnotationChildClass extends InheritedAnnotationParentClass implements InheritedAnnotationInterface {
    }

    @Inherited
    @Retention(RUNTIME)
    protected @interface InheritedAnnotation {
        @NotNull
        String value();
    }


    @DefaultValueAnnotation
    protected static class DefaultValueAnnotationClass {
    }

    @Inherited
    @Retention(RUNTIME)
    protected @interface DefaultValueAnnotation {
        @NotNull
        String value() default "string";
    }

}
