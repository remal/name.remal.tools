package name.remal.annotation.bytecode;

import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableMap;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import name.remal.annotation.BaseMetaAnnotationsTest;
import org.junit.Before;
import org.junit.Test;

public class BytecodeAnnotationsScannerTest extends BaseMetaAnnotationsTest {

    private BytecodeAnnotationsScanner scanner;

    @Before
    public void before() {
        scanner = new BytecodeAnnotationsScanner(BytecodeAnnotationsScannerTest.class.getClassLoader());
    }


    @Test
    public void simpleAnnotationsAreHandledForClasses() {
        assertThat(scanner.getMetaAnnotations(AllTypesAnnotationClass.class, AllTypesAnnotation.class))
            .containsExactly(new BytecodeAnnotationAnnotationValue(AllTypesAnnotation.class.getName(), ImmutableMap.<String, BytecodeAnnotationValue>builder()
                .put("byteValue", new BytecodeAnnotationByteValue((byte) 1))
                .put("booleanValue", new BytecodeAnnotationBooleanValue(true))
                .put("charValue", new BytecodeAnnotationCharValue('1'))
                .put("shortValue", new BytecodeAnnotationShortValue((short) 2))
                .put("intValue", new BytecodeAnnotationIntValue(3))
                .put("longValue", new BytecodeAnnotationLongValue(4))
                .put("floatValue", new BytecodeAnnotationFloatValue(5))
                .put("doubleValue", new BytecodeAnnotationDoubleValue(6))
                .put("stringValue", new BytecodeAnnotationStringValue("a"))
                .put("enumValue", new BytecodeAnnotationEnumValue(RetentionPolicy.class.getName(), "RUNTIME"))
                .put("annotationValue", new BytecodeAnnotationAnnotationValue(Retention.class.getName(), ImmutableMap.<String, BytecodeAnnotationValue>builder()
                    .put("value", new BytecodeAnnotationEnumValue(RetentionPolicy.class.getName(), "RUNTIME"))
                    .build()))
                .put("byteValues", new BytecodeAnnotationBytesArrayValue((byte) 10, (byte) 10))
                .put("booleanValues", new BytecodeAnnotationBooleansArrayValue(true, true))
                .put("charValues", new BytecodeAnnotationCharsArrayValue('2', '2'))
                .put("shortValues", new BytecodeAnnotationShortsArrayValue((short) 20, (short) 20))
                .put("intValues", new BytecodeAnnotationIntsArrayValue(30, 30))
                .put("longValues", new BytecodeAnnotationLongsArrayValue(40, 40))
                .put("floatValues", new BytecodeAnnotationFloatsArrayValue(50, 50))
                .put("doubleValues", new BytecodeAnnotationDoublesArrayValue(60.0, 60.0))
                .put("stringValues", new BytecodeAnnotationStringsArrayValue("b", "b"))
                .put("enumValues", new BytecodeAnnotationEnumsArrayValue(
                    new BytecodeAnnotationEnumValue(RetentionPolicy.class.getName(), "CLASS"),
                    new BytecodeAnnotationEnumValue(RetentionPolicy.class.getName(), "CLASS")
                ))
                .put("annotationValues", new BytecodeAnnotationAnnotationsArrayValue(
                    new BytecodeAnnotationAnnotationValue(Retention.class.getName(), ImmutableMap.<String, BytecodeAnnotationValue>builder()
                        .put("value", new BytecodeAnnotationEnumValue(RetentionPolicy.class.getName(), "CLASS"))
                        .build()),
                    new BytecodeAnnotationAnnotationValue(Retention.class.getName(), ImmutableMap.<String, BytecodeAnnotationValue>builder()
                        .put("value", new BytecodeAnnotationEnumValue(RetentionPolicy.class.getName(), "CLASS"))
                        .build())
                ))
                .build()));
    }

    @Test
    public void repeatableAnnotationsHandledForClasses() {
        assertThat(scanner.getMetaAnnotations(RepeatableAnnotationClass.class, RepeatableAnnotation.class))
            .containsExactly(
                new BytecodeAnnotationAnnotationValue(RepeatableAnnotation.class.getName(), singletonMap("value", new BytecodeAnnotationIntValue(1))),
                new BytecodeAnnotationAnnotationValue(RepeatableAnnotation.class.getName(), singletonMap("value", new BytecodeAnnotationIntValue(2))),
                new BytecodeAnnotationAnnotationValue(RepeatableAnnotation.class.getName(), singletonMap("value", new BytecodeAnnotationIntValue(3)))
            );
    }

    @Test
    public void inheritedAnnotationsHandledForClasses() {
        assertThat(scanner.getMetaAnnotations(InheritedAnnotationChildClass.class, InheritedAnnotation.class))
            .containsExactly(
                new BytecodeAnnotationAnnotationValue(InheritedAnnotation.class.getName(), singletonMap("value", new BytecodeAnnotationStringValue("child"))),
                new BytecodeAnnotationAnnotationValue(InheritedAnnotation.class.getName(), singletonMap("value", new BytecodeAnnotationStringValue("parent"))),
                new BytecodeAnnotationAnnotationValue(InheritedAnnotation.class.getName(), singletonMap("value", new BytecodeAnnotationStringValue("interface")))
            );
    }

    @Test
    public void defaultValuesArrHandledForClasses() {
        assertThat(scanner.getMetaAnnotations(DefaultValueAnnotationClass.class, DefaultValueAnnotation.class))
            .containsExactly(
                new BytecodeAnnotationAnnotationValue(DefaultValueAnnotation.class.getName(), singletonMap("value", new BytecodeAnnotationStringValue("string")))
            );
    }

    @Test
    public void metaAnnotationsAreHandledForClasses() {
        assertThat(scanner.getMetaAnnotations(MetaAnnotationClass.class, MetaAnnotation.class))
            .containsExactly(
                new BytecodeAnnotationAnnotationValue(MetaAnnotation.class.getName(), ImmutableMap.<String, BytecodeAnnotationValue>builder()
                    .put("integer", new BytecodeAnnotationIntValue(10))
                    .put("string", new BytecodeAnnotationStringValue(""))
                    .build()),
                new BytecodeAnnotationAnnotationValue(MetaAnnotation.class.getName(), ImmutableMap.<String, BytecodeAnnotationValue>builder()
                    .put("integer", new BytecodeAnnotationIntValue(0))
                    .put("string", new BytecodeAnnotationStringValue("value"))
                    .build()),
                new BytecodeAnnotationAnnotationValue(MetaAnnotation.class.getName(), ImmutableMap.<String, BytecodeAnnotationValue>builder()
                    .put("integer", new BytecodeAnnotationIntValue(-1))
                    .put("string", new BytecodeAnnotationStringValue(""))
                    .build()),
                new BytecodeAnnotationAnnotationValue(MetaAnnotation.class.getName(), ImmutableMap.<String, BytecodeAnnotationValue>builder()
                    .put("integer", new BytecodeAnnotationIntValue(0))
                    .put("string", new BytecodeAnnotationStringValue("super"))
                    .build())
            );
    }

}
