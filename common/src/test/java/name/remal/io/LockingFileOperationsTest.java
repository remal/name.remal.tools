package name.remal.io;

import static java.io.File.createTempFile;
import static org.assertj.core.api.Assertions.assertThat;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.File;
import java.time.Duration;
import org.junit.Test;

public class LockingFileOperationsTest {

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressFBWarnings("RV_RETURN_VALUE_IGNORED_BAD_PRACTICE")
    public void testLockAndDelete() throws Throwable {
        File tempFile = createTempFile("test-", ".temp");
        try {
            assertThat(tempFile).exists();
            LockingFileOperations.lockAndDelete(tempFile, Duration.ofSeconds(1));
            assertThat(tempFile).doesNotExist();

        } finally {
            tempFile.delete();
        }
    }

}
