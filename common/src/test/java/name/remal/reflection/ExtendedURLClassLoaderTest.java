package name.remal.reflection;

import static java.nio.file.Files.newOutputStream;
import static java.util.Collections.list;
import static java.util.jar.Attributes.Name;
import static java.util.jar.Attributes.Name.MANIFEST_VERSION;
import static java.util.jar.JarFile.MANIFEST_NAME;
import static java.util.stream.Collectors.toList;
import static name.remal.reflection.ExtendedURLClassLoader.LoadingOrder.PARENT_FIRST;
import static name.remal.reflection.ExtendedURLClassLoader.LoadingOrder.PARENT_ONLY;
import static name.remal.reflection.ExtendedURLClassLoader.LoadingOrder.THIS_FIRST;
import static name.remal.reflection.ExtendedURLClassLoader.LoadingOrder.THIS_ONLY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.objectweb.asm.ClassWriter.COMPUTE_FRAMES;
import static org.objectweb.asm.ClassWriter.COMPUTE_MAXS;
import static org.objectweb.asm.Opcodes.ACC_ABSTRACT;
import static org.objectweb.asm.Opcodes.ACC_INTERFACE;
import static org.objectweb.asm.Opcodes.V1_8;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.function.Predicate;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import org.jetbrains.annotations.NotNull;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

@SuppressWarnings("ConstantConditions")
@SuppressFBWarnings("PRMC_POSSIBLY_REDUNDANT_METHOD_CALLS")
public class ExtendedURLClassLoaderTest {

    private static final String TEST_CLASS_NAME = "test.TestClass";
    private static final String TEST_CLASS_RESOURCE_NAME = TEST_CLASS_NAME.replace('.', '/') + ".class";

    private static final String TEST_PARENT_CLASS_NAME = "test.TestParentClass";
    private static final String TEST_PARENT_CLASS_RESOURCE_NAME = TEST_PARENT_CLASS_NAME.replace('.', '/') + ".class";

    private static final String TEST_CHILD_CLASS_NAME = "test.TestChildClass";
    private static final String TEST_CHILD_CLASS_RESOURCE_NAME = TEST_CHILD_CLASS_NAME.replace('.', '/') + ".class";


    @ClassRule
    public static final TemporaryFolder temporaryFolder = new TemporaryFolder();

    private static File parentClassLoaderFile;

    private static File classLoaderFile;


    @Test
    public void testParentFirstLoadClass() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(PARENT_FIRST, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                assertSame(String.class.getClassLoader(), classLoader.loadClass(String.class.getName()).getClassLoader());
                assertSame(parentClassLoader, classLoader.loadClass(TEST_CLASS_NAME).getClassLoader());
                assertSame(parentClassLoader, classLoader.loadClass(TEST_PARENT_CLASS_NAME).getClassLoader());
                assertSame(classLoader, classLoader.loadClass(TEST_CHILD_CLASS_NAME).getClassLoader());
            }
        }
    }

    @Test
    public void testThisFirstLoadClass() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(THIS_FIRST, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                assertSame(String.class.getClassLoader(), classLoader.loadClass(String.class.getName()).getClassLoader());
                assertSame(classLoader, classLoader.loadClass(TEST_CLASS_NAME).getClassLoader());
                assertSame(parentClassLoader, classLoader.loadClass(TEST_PARENT_CLASS_NAME).getClassLoader());
                assertSame(classLoader, classLoader.loadClass(TEST_CHILD_CLASS_NAME).getClassLoader());
            }
        }
    }

    @Test
    public void testParentOnlyLoadClass() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(PARENT_ONLY, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                assertSame(String.class.getClassLoader(), classLoader.loadClass(String.class.getName()).getClassLoader());

                assertSame(parentClassLoader, classLoader.loadClass(TEST_CLASS_NAME).getClassLoader());

                assertSame(parentClassLoader, classLoader.loadClass(TEST_PARENT_CLASS_NAME).getClassLoader());

                try {
                    assertSame(classLoader, classLoader.loadClass(TEST_CHILD_CLASS_NAME).getClassLoader());
                    fail();
                } catch (ClassNotFoundException ignored) {
                    // OK
                }
            }
        }
    }

    @Test
    public void testThisOnlyLoadClass() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(THIS_ONLY, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                assertSame(String.class.getClassLoader(), classLoader.loadClass(String.class.getName()).getClassLoader());

                assertSame(classLoader, classLoader.loadClass(TEST_CLASS_NAME).getClassLoader());

                try {
                    assertSame(parentClassLoader, classLoader.loadClass(TEST_PARENT_CLASS_NAME).getClassLoader());
                    fail();
                } catch (ClassNotFoundException ignored) {
                    // OK
                }

                assertSame(classLoader, classLoader.loadClass(TEST_CHILD_CLASS_NAME).getClassLoader());
            }
        }
    }


    @Test
    public void testParentFirstGetResource() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(PARENT_FIRST, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                assertTrue(classLoader.getResource(TEST_CLASS_RESOURCE_NAME).getFile().startsWith(parentClassLoaderFile.toURI().toString() + "!"));
                assertTrue(classLoader.getResource(TEST_PARENT_CLASS_RESOURCE_NAME).getFile().startsWith(parentClassLoaderFile.toURI().toString() + "!"));
                assertTrue(classLoader.getResource(TEST_CHILD_CLASS_RESOURCE_NAME).getFile().startsWith(classLoaderFile.toURI().toString() + "!"));
            }
        }
    }

    @Test
    public void testThisFirstGetResource() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(THIS_FIRST, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                assertTrue(classLoader.getResource(TEST_CLASS_RESOURCE_NAME).getFile().startsWith(classLoaderFile.toURI().toString() + "!"));
                assertTrue(classLoader.getResource(TEST_PARENT_CLASS_RESOURCE_NAME).getFile().startsWith(parentClassLoaderFile.toURI().toString() + "!"));
                assertTrue(classLoader.getResource(TEST_CHILD_CLASS_RESOURCE_NAME).getFile().startsWith(classLoaderFile.toURI().toString() + "!"));
            }
        }
    }

    @Test
    public void testParentOnlyGetResource() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(PARENT_ONLY, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                assertTrue(classLoader.getResource(TEST_CLASS_RESOURCE_NAME).getFile().startsWith(parentClassLoaderFile.toURI().toString() + "!"));
                assertTrue(classLoader.getResource(TEST_PARENT_CLASS_RESOURCE_NAME).getFile().startsWith(parentClassLoaderFile.toURI().toString() + "!"));
                assertNull(classLoader.getResource(TEST_CHILD_CLASS_RESOURCE_NAME));
            }
        }
    }

    @Test
    public void testThisOnlyGetResource() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(THIS_ONLY, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                assertTrue(classLoader.getResource(TEST_CLASS_RESOURCE_NAME).getFile().startsWith(classLoaderFile.toURI().toString() + "!"));
                assertNull(classLoader.getResource(TEST_PARENT_CLASS_RESOURCE_NAME));
                assertTrue(classLoader.getResource(TEST_CHILD_CLASS_RESOURCE_NAME).getFile().startsWith(classLoaderFile.toURI().toString() + "!"));
            }
        }
    }


    @Test
    public void testParentFirstGetResources() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(PARENT_FIRST, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                List<Manifest> manifests = list(classLoader.getResources(MANIFEST_NAME)).stream().map(ExtendedURLClassLoaderTest::readManifest).collect(toList());
                assertTrue(manifests.size() > 2);
                assertEquals(manifests.size() - 2, indexOf(manifests, it -> "parent".equals(it.getMainAttributes().getValue(ExtendedURLClassLoaderTest.class.getSimpleName()))));
                assertEquals(manifests.size() - 1, indexOf(manifests, it -> "child".equals(it.getMainAttributes().getValue(ExtendedURLClassLoaderTest.class.getSimpleName()))));
            }
        }
    }

    @Test
    public void testThisFirstGetResources() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(THIS_FIRST, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                List<Manifest> manifests = list(classLoader.getResources(MANIFEST_NAME)).stream().map(ExtendedURLClassLoaderTest::readManifest).collect(toList());
                assertTrue(manifests.size() > 2);
                assertEquals(manifests.size() - 1, indexOf(manifests, it -> "parent".equals(it.getMainAttributes().getValue(ExtendedURLClassLoaderTest.class.getSimpleName()))));
                assertEquals(0, indexOf(manifests, it -> "child".equals(it.getMainAttributes().getValue(ExtendedURLClassLoaderTest.class.getSimpleName()))));
            }
        }
    }

    @Test
    public void testParentOnlyGetResources() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(PARENT_ONLY, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                List<Manifest> manifests = list(classLoader.getResources(MANIFEST_NAME)).stream().map(ExtendedURLClassLoaderTest::readManifest).collect(toList());
                assertTrue(manifests.size() > 1);
                assertEquals(manifests.size() - 1, indexOf(manifests, it -> "parent".equals(it.getMainAttributes().getValue(ExtendedURLClassLoaderTest.class.getSimpleName()))));
                assertEquals(-1, indexOf(manifests, it -> "child".equals(it.getMainAttributes().getValue(ExtendedURLClassLoaderTest.class.getSimpleName()))));
            }
        }
    }

    @Test
    public void testThisOnlyGetResources() throws Exception {
        try (URLClassLoader parentClassLoader = new URLClassLoader(new URL[]{parentClassLoaderFile.toURI().toURL()})) {
            try (URLClassLoader classLoader = new ExtendedURLClassLoader(THIS_ONLY, new URL[]{classLoaderFile.toURI().toURL()}, parentClassLoader)) {
                List<Manifest> manifests = list(classLoader.getResources(MANIFEST_NAME)).stream().map(ExtendedURLClassLoaderTest::readManifest).collect(toList());
                assertEquals(1, manifests.size());
                assertEquals(-1, indexOf(manifests, it -> "parent".equals(it.getMainAttributes().getValue(ExtendedURLClassLoaderTest.class.getSimpleName()))));
                assertEquals(0, indexOf(manifests, it -> "child".equals(it.getMainAttributes().getValue(ExtendedURLClassLoaderTest.class.getSimpleName()))));
            }
        }
    }


    @BeforeClass
    public static void beforeClass() throws Exception {
        try (OutputStream fileOutputStream = newOutputStream((parentClassLoaderFile = temporaryFolder.newFile("parent.jar")).toPath())) {
            Manifest manifest = new Manifest();
            manifest.getMainAttributes().put(MANIFEST_VERSION, "1.0");
            manifest.getMainAttributes().put(new Name(ExtendedURLClassLoaderTest.class.getSimpleName()), "parent");
            try (JarOutputStream jarOutputStream = new JarOutputStream(fileOutputStream, manifest)) {
                jarOutputStream.putNextEntry(new JarEntry(TEST_CLASS_RESOURCE_NAME));
                jarOutputStream.write(generateBytecode(TEST_CLASS_NAME));

                jarOutputStream.putNextEntry(new JarEntry(TEST_PARENT_CLASS_RESOURCE_NAME));
                jarOutputStream.write(generateBytecode(TEST_PARENT_CLASS_NAME));
            }
        }

        try (OutputStream fileOutputStream = newOutputStream((classLoaderFile = temporaryFolder.newFile("child.jar")).toPath())) {
            Manifest manifest = new Manifest();
            manifest.getMainAttributes().put(MANIFEST_VERSION, "1.0");
            manifest.getMainAttributes().put(new Name(ExtendedURLClassLoaderTest.class.getSimpleName()), "child");
            try (JarOutputStream jarOutputStream = new JarOutputStream(fileOutputStream, manifest)) {
                jarOutputStream.putNextEntry(new JarEntry(TEST_CLASS_RESOURCE_NAME));
                jarOutputStream.write(generateBytecode(TEST_CLASS_NAME));

                jarOutputStream.putNextEntry(new JarEntry(TEST_CHILD_CLASS_RESOURCE_NAME));
                jarOutputStream.write(generateBytecode(TEST_CHILD_CLASS_NAME));
            }
        }
    }

    @NotNull
    private static byte[] generateBytecode(@NotNull String className) {
        ClassNode classNode = new ClassNode();
        classNode.version = V1_8;
        classNode.access = ACC_ABSTRACT | ACC_INTERFACE;
        classNode.name = className.replace('.', '/');
        classNode.superName = "java/lang/Object";

        ClassWriter classWriter = new ClassWriter(COMPUTE_MAXS | COMPUTE_FRAMES);
        classNode.accept(classWriter);
        return classWriter.toByteArray();
    }

    @NotNull
    private static Manifest readManifest(@NotNull URL url) {
        try {
            return new Manifest(url.openStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static <T> int indexOf(@NotNull List<T> list, @NotNull Predicate<T> predicate) {
        for (int i = 0; i < list.size(); ++i) {
            if (predicate.test(list.get(i))) {
                return i;
            }
        }
        return -1;
    }

}
