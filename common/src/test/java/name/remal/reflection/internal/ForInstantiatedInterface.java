package name.remal.reflection.internal;

import org.jetbrains.annotations.Nullable;

public interface ForInstantiatedInterface {

    void assertForInstantiated(@Nullable ClassLoader rootClassLoader);

    void assertForInstantiatedWithPropagatedPackage(@Nullable ClassLoader rootClassLoader);

}
