package name.remal.reflection.internal.impl;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import name.remal.reflection.internal.ForInstantiatedInterface;
import org.jetbrains.annotations.Nullable;

public class ForInstantiatedImpl implements ForInstantiatedInterface {

    @Override
    public void assertForInstantiated(@Nullable ClassLoader rootClassLoader) {
        assertSame(rootClassLoader, ForInstantiatedInterface.class.getClassLoader());
        assertNotSame(rootClassLoader, ForInstantiatedImpl.class.getClassLoader());
        assertNotSame(rootClassLoader, this.getClass().getClassLoader());
    }

    @Override
    public void assertForInstantiatedWithPropagatedPackage(@Nullable ClassLoader rootClassLoader) {
        assertForInstantiated(rootClassLoader);
        assertSame(ForInstantiatedImpl.class.getClassLoader(), ForInstantiatedSibling.class.getClassLoader());
        assertNotSame(rootClassLoader, ForInstantiatedSibling.class.getClassLoader());
    }

}
