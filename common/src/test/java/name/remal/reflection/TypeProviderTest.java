package name.remal.reflection;

import static org.junit.Assert.assertEquals;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.junit.Test;

@SuppressFBWarnings("SEC_SIDE_EFFECT_CONSTRUCTOR")
public class TypeProviderTest {

    @Test(expected = IllegalStateException.class)
    public void testNotParameterizedType() {
        new TypeProvider() {
        };
    }

    @Test
    public void testDirectChild() {
        assertEquals(String.class, new TypeProvider<String>() {
        }.getType());
    }


    private abstract class CharSequenceTypeProvider<T extends CharSequence> extends TypeProvider<T> {
    }

    @Test
    public void testIndirectChild() {
        assertEquals(String.class, new CharSequenceTypeProvider<String>() {
        }.getType());
    }

}
