package name.remal.reflection;

import static com.google.common.io.Files.createTempDir;
import static com.google.common.io.MoreFiles.deleteRecursively;
import static com.google.common.io.RecursiveDeleteOption.ALLOW_INSECURE;
import static java.nio.file.Files.createFile;
import static name.remal.SneakyThrow.sneakyThrow;
import static org.assertj.core.api.Assertions.assertThat;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import name.remal.reflection.internal.ForInstantiatedInterface;
import name.remal.reflection.internal.impl.ForInstantiatedImpl;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Test;

public class ClassLoaderUtilsTest {

    private static final String resourceName = "temp-resource.file";

    private final File tempDir = createTempDir().getAbsoluteFile();

    @NotNull
    private final URL resourceFileURL;

    {
        try {
            File resourceFile = new File(tempDir, resourceName);
            createFile(resourceFile.toPath());

            resourceFileURL = resourceFile.toURI().normalize().toURL();

        } catch (Throwable e) {
            throw sneakyThrow(e);
        }
    }

    @After
    public void deleteTempDir() throws IOException {
        deleteRecursively(tempDir.toPath(), ALLOW_INSECURE);
    }

    @Test
    public void systemClassLoaderAddUrl() throws Throwable {
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        assertThat(systemClassLoader).isNotNull();
        ClassLoaderUtils.addURLsToClassLoader(systemClassLoader, tempDir.toURI().toURL());
        URL resourceURL = systemClassLoader.getResource(resourceName);
        assertThat(resourceURL).isNotNull();
        assertThat(resourceURL.toURI().normalize().toURL()).isEqualTo(resourceFileURL);
    }

    @Test
    @SuppressFBWarnings("DP_CREATE_CLASSLOADER_INSIDE_DO_PRIVILEGED")
    public void customClassLoaderAddUrl() throws Throwable {
        ClassLoader classLoader = new URLClassLoader(new URL[]{});
        ClassLoaderUtils.addURLsToClassLoader(classLoader, tempDir.toURI().toURL());
        {
            URL resourceURL = classLoader.getResource(resourceName);
            assertThat(resourceURL).isNotNull();
            assertThat(resourceURL.toURI().normalize().toURL()).isEqualTo(resourceFileURL);
        }
        {
            URL resourceURL = ClassLoader.getSystemClassLoader().getResource(resourceName);
            assertThat(resourceURL).isNull();
        }
    }

    @Test
    public void forInstantiated() {
        ClassLoader rootClassLoader = ForInstantiatedInterface.class.getClassLoader();
        ClassLoaderUtils.forInstantiated(rootClassLoader, ForInstantiatedInterface.class, ForInstantiatedImpl.class, impl -> {
            impl.assertForInstantiated(rootClassLoader);
        });
    }

    @Test
    public void forInstantiatedWithPropagatedPackage() {
        ClassLoader rootClassLoader = ForInstantiatedInterface.class.getClassLoader();
        ClassLoaderUtils.forInstantiatedWithPropagatedPackage(rootClassLoader, ForInstantiatedInterface.class, ForInstantiatedImpl.class, impl -> {
            impl.assertForInstantiatedWithPropagatedPackage(rootClassLoader);
        });
    }

}
