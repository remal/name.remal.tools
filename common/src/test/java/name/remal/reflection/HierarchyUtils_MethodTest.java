package name.remal.reflection;

import static name.remal.reflection.HierarchyUtils.getAllNotOverriddenMethods;
import static name.remal.reflection.HierarchyUtils.getHierarchy;
import static name.remal.reflection.HierarchyUtils.isOverriddenBy;
import static org.assertj.core.api.Assertions.assertThat;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.lang.reflect.Method;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

@SuppressFBWarnings("COM_COPIED_OVERRIDDEN_METHOD")
@SuppressWarnings("EmptyMethod")
public class HierarchyUtils_MethodTest {

    @Test
    public void hierarchyContainsSelf() {
        Method publicMethod = getPublicMethod(Child.class);
        assertThat(getHierarchy(publicMethod)).contains(publicMethod);
    }

    @Test
    public void hierarchyIsCorrectForNotGenericMethods() {
        assertThat(getHierarchy(getPublicMethod(Child.class)))
            .containsExactly(getPublicMethod(Child.class), getPublicMethod(Parent.class));
    }

    @Test
    public void privateMethodsCantBeOverridden() {
        assertThat(getHierarchy(getPrivateMethod(Child.class)))
            .containsExactly(getPrivateMethod(Child.class));
    }

    @Test
    public void genericMethodsAreHandledCorrectly() {
        assertThat(getHierarchy(getGenericMethod(Child.class)))
            .containsExactly(getGenericMethod(Child.class), getGenericMethod(Parent.class));
    }

    @Test
    public void objectMethodsHandledCorrectly() {
        assertThat(getHierarchy(getToStringMethod(Child.class)))
            .containsExactly(getToStringMethod(Child.class), getToStringMethod(Object.class));
    }

    @Test
    public void isOverriddenByWorksForSimpleMethods() {
        assertThat(isOverriddenBy(getPublicMethod(Parent.class), getPublicMethod(Child.class))).isTrue();
    }

    @Test
    public void isOverriddenByWorksForPrivateMethods() {
        assertThat(isOverriddenBy(getPrivateMethod(Parent.class), getPrivateMethod(Child.class))).isFalse();
    }

    @Test
    public void isOverriddenByWorksForGenericMethods() {
        assertThat(isOverriddenBy(getGenericMethod(Parent.class), getGenericMethod(Child.class))).isTrue();
    }

    @Test
    public void getAllNotOverriddenMethodsWorksCorrectly() {
        assertThat(getAllNotOverriddenMethods(Child.class))
            .contains(
                getPublicMethod(Child.class),
                getPrivateMethod(Child.class),
                getProtectedMethod(Child.class),
                getGenericMethod(Child.class),
                getToStringMethod(Child.class),
                getPrivateMethod(Parent.class)
            )
            .doesNotContain(
                getPublicMethod(Parent.class),
                getProtectedMethod(Parent.class),
                getGenericMethod(Parent.class),
                getToStringMethod(Object.class)
            )
        ;
    }

    @NotNull
    private static Method getPublicMethod(@NotNull Class clazz) {
        return getMethod(clazz, "publicMethod");
    }

    @NotNull
    private static Method getPrivateMethod(@NotNull Class clazz) {
        return getMethod(clazz, "privateMethod");
    }

    @NotNull
    private static Method getProtectedMethod(@NotNull Class clazz) {
        return getMethod(clazz, "protectedMethod");
    }

    @NotNull
    private static Method getGenericMethod(@NotNull Class clazz) {
        return getMethod(clazz, "genericMethod");
    }

    @NotNull
    private static Method getToStringMethod(@NotNull Class clazz) {
        return getMethod(clazz, "toString");
    }

    @NotNull
    private static Method getMethod(@NotNull Class clazz, @NotNull String name) {
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isSynthetic()) continue;
            if (method.getName().equals(name)) return method;
        }
        throw new IllegalStateException(clazz + "doesn't have method with name " + name);
    }

    private static class Parent<T> {
        public void publicMethod(Object value) {
        }

        private void privateMethod() {
        }

        protected void protectedMethod() {
        }

        public void genericMethod(T value) {
        }
    }

    private static class Child extends Parent<String> {
        public void publicMethod(Object value) {
        }

        private void privateMethod() {
        }

        protected void protectedMethod() {
        }

        public void genericMethod(String value) {
        }

        @NotNull
        public String toString() {
            return "child";
        }
    }

}
