package name.remal.reflection;

import static name.remal.reflection.HierarchyUtils.getHierarchy;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class HierarchyUtils_ClassTest {

    @Test
    public void hierarchyContainsSelf() {
        assertThat(getHierarchy(ChildClass.class)).contains(ChildClass.class);
        assertThat(getHierarchy(ChildInterface.class)).contains(ChildInterface.class);
    }

    @Test
    public void hierarchyContainsAllSuperclasses() {
        assertThat(getHierarchy(int.class)).containsSubsequence(int.class);
        assertThat(getHierarchy(Object.class)).containsSubsequence(Object.class);
        assertThat(getHierarchy(ChildClass.class)).containsSubsequence(ChildClass.class, ParentClass.class, Object.class);
    }

    @Test
    public void hierarchyContainsAllInterfaces() {
        assertThat(getHierarchy(ChildClass.class)).containsSubsequence(ChildInterface.class, ParentInterface.class);
        assertThat(getHierarchy(ChildClass.class)).containsSubsequence(TestInterface2.class, TestInterface1.class);
    }

    private interface TestInterface1 {
    }

    private interface TestInterface2 extends TestInterface1 {
    }

    private interface ParentInterface {
    }

    private interface ChildInterface extends ParentInterface {
    }

    private static class ParentClass implements ParentInterface {
    }

    private static class ChildClass extends ParentClass implements ChildInterface, TestInterface2 {
    }

}
