package name.remal;

import static name.remal.Codecs.decodeBase64;
import static name.remal.Codecs.decodeHex;
import static name.remal.Codecs.encodeBase64;
import static name.remal.Codecs.encodeHex;
import static name.remal.Codecs.md5;
import static name.remal.Codecs.newMd5Digest;
import static name.remal.Codecs.newSha256Digest;
import static name.remal.Codecs.newSha512Digest;
import static name.remal.Codecs.sha256;
import static name.remal.Codecs.sha512;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import org.junit.Test;

public class CodecsTest {

    @Test
    public void testHex() {
        byte[] data = new byte[]{-128, -70, -1, 0, 2, 10, 65, 127};
        assertArrayEquals(data, decodeHex(encodeHex(data)));
    }

    @Test
    public void testBase64() {
        byte[] data = new byte[]{-128, -70, -1, 0, 2, 10, 65, 127};
        assertArrayEquals(data, decodeBase64(encodeBase64(data)));
    }

    @Test
    public void testNewMd5Digest() {
        assertEquals("MD5", newMd5Digest().getAlgorithm());
    }

    @Test
    public void testMd5() {
        assertEquals("d41d8cd98f00b204e9800998ecf8427e", md5(new byte[0]));
        assertEquals("d41d8cd98f00b204e9800998ecf8427e", md5(""));
        assertEquals("d41d8cd98f00b204e9800998ecf8427e", md5(new ByteArrayInputStream(new byte[0])));

        assertEquals("1bc29b36f623ba82aaf6724fd3b16718", md5("md5"));
    }

    @Test
    public void testNewSha256Digest() {
        assertEquals("SHA-256", newSha256Digest().getAlgorithm());
    }

    @Test
    public void testSha256() {
        assertEquals("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", sha256(new byte[0]));
        assertEquals("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", sha256(""));
        assertEquals("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", sha256(new ByteArrayInputStream(new byte[0])));

        assertEquals("5d5b09f6dcb2d53a5fffc60c4ac0d55fabdf556069d6631545f42aa6e3500f2e", sha256("sha256"));
    }

    @Test
    public void testNewSha512Digest() {
        assertEquals("SHA-512", newSha512Digest().getAlgorithm());
    }

    @Test
    public void testSha512() {
        assertEquals("cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e", sha512(new byte[0]));
        assertEquals("cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e", sha512(""));
        assertEquals("cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e", sha512(new ByteArrayInputStream(new byte[0])));

        assertEquals("1f9720f871674c18e5fecff61d92c1355cd4bfac25699fb7ddfe7717c9669b4d085193982402156122dfaa706885fd64741704649795c65b2a5bdec40347e28a", sha512("sha512"));
    }

}
