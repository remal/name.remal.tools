package name.remal

import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.File
import java.nio.charset.StandardCharsets.UTF_8
import java.util.zip.ZipFile

class FileZipToTest {

    lateinit var tempDir: File

    @Before
    fun before() {
        tempDir = newTempDir(FileZipToTest::class.java.simpleName + "-", false)
    }

    @After
    fun after() {
        tempDir.deleteRecursively()
    }

    @Test
    fun testFileZipTo() {
        val source = tempDir.resolve("file.txt").apply { writeText("text", UTF_8) }
        val target = tempDir.resolve("file.zip")
        source.zipTo(target)

        ZipFile(target).use { zipFile ->
            val entry = zipFile.getEntry("file.txt")
            assertThat(entry).isNotNull
            val content = zipFile.getInputStream(entry).readBytes().toString(UTF_8)
            assertThat(content).isEqualTo("text")
        }
    }

    @Test
    fun testFileZipContentTo() {
        val source = tempDir.resolve("file.txt").apply { writeText("text", UTF_8) }
        val target = tempDir.resolve("file.zip")
        source.zipContentTo(target)

        ZipFile(target).use { zipFile ->
            val entry = zipFile.getEntry("file.txt")
            assertThat(entry).isNotNull
            val content = zipFile.getInputStream(entry).readBytes().toString(UTF_8)
            assertThat(content).isEqualTo("text")
        }
    }

    @Test
    fun testDirectoryZipTo() {
        val source = tempDir.resolve("dir").apply { resolve("file.txt").createParentDirectories().writeText("text", UTF_8) }
        val target = tempDir.resolve("file.zip")
        source.zipTo(target)

        ZipFile(target).use { zipFile ->
            zipFile.getEntry("dir").also {
                assertThat(it).isNotNull
                assertThat(it.isDirectory).isTrue()
            }
            zipFile.getEntry("dir/").also {
                assertThat(it).isNotNull
                assertThat(it.isDirectory).isTrue()
            }

            val fileEntry = zipFile.getEntry("dir/file.txt")
            assertThat(fileEntry).isNotNull
            val content = zipFile.getInputStream(fileEntry).readBytes().toString(UTF_8)
            assertThat(content).isEqualTo("text")
        }
    }

    @Test
    fun testDirectoryZipContentTo() {
        val source = tempDir.resolve("dir").apply { resolve("file.txt").createParentDirectories().writeText("text", UTF_8) }
        val target = tempDir.resolve("file.zip")
        source.zipContentTo(target)

        ZipFile(target).use { zipFile ->
            zipFile.getEntry("dir").also {
                assertThat(it).isNull()
            }
            zipFile.getEntry("dir/").also {
                assertThat(it).isNull()
            }

            val fileEntry = zipFile.getEntry("file.txt")
            assertThat(fileEntry).isNotNull
            val content = zipFile.getInputStream(fileEntry).readBytes().toString(UTF_8)
            assertThat(content).isEqualTo("text")
        }
    }

}
