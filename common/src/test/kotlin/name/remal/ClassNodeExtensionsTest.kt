package name.remal

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.objectweb.asm.Opcodes.ACC_PRIVATE
import org.objectweb.asm.Opcodes.ACC_PUBLIC
import org.objectweb.asm.tree.ClassNode

class ClassNodeExtensionsTest {

    @Test
    fun `isPublic and isPrivate`() {
        val classNode = ClassNode()
        assertEquals(0, classNode.access)
        assertFalse(classNode.isPublic)
        assertFalse(classNode.isPrivate)

        classNode.access = ACC_PUBLIC
        assertTrue(classNode.isPublic)
        assertFalse(classNode.isPrivate)

        classNode.access = ACC_PRIVATE
        assertFalse(classNode.isPublic)
        assertTrue(classNode.isPrivate)

        classNode.access = ACC_PUBLIC or ACC_PRIVATE
        assertTrue(classNode.isPublic)
        assertTrue(classNode.isPrivate)

        classNode.access = 0
        assertFalse(classNode.isPublic)
        assertFalse(classNode.isPrivate)

        classNode.isPublic = true
        assertEquals(ACC_PUBLIC, classNode.access)
        assertTrue(classNode.isPublic)
        assertFalse(classNode.isPrivate)

        classNode.access = 0
        classNode.isPrivate = true
        assertEquals(ACC_PRIVATE, classNode.access)
        assertFalse(classNode.isPublic)
        assertTrue(classNode.isPrivate)

        classNode.access = 0
        classNode.isPublic = true
        classNode.isPrivate = true
        assertEquals(ACC_PRIVATE, classNode.access)
        assertFalse(classNode.isPublic)
        assertTrue(classNode.isPrivate)

        classNode.access = 0
        classNode.isPublic = true
        classNode.isPrivate = true
        assertEquals(ACC_PRIVATE, classNode.access)
        classNode.isPrivate = false
        assertEquals(0, classNode.access)
        assertFalse(classNode.isPublic)
        assertFalse(classNode.isPrivate)
        classNode.isPublic = false
        assertEquals(0, classNode.access)
        assertFalse(classNode.isPublic)
        assertFalse(classNode.isPrivate)

        classNode.access = 0
        classNode.isPublic = true
        classNode.isPrivate = true
        assertEquals(ACC_PRIVATE, classNode.access)
        classNode.isPublic = false
        assertEquals(ACC_PRIVATE, classNode.access)
        assertFalse(classNode.isPublic)
        assertTrue(classNode.isPrivate)
        classNode.isPrivate = false
        assertEquals(0, classNode.access)
        assertFalse(classNode.isPublic)
        assertFalse(classNode.isPrivate)
    }

}
