package name.remal

import org.junit.Assert.assertEquals
import org.junit.Test
import java.time.ZoneId
import java.util.Date

class DateUtilsTest {

    @Test
    fun testLocalDateTime() {
        val date = Date()
        val dateTime = date.toLocalDateTime()
        assertEquals(date.time / 1000, dateTime.toEpochSecond(ZoneId.systemDefault().rules.getOffset(dateTime)))
    }

    @Test
    fun testZonedDateTime() {
        val date = Date()
        val dateTime = date.toZonedDateTime()
        assertEquals(date.time / 1000, dateTime.toEpochSecond())
    }

    @Test
    fun testOffsetDateTime() {
        val date = Date()
        val dateTime = date.toOffsetDateTime()
        assertEquals(date.time / 1000, dateTime.toEpochSecond())
    }

}
