package name.remal

import org.junit.Assert
import org.junit.Test
import java.math.BigInteger

class ClassExtensionsTest {

    @Test
    fun findCompatibleMethodSimple() {
        Assert.assertEquals(
            Methods::class.java.getMethod("simple"),
            Methods::class.java.findCompatibleMethod(Int::class.java, "simple")
        )
        Assert.assertEquals(
            Methods::class.java.getMethod("simple", Any::class.java),
            Methods::class.java.findCompatibleMethod(Int::class.java, "simple", BigInteger::class.java)
        )
        Assert.assertEquals(
            Methods::class.java.getMethod("simple", CharSequence::class.java),
            Methods::class.java.findCompatibleMethod(Int::class.java, "simple", CharSequence::class.java)
        )
        Assert.assertEquals(
            Methods::class.java.getMethod("simple", String::class.java, Collection::class.java),
            Methods::class.java.findCompatibleMethod("simple", String::class.java, List::class.java)
        )
    }

    @Test
    fun findCompatibleMethodReturnValue() {
        Assert.assertEquals(
            Methods::class.java.getMethod("retVal"),
            Methods::class.java.findCompatibleMethod(CharSequence::class.java, "retVal")
        )
    }

    @Suppress("UNUSED_PARAMETER")
    class Methods {

        fun simple(): Int = TODO()

        fun simple(param: Any): Int = TODO()
        fun simple(param: CharSequence): Int = TODO()

        fun simple(param1: String, param2: Collection<*>): Int = TODO()

        fun retVal(): String = TODO()

    }

}
