package name.remal

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class RetryTest {

    class RetriableException : RuntimeException()

    @Test
    fun testFailedRetry() {
        var counter = 0
        val throwable: Throwable?
        try {
            retry(5, RetriableException::class.java) {
                ++counter
                throw RetriableException()
            }
        } catch (e: Throwable) {
            throwable = e
        }

        assertEquals(5, counter)
        assertTrue(throwable is RetriableException)
    }

    @Test
    fun testSuccessRetry() {
        var counter = 0
        var result: String? = null
        var throwable: Throwable? = null
        try {
            result = retry(5, RetriableException::class.java) {
                ++counter
                if (counter < 5) throw RetriableException()
                return@retry "result"
            }
        } catch (e: Throwable) {
            throwable = e
        }

        assertEquals(5, counter)
        assertEquals("result", result)
        assertFalse(throwable is RetriableException)
    }

}
