package name.remal

import org.jdom2.input.SAXBuilder
import org.jdom2.output.XMLOutputter
import org.junit.Assert.assertEquals
import org.junit.Test

class JDOMUtilsTest {

    @Test
    fun testDocumentClearNamespaces() {
        val documentWithNS = JDOMUtilsTest::class.java.getResourceAsStream("testDocumentClearNamespaces-with.xml").use { SAXBuilder().build(it) }
        val documentWithoutNS = JDOMUtilsTest::class.java.getResourceAsStream("testDocumentClearNamespaces-without.xml").use { SAXBuilder().build(it) }
        println(XMLOutputter().outputString(documentWithoutNS))
        println(XMLOutputter().outputString(documentWithNS))
        assertEquals(
            XMLOutputter().outputString(documentWithoutNS),
            XMLOutputter().outputString(documentWithNS.clearNamespaces())
        )
    }

}
