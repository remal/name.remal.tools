package name.remal.annotation;

import java.lang.annotation.Annotation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ErrorCreatingAnnotationInstanceException extends RuntimeException {

    private static String formatMessage(@NotNull Class<? extends Annotation> type, @Nullable String message) {
        StringBuilder sb = new StringBuilder();
        sb.append("Error creating annotation ").append(type.getName()).append(" instance");
        if (message != null && !message.isEmpty()) {
            sb.append(": ").append(message);
        }
        return sb.toString();
    }

    public ErrorCreatingAnnotationInstanceException(@NotNull Class<? extends Annotation> type) {
        super(formatMessage(type, null));
    }

    public ErrorCreatingAnnotationInstanceException(@NotNull Class<? extends Annotation> type, @NotNull String message) {
        super(formatMessage(type, message));
    }

    public ErrorCreatingAnnotationInstanceException(@NotNull Class<? extends Annotation> type, @NotNull String message, @NotNull Throwable cause) {
        super(formatMessage(type, message), cause);
    }

    public ErrorCreatingAnnotationInstanceException(@NotNull Class<? extends Annotation> type, @NotNull Throwable cause) {
        super(formatMessage(type, null), cause);
    }

}
