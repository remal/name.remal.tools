package name.remal.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.jetbrains.annotations.NotNull;

@Documented
@Repeatable(AnnotationAttributeAlias.List.class)
@Target(METHOD)
@Retention(RUNTIME)
public @interface AnnotationAttributeAlias {

    @NotNull Class<? extends Annotation> annotationClass();

    @NotNull String attributeName();

    @Documented
    @Target(METHOD)
    @Retention(RUNTIME)
    @interface List {
        @NotNull AnnotationAttributeAlias[] value() default {};
    }

}
