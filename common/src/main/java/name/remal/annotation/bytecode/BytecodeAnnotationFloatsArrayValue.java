package name.remal.annotation.bytecode;

import static java.lang.System.arraycopy;
import static java.util.Objects.requireNonNull;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.InvalidObjectException;
import java.io.ObjectInputValidation;
import java.util.Arrays;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@SuppressFBWarnings({"EI_EXPOSE_REP", "EI_EXPOSE_REP2"})
public class BytecodeAnnotationFloatsArrayValue extends BytecodeAnnotationValue implements ObjectInputValidation {

    private static final float[] EMPTY = new float[0];

    private static final long serialVersionUID = 1L;

    @NotNull
    private float[] value = EMPTY;

    public BytecodeAnnotationFloatsArrayValue() {
    }

    @Contract("null->fail")
    public BytecodeAnnotationFloatsArrayValue(@NotNull float... value) {
        this.value = requireNonNull(value);
    }

    @NotNull
    public float[] getValue() {
        return value;
    }

    @Contract("null->fail")
    public void setValue(@NotNull float[] value) {
        this.value = requireNonNull(value);
    }

    @NotNull
    public BytecodeAnnotationFloatsArrayValue addItem(float item) {
        float[] newValue = new float[value.length + 1];
        arraycopy(value, 0, newValue, 0, value.length);
        newValue[value.length] = item;
        value = newValue;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return Arrays.toString(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationFloatsArrayValue)) return false;
        BytecodeAnnotationFloatsArrayValue that = (BytecodeAnnotationFloatsArrayValue) o;
        return Arrays.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getValue());
    }

    @Override
    public boolean isFloatsArray() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationFloatsArrayValue asFloatsArray() {
        return this;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void validateObject() throws InvalidObjectException {
        if (value == null) {
            throw new InvalidObjectException("value is null");
        }
    }

}
