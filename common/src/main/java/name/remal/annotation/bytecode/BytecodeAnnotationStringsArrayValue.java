package name.remal.annotation.bytecode;

import static java.lang.System.arraycopy;
import static java.util.Objects.requireNonNull;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.InvalidObjectException;
import java.io.ObjectInputValidation;
import java.util.Arrays;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@SuppressFBWarnings({"EI_EXPOSE_REP", "EI_EXPOSE_REP2"})
public class BytecodeAnnotationStringsArrayValue extends BytecodeAnnotationValue implements ObjectInputValidation {

    private static final String[] EMPTY = new String[0];

    private static final long serialVersionUID = 1L;

    @NotNull
    private String[] value = EMPTY;

    public BytecodeAnnotationStringsArrayValue() {
    }

    @Contract("null->fail")
    public BytecodeAnnotationStringsArrayValue(@NotNull String... value) {
        requireNonNull(value);
        for (int i = 0; i < value.length; ++i) {
            requireNonNull(value[i], "value[" + i + ']');
        }
        this.value = value;
    }

    @NotNull
    public String[] getValue() {
        return value;
    }

    @Contract("null->fail")
    public void setValue(@NotNull String[] value) {
        requireNonNull(value);
        for (int i = 0; i < value.length; ++i) {
            requireNonNull(value[i], "value[" + i + ']');
        }
        this.value = value;
    }

    @NotNull
    @Contract("null->fail")
    public BytecodeAnnotationStringsArrayValue addItem(@NotNull String item) {
        String[] newValue = new String[value.length + 1];
        arraycopy(value, 0, newValue, 0, value.length);
        newValue[value.length] = requireNonNull(item);
        value = newValue;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return Arrays.toString(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationStringsArrayValue)) return false;
        BytecodeAnnotationStringsArrayValue that = (BytecodeAnnotationStringsArrayValue) o;
        return Arrays.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getValue());
    }

    @Override
    public boolean isStringsArray() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationStringsArrayValue asStringsArray() {
        return this;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void validateObject() throws InvalidObjectException {
        if (value == null) {
            throw new InvalidObjectException("value is null");
        }
        for (int i = 0; i < value.length; ++i) {
            requireNonNull(value[i], "value[" + i + ']');
        }
    }

}
