package name.remal.annotation.bytecode;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationFloatValue extends BytecodeAnnotationValue {

    private static final long serialVersionUID = 1L;

    private float value;

    public BytecodeAnnotationFloatValue() {
    }

    public BytecodeAnnotationFloatValue(float value) {
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    @NotNull
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationFloatValue)) return false;
        BytecodeAnnotationFloatValue that = (BytecodeAnnotationFloatValue) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Float.hashCode(value);
    }

    @Override
    public boolean isFloat() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationFloatValue asFloat() {
        return this;
    }

}
