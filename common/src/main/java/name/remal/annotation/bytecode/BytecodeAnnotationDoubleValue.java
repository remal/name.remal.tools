package name.remal.annotation.bytecode;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationDoubleValue extends BytecodeAnnotationValue {

    private static final long serialVersionUID = 1L;

    private double value;

    public BytecodeAnnotationDoubleValue() {
    }

    public BytecodeAnnotationDoubleValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    @NotNull
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationDoubleValue)) return false;
        BytecodeAnnotationDoubleValue that = (BytecodeAnnotationDoubleValue) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Double.hashCode(value);
    }

    @Override
    public boolean isDouble() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationDoubleValue asDouble() {
        return this;
    }

}
