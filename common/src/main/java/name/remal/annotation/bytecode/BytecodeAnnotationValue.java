package name.remal.annotation.bytecode;

import java.io.Serializable;
import org.jetbrains.annotations.NotNull;

public abstract class BytecodeAnnotationValue implements Serializable {

    private static final long serialVersionUID = 1L;

    public boolean isByte() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationByteValue asByte() {
        throw new UnsupportedOperationException("Not a byte value");
    }

    public boolean isBytesArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationBytesArrayValue asBytesArray() {
        throw new UnsupportedOperationException("Not a bytes array value");
    }

    public boolean isBoolean() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationBooleanValue asBoolean() {
        throw new UnsupportedOperationException("Not a boolean value");
    }

    public boolean isBooleansArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationBooleansArrayValue asBooleansArray() {
        throw new UnsupportedOperationException("Not a booleans array value");
    }

    public boolean isChar() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationCharValue asChar() {
        throw new UnsupportedOperationException("Not a char value");
    }

    public boolean isCharsArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationCharsArrayValue asCharsArray() {
        throw new UnsupportedOperationException("Not a chars array value");
    }

    public boolean isShort() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationShortValue asShort() {
        throw new UnsupportedOperationException("Not a short value");
    }

    public boolean isShortsArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationShortsArrayValue asShortsArray() {
        throw new UnsupportedOperationException("Not a shorts array value");
    }

    public boolean isInt() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationIntValue asInt() {
        throw new UnsupportedOperationException("Not a int value");
    }

    public boolean isIntsArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationIntsArrayValue asIntsArray() {
        throw new UnsupportedOperationException("Not a ints array value");
    }

    public boolean isLong() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationLongValue asLong() {
        throw new UnsupportedOperationException("Not a long value");
    }

    public boolean isLongsArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationLongsArrayValue asLongsArray() {
        throw new UnsupportedOperationException("Not a longs array value");
    }

    public boolean isFloat() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationFloatValue asFloat() {
        throw new UnsupportedOperationException("Not a float value");
    }

    public boolean isFloatsArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationFloatsArrayValue asFloatsArray() {
        throw new UnsupportedOperationException("Not a floats array value");
    }

    public boolean isDouble() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationDoubleValue asDouble() {
        throw new UnsupportedOperationException("Not a double value");
    }

    public boolean isDoublesArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationDoublesArrayValue asDoublesArray() {
        throw new UnsupportedOperationException("Not a doubles array value");
    }

    public boolean isString() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationStringValue asString() {
        throw new UnsupportedOperationException("Not a string value");
    }

    public boolean isStringsArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationStringsArrayValue asStringsArray() {
        throw new UnsupportedOperationException("Not a strings array value");
    }

    public boolean isClass() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationClassValue asClass() {
        throw new UnsupportedOperationException("Not a class value");
    }

    public boolean isClassesArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationClassesArrayValue asClassesArray() {
        throw new UnsupportedOperationException("Not a classes array value");
    }

    public boolean isEnum() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationEnumValue asEnum() {
        throw new UnsupportedOperationException("Not a enum value");
    }

    public boolean isEnumsArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationEnumsArrayValue asEnumsArray() {
        throw new UnsupportedOperationException("Not a enums array value");
    }

    public boolean isAnnotation() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationAnnotationValue asAnnotation() {
        throw new UnsupportedOperationException("Not a annotation value");
    }

    public boolean isAnnotationsArray() {
        return false;
    }

    @NotNull
    public BytecodeAnnotationAnnotationsArrayValue asAnnotationsArray() {
        throw new UnsupportedOperationException("Not a annotations array value");
    }

}
