package name.remal.annotation.bytecode;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationCharValue extends BytecodeAnnotationValue {

    private static final long serialVersionUID = 1L;

    private char value;

    public BytecodeAnnotationCharValue() {
    }

    public BytecodeAnnotationCharValue(char value) {
        this.value = value;
    }

    public char getValue() {
        return value;
    }

    public void setValue(char value) {
        this.value = value;
    }

    @Override
    @NotNull
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationCharValue)) return false;
        BytecodeAnnotationCharValue that = (BytecodeAnnotationCharValue) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Character.hashCode(value);
    }

    @Override
    public boolean isChar() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationCharValue asChar() {
        return this;
    }

}
