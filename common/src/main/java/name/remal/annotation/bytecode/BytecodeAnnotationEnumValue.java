package name.remal.annotation.bytecode;

import static java.util.Objects.requireNonNull;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.InvalidObjectException;
import java.io.ObjectInputValidation;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationEnumValue extends BytecodeAnnotationValue implements ObjectInputValidation {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String declaringClassName;

    @NotNull
    private String name;

    @Contract("null,_->fail; _,null->fail")
    public BytecodeAnnotationEnumValue(@NotNull String declaringClassName, @NotNull String name) {
        this.declaringClassName = requireNonNull(declaringClassName);
        this.name = requireNonNull(name);
    }

    @NotNull
    public String getDeclaringClassName() {
        return declaringClassName;
    }

    @Contract("null->fail")
    public void setDeclaringClassName(@NotNull String declaringClassName) {
        this.declaringClassName = requireNonNull(declaringClassName);
    }

    @NotNull
    public String getName() {
        return name;
    }

    @Contract("null->fail")
    public void setName(@NotNull String name) {
        this.name = requireNonNull(name);
    }

    @Override
    @NotNull
    public String toString() {
        return declaringClassName + '.' + name;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationEnumValue)) return false;
        BytecodeAnnotationEnumValue that = (BytecodeAnnotationEnumValue) o;
        return declaringClassName.equals(that.declaringClassName)
            && name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return declaringClassName.hashCode() * 31 + name.hashCode();
    }

    @Override
    public boolean isEnum() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationEnumValue asEnum() {
        return this;
    }

    // for deserialization
    @SuppressWarnings("ConstantConditions")
    @SuppressFBWarnings("NP_STORE_INTO_NONNULL_FIELD")
    private BytecodeAnnotationEnumValue() {
        declaringClassName = null;
        name = null;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void validateObject() throws InvalidObjectException {
        if (declaringClassName == null) {
            throw new InvalidObjectException("declaringClassName is null");
        }
        if (name == null) {
            throw new InvalidObjectException("name is null");
        }
    }

}
