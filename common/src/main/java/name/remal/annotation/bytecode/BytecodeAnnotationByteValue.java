package name.remal.annotation.bytecode;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationByteValue extends BytecodeAnnotationValue {

    private static final long serialVersionUID = 1L;

    private byte value;

    public BytecodeAnnotationByteValue() {
    }

    public BytecodeAnnotationByteValue(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return value;
    }

    public void setValue(byte value) {
        this.value = value;
    }

    @Override
    @NotNull
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationByteValue)) return false;
        BytecodeAnnotationByteValue that = (BytecodeAnnotationByteValue) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Byte.hashCode(value);
    }

    @Override
    public boolean isByte() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationByteValue asByte() {
        return this;
    }

}
