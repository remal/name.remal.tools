package name.remal.annotation.bytecode;

import static java.util.Objects.requireNonNull;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.InvalidObjectException;
import java.io.ObjectInputValidation;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationAnnotationValue extends BytecodeAnnotationValue implements ObjectInputValidation {

    @NotNull
    @Contract("null->fail")
    private static Map<String, BytecodeAnnotationValue> checkFields(@NotNull Map<String, BytecodeAnnotationValue> fields) {
        requireNonNull(fields);
        for (Entry<String, BytecodeAnnotationValue> entry : fields.entrySet()) {
            if (entry.getKey() == null) {
                throw new NullPointerException();
            }
            if (entry.getValue() == null) {
                throw new NullPointerException("fields[" + entry.getKey() + ']');
            }
        }
        return fields;
    }

    private static final long serialVersionUID = 1L;

    @NotNull
    private String className;

    @NotNull
    private Map<String, BytecodeAnnotationValue> fields;

    @Contract("null,_->fail; _,null->fail")
    public BytecodeAnnotationAnnotationValue(@NotNull String className, @NotNull Map<String, BytecodeAnnotationValue> fields) {
        this.className = requireNonNull(className);
        this.fields = checkFields(fields);
    }

    @Contract("null->fail")
    public BytecodeAnnotationAnnotationValue(@NotNull String className) {
        this(className, new LinkedHashMap<>());
    }

    @NotNull
    public String getClassName() {
        return className;
    }

    @Contract("null->fail")
    public void setClassName(@NotNull String className) {
        this.className = requireNonNull(className);
    }

    @NotNull
    public Map<String, BytecodeAnnotationValue> getFields() {
        return fields;
    }

    @Contract("null->fail")
    public void setFields(@NotNull Map<String, BytecodeAnnotationValue> fields) {
        this.fields = checkFields(fields);
    }

    @Nullable
    public BytecodeAnnotationValue getField(@NotNull String field) {
        return fields.get(field);
    }

    @NotNull
    @Contract("null,_->fail; _,null->fail")
    public BytecodeAnnotationAnnotationValue setField(@NotNull String field, @NotNull BytecodeAnnotationValue value) {
        fields.put(requireNonNull(field), requireNonNull(value));
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return className + fields;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationAnnotationValue)) return false;
        BytecodeAnnotationAnnotationValue that = (BytecodeAnnotationAnnotationValue) o;
        return className.equals(that.className)
            && fields.equals(that.fields);
    }

    @Override
    public int hashCode() {
        return className.hashCode() * 31 + fields.hashCode();
    }

    @Override
    public boolean isAnnotation() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationAnnotationValue asAnnotation() {
        return this;
    }

    // for deserialization
    @SuppressWarnings("ConstantConditions")
    @SuppressFBWarnings("NP_STORE_INTO_NONNULL_FIELD")
    private BytecodeAnnotationAnnotationValue() {
        className = null;
        fields = null;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void validateObject() throws InvalidObjectException {
        if (className == null) {
            throw new InvalidObjectException("className is null");
        }
        checkFields(fields);
        for (Entry<String, BytecodeAnnotationValue> entry : fields.entrySet()) {
            BytecodeAnnotationValue value = entry.getValue();
            if (value instanceof ObjectInputValidation) {
                try {
                    ((ObjectInputValidation) value).validateObject();
                } catch (InvalidObjectException e) {
                    throw new InvalidObjectException("fields[" + entry.getKey() + "]" + e.getMessage());
                }
            }
        }
    }

}
