package name.remal.annotation.bytecode;

import static java.util.Objects.requireNonNull;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.InvalidObjectException;
import java.io.ObjectInputValidation;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationStringValue extends BytecodeAnnotationValue implements ObjectInputValidation {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String value;

    @Contract("null->fail")
    public BytecodeAnnotationStringValue(@NotNull String value) {
        this.value = requireNonNull(value);
    }

    @NotNull
    public String getValue() {
        return value;
    }

    @Contract("null->fail")
    public void setValue(@NotNull String value) {
        this.value = requireNonNull(value);
    }

    @Override
    @NotNull
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationStringValue)) return false;
        BytecodeAnnotationStringValue that = (BytecodeAnnotationStringValue) o;
        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean isString() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationStringValue asString() {
        return this;
    }

    // for deserialization
    @SuppressWarnings("ConstantConditions")
    @SuppressFBWarnings("NP_STORE_INTO_NONNULL_FIELD")
    private BytecodeAnnotationStringValue() {
        value = null;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void validateObject() throws InvalidObjectException {
        if (value == null) {
            throw new InvalidObjectException("value is null");
        }
    }

}
