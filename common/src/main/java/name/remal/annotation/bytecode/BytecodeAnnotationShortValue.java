package name.remal.annotation.bytecode;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationShortValue extends BytecodeAnnotationValue {

    private static final long serialVersionUID = 1L;

    private short value;

    public BytecodeAnnotationShortValue() {
    }

    public BytecodeAnnotationShortValue(short value) {
        this.value = value;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    @Override
    @NotNull
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationShortValue)) return false;
        BytecodeAnnotationShortValue that = (BytecodeAnnotationShortValue) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Short.hashCode(value);
    }

    @Override
    public boolean isShort() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationShortValue asShort() {
        return this;
    }

}
