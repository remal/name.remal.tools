package name.remal.annotation.bytecode;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationIntValue extends BytecodeAnnotationValue {

    private static final long serialVersionUID = 1L;

    private int value;

    public BytecodeAnnotationIntValue() {
    }

    public BytecodeAnnotationIntValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    @NotNull
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationIntValue)) return false;
        BytecodeAnnotationIntValue that = (BytecodeAnnotationIntValue) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(value);
    }

    @Override
    public boolean isInt() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationIntValue asInt() {
        return this;
    }

}
