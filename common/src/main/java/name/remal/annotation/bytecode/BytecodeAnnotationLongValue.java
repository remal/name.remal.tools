package name.remal.annotation.bytecode;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationLongValue extends BytecodeAnnotationValue {

    private static final long serialVersionUID = 1L;

    private long value;

    public BytecodeAnnotationLongValue() {
    }

    public BytecodeAnnotationLongValue(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    @Override
    @NotNull
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationLongValue)) return false;
        BytecodeAnnotationLongValue that = (BytecodeAnnotationLongValue) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(value);
    }

    @Override
    public boolean isLong() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationLongValue asLong() {
        return this;
    }

}
