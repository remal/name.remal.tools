package name.remal.annotation.bytecode;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationBooleanValue extends BytecodeAnnotationValue {

    private static final long serialVersionUID = 1L;

    private boolean value;

    public BytecodeAnnotationBooleanValue() {
    }

    public BytecodeAnnotationBooleanValue(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    @Override
    @NotNull
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationBooleanValue)) return false;
        BytecodeAnnotationBooleanValue that = (BytecodeAnnotationBooleanValue) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Boolean.hashCode(value);
    }

    @Override
    public boolean isBoolean() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationBooleanValue asBoolean() {
        return this;
    }

}
