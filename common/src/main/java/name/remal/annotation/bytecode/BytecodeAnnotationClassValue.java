package name.remal.annotation.bytecode;

import static java.util.Objects.requireNonNull;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.InvalidObjectException;
import java.io.ObjectInputValidation;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BytecodeAnnotationClassValue extends BytecodeAnnotationValue implements ObjectInputValidation {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String className;

    @Contract("null->fail")
    public BytecodeAnnotationClassValue(@NotNull String className) {
        this.className = requireNonNull(className);
    }

    @NotNull
    public String getClassName() {
        return className;
    }

    @Contract("null->fail")
    public void setClassName(@NotNull String className) {
        this.className = requireNonNull(className);
    }

    @Override
    @NotNull
    public String toString() {
        return className;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationClassValue)) return false;
        BytecodeAnnotationClassValue that = (BytecodeAnnotationClassValue) o;
        return className.equals(that.className);
    }

    @Override
    public int hashCode() {
        return className.hashCode();
    }

    @Override
    public boolean isClass() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationClassValue asClass() {
        return this;
    }

    // for deserialization
    @SuppressWarnings("ConstantConditions")
    @SuppressFBWarnings("NP_STORE_INTO_NONNULL_FIELD")
    private BytecodeAnnotationClassValue() {
        className = null;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void validateObject() throws InvalidObjectException {
        if (className == null) {
            throw new InvalidObjectException("className is null");
        }
    }

}
