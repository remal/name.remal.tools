package name.remal.annotation.bytecode;

import static java.lang.System.arraycopy;
import static java.util.Objects.requireNonNull;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.InvalidObjectException;
import java.io.ObjectInputValidation;
import java.util.Arrays;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@SuppressFBWarnings({"EI_EXPOSE_REP", "EI_EXPOSE_REP2"})
public class BytecodeAnnotationEnumsArrayValue extends BytecodeAnnotationValue implements ObjectInputValidation {

    private static final BytecodeAnnotationEnumValue[] EMPTY = new BytecodeAnnotationEnumValue[0];

    private static final long serialVersionUID = 1L;

    @NotNull
    private BytecodeAnnotationEnumValue[] value = EMPTY;

    public BytecodeAnnotationEnumsArrayValue() {
    }

    @Contract("null->fail")
    public BytecodeAnnotationEnumsArrayValue(@NotNull BytecodeAnnotationEnumValue... value) {
        requireNonNull(value);
        for (int i = 0; i < value.length; ++i) {
            requireNonNull(value[i], "value[" + i + ']');
        }
        this.value = value;
    }

    @NotNull
    public BytecodeAnnotationEnumValue[] getValue() {
        return value;
    }

    @Contract("null->fail")
    public void setValue(@NotNull BytecodeAnnotationEnumValue[] value) {
        requireNonNull(value);
        for (int i = 0; i < value.length; ++i) {
            requireNonNull(value[i], "value[" + i + ']');
        }
        this.value = value;
    }

    @NotNull
    @Contract("null->fail")
    public BytecodeAnnotationEnumsArrayValue addItem(@NotNull BytecodeAnnotationEnumValue item) {
        BytecodeAnnotationEnumValue[] newValue = new BytecodeAnnotationEnumValue[value.length + 1];
        arraycopy(value, 0, newValue, 0, value.length);
        newValue[value.length] = requireNonNull(item);
        value = newValue;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return Arrays.toString(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationEnumsArrayValue)) return false;
        BytecodeAnnotationEnumsArrayValue that = (BytecodeAnnotationEnumsArrayValue) o;
        return Arrays.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getValue());
    }

    @Override
    public boolean isEnumsArray() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationEnumsArrayValue asEnumsArray() {
        return this;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void validateObject() throws InvalidObjectException {
        if (value == null) {
            throw new InvalidObjectException("value is null");
        }
        for (int i = 0; i < value.length; ++i) {
            requireNonNull(value[i], "value[" + i + ']');
            try {
                value[i].validateObject();
            } catch (InvalidObjectException e) {
                throw new InvalidObjectException("value[" + i + "]: " + e.getMessage());
            }
        }
    }

}
