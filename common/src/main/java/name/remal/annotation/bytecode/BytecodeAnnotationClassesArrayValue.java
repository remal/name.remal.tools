package name.remal.annotation.bytecode;

import static java.lang.System.arraycopy;
import static java.util.Objects.requireNonNull;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.InvalidObjectException;
import java.io.ObjectInputValidation;
import java.util.Arrays;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@SuppressFBWarnings({"EI_EXPOSE_REP", "EI_EXPOSE_REP2"})
public class BytecodeAnnotationClassesArrayValue extends BytecodeAnnotationValue implements ObjectInputValidation {

    private static final String[] EMPTY = new String[0];

    private static final long serialVersionUID = 1L;

    @NotNull
    private String[] classNames = EMPTY;

    public BytecodeAnnotationClassesArrayValue() {
    }

    @Contract("null->fail")
    public BytecodeAnnotationClassesArrayValue(@NotNull String... classNames) {
        requireNonNull(classNames);
        for (int i = 0; i < classNames.length; ++i) {
            requireNonNull(classNames[i], "classNames[" + i + ']');
        }
        this.classNames = classNames;
    }

    @NotNull
    public String[] getClassNames() {
        return classNames;
    }

    @Contract("null->fail")
    public void setClassNames(@NotNull String[] classNames) {
        requireNonNull(classNames);
        for (int i = 0; i < classNames.length; ++i) {
            requireNonNull(classNames[i], "classNames[" + i + ']');
        }
        this.classNames = classNames;
    }

    @NotNull
    @Contract("null->fail")
    public BytecodeAnnotationClassesArrayValue addItem(@NotNull String item) {
        String[] newValue = new String[classNames.length + 1];
        arraycopy(classNames, 0, newValue, 0, classNames.length);
        newValue[classNames.length] = requireNonNull(item);
        classNames = newValue;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return Arrays.toString(classNames);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationClassesArrayValue)) return false;
        BytecodeAnnotationClassesArrayValue that = (BytecodeAnnotationClassesArrayValue) o;
        return Arrays.equals(classNames, that.classNames);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getClassNames());
    }

    @Override
    public boolean isClassesArray() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationClassesArrayValue asClassesArray() {
        return this;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void validateObject() throws InvalidObjectException {
        if (classNames == null) {
            throw new InvalidObjectException("classNames is null");
        }
        for (int i = 0; i < classNames.length; ++i) {
            requireNonNull(classNames[i], "classNames[" + i + ']');
        }
    }

}
