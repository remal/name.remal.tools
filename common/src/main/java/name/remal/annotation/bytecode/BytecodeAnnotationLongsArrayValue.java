package name.remal.annotation.bytecode;

import static java.lang.System.arraycopy;
import static java.util.Objects.requireNonNull;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.InvalidObjectException;
import java.io.ObjectInputValidation;
import java.util.Arrays;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@SuppressFBWarnings({"EI_EXPOSE_REP", "EI_EXPOSE_REP2"})
public class BytecodeAnnotationLongsArrayValue extends BytecodeAnnotationValue implements ObjectInputValidation {

    private static final long[] EMPTY = new long[0];

    private static final long serialVersionUID = 1L;

    @NotNull
    private long[] value = EMPTY;

    public BytecodeAnnotationLongsArrayValue() {
    }

    @Contract("null->fail")
    public BytecodeAnnotationLongsArrayValue(@NotNull long... value) {
        this.value = requireNonNull(value);
    }

    @NotNull
    public long[] getValue() {
        return value;
    }

    @Contract("null->fail")
    public void setValue(@NotNull long[] value) {
        this.value = requireNonNull(value);
    }

    @NotNull
    public BytecodeAnnotationLongsArrayValue addItem(long item) {
        long[] newValue = new long[value.length + 1];
        arraycopy(value, 0, newValue, 0, value.length);
        newValue[value.length] = item;
        value = newValue;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return Arrays.toString(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationLongsArrayValue)) return false;
        BytecodeAnnotationLongsArrayValue that = (BytecodeAnnotationLongsArrayValue) o;
        return Arrays.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getValue());
    }

    @Override
    public boolean isLongsArray() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationLongsArrayValue asLongsArray() {
        return this;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void validateObject() throws InvalidObjectException {
        if (value == null) {
            throw new InvalidObjectException("value is null");
        }
    }

}
