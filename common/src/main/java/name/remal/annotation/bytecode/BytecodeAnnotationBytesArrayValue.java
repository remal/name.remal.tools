package name.remal.annotation.bytecode;

import static java.lang.System.arraycopy;
import static java.util.Objects.requireNonNull;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.InvalidObjectException;
import java.io.ObjectInputValidation;
import java.util.Arrays;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@SuppressFBWarnings({"EI_EXPOSE_REP", "EI_EXPOSE_REP2"})
public class BytecodeAnnotationBytesArrayValue extends BytecodeAnnotationValue implements ObjectInputValidation {

    private static final byte[] EMPTY = new byte[0];

    private static final long serialVersionUID = 1L;

    @NotNull
    private byte[] value = EMPTY;

    public BytecodeAnnotationBytesArrayValue() {
    }

    @Contract("null->fail")
    public BytecodeAnnotationBytesArrayValue(@NotNull byte... value) {
        this.value = requireNonNull(value);
    }

    @NotNull
    public byte[] getValue() {
        return value;
    }

    @Contract("null->fail")
    public void setValue(@NotNull byte[] value) {
        this.value = requireNonNull(value);
    }

    @NotNull
    public BytecodeAnnotationBytesArrayValue addItem(byte item) {
        byte[] newValue = new byte[value.length + 1];
        arraycopy(value, 0, newValue, 0, value.length);
        newValue[value.length] = item;
        value = newValue;
        return this;
    }

    @Override
    @NotNull
    public String toString() {
        return Arrays.toString(value);
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BytecodeAnnotationBytesArrayValue)) return false;
        BytecodeAnnotationBytesArrayValue that = (BytecodeAnnotationBytesArrayValue) o;
        return Arrays.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(getValue());
    }

    @Override
    public boolean isBytesArray() {
        return true;
    }

    @Override
    @NotNull
    public BytecodeAnnotationBytesArrayValue asBytesArray() {
        return this;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void validateObject() throws InvalidObjectException {
        if (value == null) {
            throw new InvalidObjectException("value is null");
        }
    }

}
