package name.remal.annotation.bytecode;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@FunctionalInterface
interface BytecodeRetriever {

    @Nullable
    byte[] retrieve(@NotNull String className) throws Exception;

}
