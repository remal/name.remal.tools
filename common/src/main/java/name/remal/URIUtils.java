package name.remal;

import static name.remal.SneakyThrow.sneakyThrow;

import java.net.URI;
import java.net.URISyntaxException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class URIUtils {

    @NotNull
    public static URI withScheme(@NotNull URI uri, @Nullable String scheme) {
        try {
            if (scheme != null && scheme.isEmpty()) {
                scheme = null;
            }
            String ssp = uri.getSchemeSpecificPart();
            if (ssp.isEmpty()) {
                return new URI(scheme, uri.getAuthority(), uri.getPath(), uri.getQuery(), uri.getFragment());
            } else {
                return new URI(scheme, ssp, uri.getFragment());
            }
        } catch (URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URI withSchemeSpecificPart(@NotNull URI uri, @NotNull String schemeSpecificPart) {
        try {
            return new URI(uri.getScheme(), schemeSpecificPart, uri.getFragment());
        } catch (URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URI withAuthority(@NotNull URI uri, @Nullable String authority) {
        try {
            return new URI(uri.getScheme(), authority, uri.getPath(), uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URI withUserInfo(@NotNull URI uri, @Nullable String userInfo) {
        try {
            return new URI(uri.getScheme(), userInfo, uri.getHost(), uri.getPort(), uri.getPath(), uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URI withHost(@NotNull URI uri, @Nullable String host) {
        try {
            return new URI(uri.getScheme(), uri.getUserInfo(), host, uri.getPort(), uri.getPath(), uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URI withPort(@NotNull URI uri, int port) {
        try {
            return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), port, uri.getPath(), uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URI withDefaultPort(@NotNull URI uri) {
        return withPort(uri, -1);
    }

    @NotNull
    public static URI withPath(@NotNull URI uri, @Nullable String path) {
        try {
            return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort(), path, uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URI withQuery(@NotNull URI uri, @Nullable String query) {
        try {
            if (query != null && query.isEmpty()) {
                query = null;
            }
            return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort(), uri.getPath(), query, uri.getFragment());
        } catch (URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URI withFragment(@NotNull URI uri, @Nullable String fragment) {
        try {
            if (fragment != null && fragment.isEmpty()) {
                fragment = null;
            }
            return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort(), uri.getPath(), uri.getQuery(), fragment);
        } catch (URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

}
