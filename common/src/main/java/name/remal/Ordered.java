package name.remal;

import org.jetbrains.annotations.NotNull;

public interface Ordered<T extends Ordered<T>> extends Comparable<T> {

    default int getOrder() {
        return 0;
    }

    @Override
    @SuppressWarnings("deprecation")
    default int compareTo(@NotNull T other) {
        int result = Integer.compare(getSuperOrder(), other.getSuperOrder());
        if (0 == result) result = Integer.compare(getOrder(), other.getOrder());
        return result;
    }

    /**
     * @deprecated This method shouldn't be overridden in implementation classes
     */
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    default int getSuperOrder() {
        return 0;
    }

}
