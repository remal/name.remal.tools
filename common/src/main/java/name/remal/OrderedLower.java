package name.remal;

public interface OrderedLower<T extends Ordered<T>> extends Ordered<T> {

    @SuppressWarnings("deprecation")
    @Override
    default int getSuperOrder() {
        return 1000;
    }

}
