package name.remal;

import static name.remal.SneakyThrow.sneakyThrow;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class URLUtils {

    @NotNull
    public static URL withProtocol(@NotNull URL url, @NotNull String protocol) {
        try {
            return URIUtils.withScheme(url.toURI(), protocol).toURL();
        } catch (@NotNull MalformedURLException | URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URL withAuthority(@NotNull URL url, @NotNull String authority) {
        try {
            return URIUtils.withAuthority(url.toURI(), authority).toURL();
        } catch (@NotNull MalformedURLException | URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URL withUserInfo(@NotNull URL url, @Nullable String userInfo) {
        try {
            return URIUtils.withUserInfo(url.toURI(), userInfo).toURL();
        } catch (@NotNull MalformedURLException | URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URL withHost(@NotNull URL url, @NotNull String host) {
        try {
            return URIUtils.withHost(url.toURI(), host).toURL();
        } catch (@NotNull MalformedURLException | URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URL withPort(@NotNull URL url, int port) {
        try {
            return URIUtils.withPort(url.toURI(), port).toURL();
        } catch (@NotNull MalformedURLException | URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URL withDefaultPort(@NotNull URL url) {
        return withPort(url, -1);
    }

    @NotNull
    public static URL withPath(@NotNull URL url, @NotNull String path) {
        try {
            return URIUtils.withPath(url.toURI(), path).toURL();
        } catch (@NotNull MalformedURLException | URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URL withQuery(@NotNull URL url, @Nullable String query) {
        try {
            return URIUtils.withQuery(url.toURI(), query).toURL();
        } catch (@NotNull MalformedURLException | URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static URL withRef(@NotNull URL url, @Nullable String ref) {
        try {
            return URIUtils.withFragment(url.toURI(), ref).toURL();
        } catch (@NotNull MalformedURLException | URISyntaxException e) {
            throw sneakyThrow(e);
        }
    }

}
