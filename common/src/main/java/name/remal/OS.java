package name.remal;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static kr.motd.maven.os.Detector.DETECTED_ARCH;
import static kr.motd.maven.os.Detector.DETECTED_CLASSIFIER;
import static kr.motd.maven.os.Detector.DETECTED_NAME;
import static kr.motd.maven.os.Detector.DETECTED_VERSION;
import static name.remal.log.LogUtils.logDebug;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import kr.motd.maven.os.Detector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class OS {

    @NotNull
    public static String getOsName() {
        return requireNonNull(PropertiesHolder.PROPERTIES.getProperty(DETECTED_NAME), DETECTED_NAME + " is not set");
    }

    @NotNull
    public static String getOsArch() {
        return requireNonNull(PropertiesHolder.PROPERTIES.getProperty(DETECTED_ARCH), DETECTED_ARCH + " is not set");
    }

    @NotNull
    public static String getOsClassifier() {
        return requireNonNull(PropertiesHolder.PROPERTIES.getProperty(DETECTED_CLASSIFIER), DETECTED_CLASSIFIER + " is not set");
    }

    @Nullable
    public static String getOsVersion() {
        return PropertiesHolder.PROPERTIES.getProperty(DETECTED_VERSION);
    }


    public static boolean isAix() {
        return "aix".equals(getOsName());
    }

    public static boolean isHpux() {
        return "hpux".equals(getOsName());
    }

    public static boolean isOs400() {
        return "os400".equals(getOsName());
    }

    public static boolean isLinux() {
        return "linux".equals(getOsName());
    }

    public static boolean isOsx() {
        return "osx".equals(getOsName());
    }

    public static boolean isFreebsd() {
        return "freebsd".equals(getOsName());
    }

    public static boolean isOpenbsd() {
        return "openbsd".equals(getOsName());
    }

    public static boolean isNetbsd() {
        return "netbsd".equals(getOsName());
    }

    public static boolean isSunos() {
        return "sunos".equals(getOsName());
    }

    public static boolean isWindows() {
        return "windows".equals(getOsName());
    }

    public static boolean isZos() {
        return "zos".equals(getOsName());
    }


    private static class PropertiesHolder {
        private static final Properties PROPERTIES;

        static {
            PROPERTIES = new Properties();
            new DetectorImpl().detect(PROPERTIES, emptyList());

            new ArrayList<>(PROPERTIES.entrySet()).forEach(entry -> {
                if (entry.getKey().toString().isEmpty() || entry.getValue().toString().isEmpty()) {
                    PROPERTIES.remove(entry.getKey());
                }
            });
        }
    }


    private static class DetectorImpl extends Detector {
        @Override
        public void detect(@NotNull Properties props, @NotNull List<String> classifierWithLikes) {
            super.detect(props, classifierWithLikes);
        }

        @Override
        protected void log(String message) {
            logDebug(OS.class, message);
        }

        @Override
        protected void logProperty(String name, String value) {
            logDebug(OS.class, name + " = " + value);
        }
    }


    private OS() {
    }

}
