package name.remal.collections;

@FunctionalInterface
public interface HandlerRegistration {

    void deregister();

}
