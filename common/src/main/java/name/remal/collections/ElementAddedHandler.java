package name.remal.collections;

import org.jetbrains.annotations.NotNull;

@FunctionalInterface
public interface ElementAddedHandler<E> {

    void onElementAdded(@NotNull E element) throws Throwable;

}
