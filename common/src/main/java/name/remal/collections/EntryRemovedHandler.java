package name.remal.collections;

@FunctionalInterface
public interface EntryRemovedHandler<K, V> {

    void onEntryRemoved(K key, V value) throws Throwable;

}
