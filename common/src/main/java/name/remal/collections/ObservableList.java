package name.remal.collections;

import java.util.List;

public interface ObservableList<E> extends ObservableCollection<E>, List<E> {
}
