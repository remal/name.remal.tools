package name.remal.collections;

import org.jetbrains.annotations.NotNull;

@FunctionalInterface
public interface ElementRemovedHandler<E> {

    void onElementRemoved(@NotNull E element) throws Throwable;

}
