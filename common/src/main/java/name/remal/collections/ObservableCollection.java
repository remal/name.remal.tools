package name.remal.collections;

import java.util.Collection;
import org.jetbrains.annotations.NotNull;

public interface ObservableCollection<E> extends Collection<E> {

    @NotNull
    HandlerRegistration registerElementAddedHandler(@NotNull ElementAddedHandler<E> handler);

    @NotNull
    HandlerRegistration registerElementRemovedHandler(@NotNull ElementRemovedHandler<E> handler);

    @NotNull
    default HandlerRegistration registerCollectionChangedHandler(@NotNull CollectionChangedHandler handler) {
        HandlerRegistration elementAddedHandlerRegistration = registerElementAddedHandler(__ -> handler.onCollectionChanged());
        HandlerRegistration elementRemovedHandlerRegistration = registerElementRemovedHandler(__ -> handler.onCollectionChanged());
        return () -> {
            elementAddedHandlerRegistration.deregister();
            elementRemovedHandlerRegistration.deregister();
        };
    }

}
