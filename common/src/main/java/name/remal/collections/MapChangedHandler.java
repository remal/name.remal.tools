package name.remal.collections;

@FunctionalInterface
public interface MapChangedHandler {

    void onMapChanged() throws Throwable;

}
