package name.remal.collections;

@FunctionalInterface
public interface CollectionChangedHandler {

    void onCollectionChanged() throws Throwable;

}
