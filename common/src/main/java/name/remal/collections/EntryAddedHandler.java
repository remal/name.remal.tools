package name.remal.collections;

@FunctionalInterface
public interface EntryAddedHandler<K, V> {

    void onEntryAdded(K key, V value) throws Throwable;

}
