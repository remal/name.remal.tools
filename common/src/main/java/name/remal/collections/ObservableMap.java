package name.remal.collections;

import java.util.Map;
import org.jetbrains.annotations.NotNull;

public interface ObservableMap<K, V> extends Map<K, V> {

    @NotNull
    HandlerRegistration registerEntryAddedHandler(@NotNull EntryAddedHandler<K, V> handler);

    @NotNull
    HandlerRegistration registerEntryRemovedHandler(@NotNull EntryRemovedHandler<K, V> handler);

    @NotNull
    default HandlerRegistration registerMapChangedHandler(@NotNull MapChangedHandler handler) {
        HandlerRegistration entryAddedHandlerRegistration = registerEntryAddedHandler((key, value) -> handler.onMapChanged());
        HandlerRegistration entryRemovedHandlerRegistration = registerEntryRemovedHandler((key, value) -> handler.onMapChanged());
        return () -> {
            entryAddedHandlerRegistration.deregister();
            entryRemovedHandlerRegistration.deregister();
        };
    }

}
