package name.remal.proxy;

import static name.remal.SneakyThrow.sneakyThrow;

import java.lang.reflect.Method;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface MethodHandler {

    boolean canHandle(@NotNull Method method);

    @Nullable
    Object handle(@NotNull Object proxy, @NotNull Method method, @Nullable Object[] args) throws Throwable;

    @NotNull
    static MethodHandler of(@NotNull CanHandle canHandle, @NotNull Handler handler) {
        return new MethodHandler() {
            @Override
            public boolean canHandle(@NotNull Method method) {
                try {
                    return canHandle.canHandle(method);
                } catch (Throwable throwable) {
                    throw sneakyThrow(throwable);
                }
            }

            @Nullable
            @Override
            public Object handle(@NotNull Object proxy, @NotNull Method method, @Nullable Object[] args) throws Throwable {
                return handler.handle(proxy, method, args);
            }
        };
    }

    @FunctionalInterface
    interface CanHandle {
        boolean canHandle(@NotNull Method method) throws Throwable;
    }

    @FunctionalInterface
    interface Handler {

        @Nullable
        Object handle(@NotNull Object proxy, @NotNull Method method, @Nullable Object[] args) throws Throwable;

    }

}
