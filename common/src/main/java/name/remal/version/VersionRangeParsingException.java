package name.remal.version;

public class VersionRangeParsingException extends RuntimeException {

    private static final long serialVersionUID = 1;

    public VersionRangeParsingException() {
    }

    public VersionRangeParsingException(String message) {
        super(message);
    }

    public VersionRangeParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public VersionRangeParsingException(Throwable cause) {
        super(cause);
    }

}
