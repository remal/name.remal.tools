package name.remal.version;

public class VersionParsingException extends RuntimeException {

    private static final long serialVersionUID = 1;

    public VersionParsingException() {
    }

    public VersionParsingException(String message) {
        super(message);
    }

    public VersionParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public VersionParsingException(Throwable cause) {
        super(cause);
    }

}
