package name.remal;

import name.remal.gradle_plugins.api.ExcludeFromCodeCoverage;
import org.jetbrains.annotations.NotNull;

@ExcludeFromCodeCoverage
public class Constants {

    @NotNull
    public static final String SERVICE_FILE_BASE_PATH = "META-INF/services";

}
