package name.remal;

public interface OrderedLatest<T extends Ordered<T>> extends Ordered<T> {

    @SuppressWarnings("deprecation")
    @Override
    default int getSuperOrder() {
        return Integer.MAX_VALUE;
    }

    @Override
    default int getOrder() {
        return Integer.MAX_VALUE;
    }

}
