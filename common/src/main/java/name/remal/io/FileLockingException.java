package name.remal.io;

import java.io.IOException;

public class FileLockingException extends IOException {

    private static final long serialVersionUID = 0;

    public FileLockingException() {
    }

    public FileLockingException(String message) {
        super(message);
    }

    public FileLockingException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileLockingException(Throwable cause) {
        super(cause);
    }

}
