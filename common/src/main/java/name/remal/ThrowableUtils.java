package name.remal;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static name.remal.UncheckedCast.uncheckedCast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ThrowableUtils {

    @NotNull
    private static <T extends Throwable> List<@NotNull T> findImpl(@NotNull Throwable rootThrowable, @NotNull Class<T> type, boolean doReturnOnlyFirst) {
        List<T> result = null;

        Set<Throwable> processedThrowables = new HashSet<>();
        processedThrowables.add(rootThrowable);
        Queue<Throwable> throwablesQueue = new LinkedList<>();
        throwablesQueue.add(rootThrowable);
        while (true) {
            Throwable throwable = throwablesQueue.poll();
            if (throwable == null) break;

            if (type.isInstance(throwable)) {
                if (doReturnOnlyFirst) return singletonList(uncheckedCast(throwable));
                if (result == null) result = new ArrayList<>();
                result.add(uncheckedCast(throwable));
            }

            {
                Throwable cause = throwable.getCause();
                if (cause != null && processedThrowables.add(cause)) throwablesQueue.add(cause);
            }

            for (Throwable supressed : throwable.getSuppressed()) {
                if (processedThrowables.add(supressed)) throwablesQueue.add(supressed);
            }
        }

        return result != null ? result : emptyList();
    }

    @NotNull
    public static <T extends Throwable> List<@NotNull T> findAll(@NotNull Throwable throwable, @NotNull Class<T> type) {
        return findImpl(throwable, type, false);
    }

    @Nullable
    public static <T extends Throwable> T findFirst(@NotNull Throwable throwable, @NotNull Class<T> type) {
        List<T> list = findImpl(throwable, type, true);
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Nullable
    public static <T extends Throwable> T findLast(@NotNull Throwable throwable, @NotNull Class<T> type) {
        List<T> list = findImpl(throwable, type, false);
        return !list.isEmpty() ? list.get(list.size() - 1) : null;
    }

    public static boolean contains(@NotNull Throwable throwable, @NotNull Class<? extends Throwable> type) {
        return findFirst(throwable, type) != null;
    }

}
