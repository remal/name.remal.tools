package name.remal.reflection;

import com.google.common.reflect.TypeToken;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import name.remal.gradle_plugins.api.RelocateClasses;
import org.jetbrains.annotations.NotNull;

public abstract class TypeProvider<T> {

    @NotNull
    private final Type type;

    @RelocateClasses(TypeToken.class)
    protected TypeProvider() {
        final Type superclass;
        if (TypeProvider.class == getClass().getSuperclass()) {
            superclass = getClass().getGenericSuperclass();
        } else {
            superclass = TypeToken.of(getClass()).getSupertype(TypeProvider.class).getType();
        }

        if (!(superclass instanceof ParameterizedType)) throw new IllegalStateException(superclass + " isn't parameterized");
        type = ((ParameterizedType) superclass).getActualTypeArguments()[0];
    }

    @NotNull
    public Type getType() {
        return type;
    }

}
