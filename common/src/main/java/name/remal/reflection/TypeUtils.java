package name.remal.reflection;

import com.google.common.reflect.TypeToken;
import java.lang.reflect.Type;
import name.remal.gradle_plugins.api.RelocateClasses;
import org.jetbrains.annotations.NotNull;

public class TypeUtils {

    @NotNull
    @RelocateClasses(TypeToken.class)
    public static Class<?> toClass(@NotNull Type type) {
        return TypeToken.of(type).getRawType();
    }

}
