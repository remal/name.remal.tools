package name.remal;

import static java.nio.charset.StandardCharsets.UTF_8;
import static name.remal.SneakyThrow.sneakyThrow;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.net.URLCodec;
import org.jetbrains.annotations.NotNull;

public class Codecs {

    @NotNull
    public static String encodeHex(@NotNull byte[] data) {
        return Hex.encodeHexString(data, true);
    }

    @NotNull
    public static byte[] decodeHex(@NotNull String data) {
        try {
            return Hex.decodeHex(data);
        } catch (DecoderException e) {
            throw sneakyThrow(e);
        }
    }


    @NotNull
    public static String encodeBase64(@NotNull byte[] data) {
        return Base64.encodeBase64String(data);
    }

    @NotNull
    public static String encodeBase64URLSafe(@NotNull byte[] data) {
        return Base64.encodeBase64URLSafeString(data);
    }

    @NotNull
    public static byte[] decodeBase64(@NotNull String data) {
        return Base64.decodeBase64(data);
    }


    @NotNull
    public static String encodeURIComponent(@NotNull String data, @NotNull Charset charset) {
        try {
            return new URLCodec().encode(data, charset.name());
        } catch (UnsupportedEncodingException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static String encodeURIComponent(@NotNull String data) {
        return encodeURIComponent(data, UTF_8);
    }

    @NotNull
    public static String decodeURIComponent(@NotNull String data, @NotNull Charset charset) {
        try {
            return new URLCodec().decode(data, charset.name());
        } catch (@NotNull UnsupportedEncodingException | DecoderException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static String decodeURIComponent(@NotNull String data) {
        return decodeURIComponent(data, UTF_8);
    }


    private static void update(@NotNull MessageDigest digest, @NotNull InputStream data) {
        try {
            byte[] buffer = new byte[1024];
            int read;
            while (-1 < (read = data.read(buffer, 0, buffer.length))) {
                digest.update(buffer, 0, read);
            }
        } catch (IOException e) {
            throw sneakyThrow(e);
        }
    }


    @NotNull
    @SuppressFBWarnings("WEAK_MESSAGE_DIGEST_MD5")
    public static MessageDigest newMd5Digest() {
        try {
            return MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static String md5(@NotNull byte[] data) {
        return encodeHex(newMd5Digest().digest(data));
    }

    @NotNull
    public static String md5(@NotNull String data, @NotNull Charset charset) {
        return md5(data.getBytes(charset));
    }

    @NotNull
    public static String md5(@NotNull String data) {
        return md5(data, UTF_8);
    }

    @NotNull
    public static String md5(@NotNull InputStream data) {
        MessageDigest digest = newMd5Digest();
        update(digest, data);
        return encodeHex(digest.digest());
    }


    @NotNull
    public static MessageDigest newSha256Digest() {
        try {
            return MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static String sha256(@NotNull byte[] data) {
        return encodeHex(newSha256Digest().digest(data));
    }

    @NotNull
    public static String sha256(@NotNull String data, @NotNull Charset charset) {
        return sha256(data.getBytes(charset));
    }

    @NotNull
    public static String sha256(@NotNull String data) {
        return sha256(data, UTF_8);
    }

    @NotNull
    public static String sha256(@NotNull InputStream data) {
        MessageDigest digest = newSha256Digest();
        update(digest, data);
        return encodeHex(digest.digest());
    }


    @NotNull
    public static MessageDigest newSha512Digest() {
        try {
            return MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            throw sneakyThrow(e);
        }
    }

    @NotNull
    public static String sha512(@NotNull byte[] data) {
        return encodeHex(newSha512Digest().digest(data));
    }

    @NotNull
    public static String sha512(@NotNull String data, @NotNull Charset charset) {
        return sha512(data.getBytes(charset));
    }

    @NotNull
    public static String sha512(@NotNull String data) {
        return sha512(data, UTF_8);
    }

    @NotNull
    public static String sha512(@NotNull InputStream data) {
        MessageDigest digest = newSha512Digest();
        update(digest, data);
        return encodeHex(digest.digest());
    }


}
