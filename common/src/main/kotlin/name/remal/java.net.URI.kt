package name.remal

import java.net.URI

fun URI.withScheme(scheme: String?): URI = URIUtils.withScheme(this, scheme)
fun URI.withSchemeSpecificPart(schemeSpecificPart: String): URI = URIUtils.withSchemeSpecificPart(this, schemeSpecificPart)
fun URI.withAuthority(authority: String?): URI = URIUtils.withAuthority(this, authority)
fun URI.withUserInfo(userInfo: String?): URI = URIUtils.withUserInfo(this, userInfo)
fun URI.withHost(host: String?): URI = URIUtils.withHost(this, host)
fun URI.withPort(port: Int): URI = URIUtils.withPort(this, port)
fun URI.withDefaultPort(): URI = URIUtils.withDefaultPort(this)
fun URI.withPath(path: String?): URI = URIUtils.withPath(this, path)
fun URI.withQuery(query: String?): URI = URIUtils.withQuery(this, query)
fun URI.withFragment(fragment: String?): URI = URIUtils.withFragment(this, fragment)
