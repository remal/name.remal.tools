package name.remal

import java.util.Arrays
import java.util.stream.LongStream

fun LongArray.stream(): LongStream = Arrays.stream(this)


fun LongStream.averageOrNull(): Double? = average().let { if (it.isPresent) it.asDouble else null }
