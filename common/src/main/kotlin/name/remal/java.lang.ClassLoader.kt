package name.remal

import name.remal.reflection.ClassLoaderUtils
import name.remal.reflection.ClassLoaderUtils.InstantiatedClassesPropagation
import java.io.File
import java.net.URL
import java.nio.file.Path

fun ClassLoader.getPackageOrNull(packageName: String) = ClassLoaderUtils.getPackageOrNull(this, packageName)

fun ClassLoader.tryLoadClass(className: String, initialize: Boolean = false): Class<*>? = try {
    Class.forName(className, initialize, this)
} catch (ignored: LinkageError) {
    null
} catch (ignored: ClassNotFoundException) {
    null
}

fun ClassLoader.addURL(vararg urls: URL) = ClassLoaderUtils.addURLsToClassLoader(this, *urls)
fun ClassLoader.addURL(vararg files: File) = addURL(*files.map { it.toURI().toURL() }.toTypedArray())
fun ClassLoader.addURL(vararg paths: Path) = addURL(*paths.map { it.toUri().toURL() }.toTypedArray())


fun <T, R> ClassLoader.forInstantiated(type: Class<T>, implementationType: Class<out T>, action: (T) -> R): R = ClassLoaderUtils.forInstantiated(this, type, implementationType, action)

fun <T, R> ClassLoader.forInstantiated(type: Class<T>, implementationType: Class<out T>, propagation: InstantiatedClassesPropagation, action: (T) -> R): R = ClassLoaderUtils.forInstantiated(this, type, implementationType, propagation, action)

fun <T, R> ClassLoader.forInstantiatedWithPropagatedPackage(type: Class<T>, implementationType: Class<out T>, action: (T) -> R): R = ClassLoaderUtils.forInstantiatedWithPropagatedPackage(this, type, implementationType, action)

@JvmName("forInstantiatedVoid")
fun <T> ClassLoader.forInstantiated(type: Class<T>, implementationType: Class<out T>, action: (T) -> Unit) = ClassLoaderUtils.forInstantiated(this, type, implementationType, action)

@JvmName("forInstantiatedVoid")
fun <T> ClassLoader.forInstantiated(type: Class<T>, implementationType: Class<out T>, propagation: InstantiatedClassesPropagation, action: (T) -> Unit) = ClassLoaderUtils.forInstantiated(this, type, implementationType, propagation, action)

@JvmName("forInstantiatedWithPropagatedPackageVoid")
fun <T> ClassLoader.forInstantiatedWithPropagatedPackage(type: Class<T>, implementationType: Class<out T>, action: (T) -> Unit) = ClassLoaderUtils.forInstantiatedWithPropagatedPackage(this, type, implementationType, action)
