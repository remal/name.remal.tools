package name.remal

import java.util.Collections.synchronizedSortedMap
import java.util.SortedMap

fun <K : Comparable<K>, V> SortedMap<K, V>.asSynchronized(): SortedMap<K, V> = synchronizedSortedMap<K, V>(this)
