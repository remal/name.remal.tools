package name.remal

import org.objectweb.asm.Opcodes.ACC_MANDATED
import org.objectweb.asm.Opcodes.ACC_STATIC_PHASE
import org.objectweb.asm.Opcodes.ACC_SYNTHETIC
import org.objectweb.asm.Opcodes.ACC_TRANSITIVE
import org.objectweb.asm.tree.ModuleRequireNode

var ModuleRequireNode.isTransitive: Boolean
    get() = (access and ACC_TRANSITIVE) != 0
    set(value) {
        access = if (value) access or ACC_TRANSITIVE else access and ACC_TRANSITIVE.inv()
    }

var ModuleRequireNode.isStatic: Boolean
    get() = (access and ACC_STATIC_PHASE) != 0
    set(value) {
        access = if (value) access or ACC_STATIC_PHASE else access and ACC_STATIC_PHASE.inv()
    }

var ModuleRequireNode.isSynthetic: Boolean
    get() = (access and ACC_SYNTHETIC) != 0
    set(value) {
        access = if (value) access or ACC_SYNTHETIC else access and ACC_SYNTHETIC.inv()
    }

var ModuleRequireNode.isMandated: Boolean
    get() = (access and ACC_MANDATED) != 0
    set(value) {
        access = if (value) access or ACC_MANDATED else access and ACC_MANDATED.inv()
    }
