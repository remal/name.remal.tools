package name.remal

import java.time.Duration

fun Duration.toSeconds() = seconds
fun Duration.toMicros() = toNanos() / 1000
