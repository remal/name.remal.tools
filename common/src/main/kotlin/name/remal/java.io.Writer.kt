package name.remal

import java.io.Writer

fun Writer.write(obj: Any?) = apply { append(obj) }

fun Writer.writeln() = apply { appendln() }
fun Writer.writeln(csq: CharSequence?) = apply { appendln(csq) }
fun Writer.writeln(obj: Any?) = apply { appendln(obj) }
