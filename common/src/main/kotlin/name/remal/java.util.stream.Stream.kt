package name.remal

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings
import java.util.Comparator
import java.util.HashMap
import java.util.HashSet
import java.util.LinkedHashMap
import java.util.SortedMap
import java.util.SortedSet
import java.util.TreeMap
import java.util.TreeSet
import java.util.stream.Collectors
import java.util.stream.Stream
import java.util.stream.StreamSupport

fun <T> emptyStream(): Stream<T> = Stream.of()
fun <T> streamOf(): Stream<T> = Stream.of()
fun <T> streamOf(vararg elements: T): Stream<T> = Stream.of(*elements)

operator fun <T> Stream<out T>.plus(other: Stream<out T>): Stream<T> = Stream.concat(this, other)

fun <T : Any> Stream<T?>.filterNotNull(): Stream<T> = filter { it != null }.uncheckedCast()
fun <T : Any, R : T> Stream<T?>.filterIsInstance(type: Class<R>): Stream<R> = filter { type.isInstance(it) }.uncheckedCast()
@SuppressFBWarnings("UPM_UNCALLED_PRIVATE_METHOD")
inline fun <T : Any, reified R : T> Stream<T?>.filterIsInstance(): Stream<R> = filterIsInstance(R::class.java)

@JvmName("flatMapCollection")
fun <T, R> Stream<T>.flatMap(mapper: (T) -> Collection<R>): Stream<R> = this.flatMap { mapper(it).stream() }

@JvmName("flatMapIterable")
fun <T, R> Stream<T>.flatMap(mapper: (T) -> Iterable<R>): Stream<R> = this.flatMap { StreamSupport.stream(mapper(it).spliterator(), false) }


fun <T> Stream<T>.firstOrNull(): T? = findFirst().orNull

fun <T> Stream<T>.asIterable(): Iterable<T> = object : Iterable<T> {
    override fun iterator() = this@asIterable.iterator()
}

fun <T> Stream<T>.toList(): List<T> = collect(Collectors.toList())
fun <T> Stream<T>.toSet(): Set<T> = collect(Collectors.toCollection(::LinkedHashSet))
fun <T> Stream<T>.toHashSet(): Set<T> = collect(Collectors.toCollection(::HashSet))
fun <T : Comparable<T>> Stream<T>.toSortedSet(): SortedSet<T> = collect(Collectors.toCollection { TreeSet<T>() })
fun <K, V, T : Pair<K, V>> Stream<T>.toMap(): Map<K, V> = collect(Collectors.toMap({ it.first }, { it.second }, { _, value -> value }, ::LinkedHashMap))
fun <K, V, T : Pair<K, V>> Stream<T>.toHashMap(): Map<K, V> = collect(Collectors.toMap({ it.first }, { it.second }, { _, value -> value }, ::HashMap))
fun <K : Comparable<K>, V, T : Pair<K, V>> Stream<T>.toSortedMap(): SortedMap<K, V> = collect(Collectors.toMap({ it.first }, { it.second }, { _, value -> value }, ::TreeMap))
fun <V, K> Stream<V>.toMap(keyExtractor: (value: V) -> K): Map<K, V> = collect(Collectors.toMap(keyExtractor, { it }, { _, value -> value }, ::LinkedHashMap))
fun <V, K> Stream<V>.toHashMap(keyExtractor: (value: V) -> K): Map<K, V> = collect(Collectors.toMap(keyExtractor, { it }, { _, value -> value }, ::HashMap))
fun <V, K : Comparable<K>> Stream<V>.toSortedMap(keyExtractor: (value: V) -> K): Map<K, V> = collect(Collectors.toMap(keyExtractor, { it }, { _, value -> value }, ::TreeMap))

@JvmName("joinCharSequencesToString")
fun <T : CharSequence> Stream<T>.joinToString(separator: CharSequence = ", ", prefix: CharSequence = "", postfix: CharSequence = ""): String = collect(Collectors.joining(separator, prefix, postfix))

fun <T> Stream<T>.joinToString(separator: CharSequence = ", ", prefix: CharSequence = "", postfix: CharSequence = ""): String = map { "$it" }.joinToString(separator, prefix, postfix)

fun <T : Comparable<T>> Stream<T>.max(): T? = max(Comparator.naturalOrder()).orNull
fun <T : Comparable<T>> Stream<T>.min(): T? = min(Comparator.naturalOrder()).orNull

fun <T> Stream<T>.reduce(reducer: (first: T, second: T) -> T): T? = collect(Collectors.reducing(reducer)).orNull
