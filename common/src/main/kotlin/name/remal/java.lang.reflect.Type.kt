package name.remal

import name.remal.reflection.TypeUtils
import java.lang.reflect.Type

fun Type.asClass() = TypeUtils.toClass(this)
