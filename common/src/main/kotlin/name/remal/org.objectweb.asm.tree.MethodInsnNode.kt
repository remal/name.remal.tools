package name.remal

import org.objectweb.asm.Opcodes.INVOKESPECIAL
import org.objectweb.asm.Opcodes.INVOKESTATIC
import org.objectweb.asm.Type.getConstructorDescriptor
import org.objectweb.asm.Type.getInternalName
import org.objectweb.asm.Type.getMethodDescriptor
import org.objectweb.asm.tree.MethodInsnNode
import java.lang.reflect.Constructor
import java.lang.reflect.Method

fun MethodInsnNode.matches(ctor: Constructor<*>): Boolean {
    if (INVOKESPECIAL != opcode) return false
    if (getInternalName(ctor.declaringClass) != owner) return false
    if ("<init>" != name) return false
    if (getConstructorDescriptor(ctor) != desc) return false
    return true
}

fun MethodInsnNode.matches(method: Method): Boolean {
    if (INVOKESPECIAL == opcode) return false
    if (method.isStatic xor (INVOKESTATIC == opcode)) return false
    if (getInternalName(method.declaringClass) != owner) return false
    if (method.name != name) return false
    if (getMethodDescriptor(method) != desc) return false
    return true
}
