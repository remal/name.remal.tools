package name.remal

fun Appendable.append(obj: Any?) = append(obj?.arrayToString())
fun Appendable.appendln(obj: Any?) = append(obj?.arrayToString()).appendln()
