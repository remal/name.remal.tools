package name.remal

import java.util.Collections.synchronizedNavigableSet
import java.util.NavigableSet

fun <T> NavigableSet<T>.asSynchronized(): NavigableSet<T> = synchronizedNavigableSet<T>(this)
