package name.remal

val CLASS_FILE_NAME_SUFFIX = ".class"

fun classNameToResourceName(className: String) = className.replace('.', '/') + CLASS_FILE_NAME_SUFFIX

fun resourceNameToClassName(resourceName: String): String {
    if (!isClassResourceName(resourceName)) throw IllegalArgumentException("$resourceName is not class-resource")
    return resourceName.substring(0, resourceName.length - CLASS_FILE_NAME_SUFFIX.length).replace('/', '.')
}

fun isClassResourceName(resourceName: String) = resourceName.endsWith(CLASS_FILE_NAME_SUFFIX)
