package name.remal

import name.remal.annotation.AnnotationUtils

val <T : Annotation> T.annotationType: Class<T>
    get() = (this as JavaAnnotation).annotationType().uncheckedCast()

fun <T : Annotation> T.withAttributes(attributes: Map<String, Any>?) = AnnotationUtils.withAttributes(this, attributes)
