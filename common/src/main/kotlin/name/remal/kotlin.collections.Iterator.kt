package name.remal

import java.util.Spliterator.ORDERED
import java.util.Spliterators.spliteratorUnknownSize
import java.util.stream.Stream
import java.util.stream.StreamSupport

fun <T> Iterator<T>.asStream(): Stream<T> {
    return StreamSupport.stream(
        spliteratorUnknownSize(this, ORDERED),
        false
    )
}
