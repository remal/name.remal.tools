package name.remal

import org.jdom2.Document
import org.jdom2.Element

fun Document.clearNamespaces() = apply {
    descendants.forEach { node ->
        if (node is Element) {
            if (node.hasAttributes()) {
                node.attributes.forEach { it.namespace = null }
            }
            node.namespace = null
            if (node.hasAdditionalNamespaces()) {
                node.additionalNamespaces.toList().forEach(node::removeNamespaceDeclaration)
            }
        }
    }
}
