package name.remal

import name.remal.annotation.AnnotationUtils
import java.lang.annotation.ElementType
import java.lang.annotation.Inherited
import java.lang.reflect.AnnotatedElement
import java.lang.reflect.Method

val <T : Annotation> Class<T>.isLangCoreAnnotation get() = AnnotationUtils.isLangCoreAnnotation(this)

val <T : Annotation> Class<T>.isInherited: Boolean get() = null != getDeclaredAnnotation(Inherited::class.java)

val <T : Annotation> Class<T>.canAnnotateTypes: Boolean get() = canAnnotate(ElementType.TYPE)
val <T : Annotation> Class<T>.canAnnotateFields: Boolean get() = canAnnotate(ElementType.FIELD)
val <T : Annotation> Class<T>.canAnnotateMethods: Boolean get() = canAnnotate(ElementType.METHOD)
val <T : Annotation> Class<T>.canAnnotateParameters: Boolean get() = canAnnotate(ElementType.PARAMETER)
val <T : Annotation> Class<T>.canAnnotateConstructors: Boolean get() = canAnnotate(ElementType.CONSTRUCTOR)
val <T : Annotation> Class<T>.canAnnotateLocalVariables: Boolean get() = canAnnotate(ElementType.LOCAL_VARIABLE)
val <T : Annotation> Class<T>.canAnnotateAnnotations: Boolean get() = canAnnotate(ElementType.ANNOTATION_TYPE)
val <T : Annotation> Class<T>.canAnnotatePackages: Boolean get() = canAnnotate(ElementType.PACKAGE)
val <T : Annotation> Class<T>.canAnnotateTypeParameters: Boolean get() = canAnnotate(ElementType.TYPE_PARAMETER)
val <T : Annotation> Class<T>.canAnnotateTypeUses: Boolean get() = canAnnotate(ElementType.TYPE_USE)

val <T : Annotation> Class<T>.attributeMethods: List<Method> get() = AnnotationUtils.getAttributeMethods(this)


fun <T : Annotation> Class<T>.canAnnotate(elementType: ElementType) = AnnotationUtils.canAnnotate(this, elementType)
fun <T : Annotation> Class<T>.canAnnotate(element: AnnotatedElement) = AnnotationUtils.canAnnotate(this, element)

fun <T : Annotation> Class<T>.newAnnotationInstance(attributes: Map<String, Any?>? = null) = AnnotationUtils.newAnnotationInstance(this, attributes)
