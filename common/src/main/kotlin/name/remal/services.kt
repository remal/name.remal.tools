package name.remal

fun <T> loadServices(serviceType: Class<T>, serviceSubtypeName: String, classLoader: ClassLoader): Iterable<T> = Services.loadServices(serviceType, serviceSubtypeName, classLoader)
fun <T> loadServices(serviceType: Class<T>, serviceSubtypeName: String): Iterable<T> = Services.loadServices(serviceType, serviceSubtypeName)
fun <T> loadServices(serviceType: Class<T>, classLoader: ClassLoader): Iterable<T> = Services.loadServices(serviceType, classLoader)
fun <T> loadServices(serviceType: Class<T>): Iterable<T> = Services.loadServices(serviceType)

fun <T> loadServicesList(serviceType: Class<T>, serviceSubtypeName: String, classLoader: ClassLoader): List<T> = Services.loadServicesList(serviceType, serviceSubtypeName, classLoader)
fun <T> loadServicesList(serviceType: Class<T>, serviceSubtypeName: String): List<T> = Services.loadServicesList(serviceType, serviceSubtypeName)
fun <T> loadServicesList(serviceType: Class<T>, classLoader: ClassLoader): List<T> = Services.loadServicesList(serviceType, classLoader)
fun <T> loadServicesList(serviceType: Class<T>): List<T> = Services.loadServicesList(serviceType)

fun <T, R : T> loadImplementationClasses(serviceType: Class<T>, serviceSubtypeName: String, classLoader: ClassLoader): Iterable<Class<R>> = Services.loadImplementationClasses(serviceType, serviceSubtypeName, classLoader)
fun <T, R : T> loadImplementationClasses(serviceType: Class<T>, serviceSubtypeName: String): Iterable<Class<R>> = Services.loadImplementationClasses(serviceType, serviceSubtypeName)
fun <T, R : T> loadImplementationClasses(serviceType: Class<T>, classLoader: ClassLoader): Iterable<Class<R>> = Services.loadImplementationClasses(serviceType, classLoader)
fun <T, R : T> loadImplementationClasses(serviceType: Class<T>): Iterable<Class<R>> = Services.loadImplementationClasses(serviceType)

fun <T, R : T> loadImplementationClassesList(serviceType: Class<T>, serviceSubtypeName: String, classLoader: ClassLoader): List<Class<R>> = Services.loadImplementationClassesList(serviceType, serviceSubtypeName, classLoader)
fun <T, R : T> loadImplementationClassesList(serviceType: Class<T>, serviceSubtypeName: String): List<Class<R>> = Services.loadImplementationClassesList(serviceType, serviceSubtypeName)
fun <T, R : T> loadImplementationClassesList(serviceType: Class<T>, classLoader: ClassLoader): List<Class<R>> = Services.loadImplementationClassesList(serviceType, classLoader)
fun <T, R : T> loadImplementationClassesList(serviceType: Class<T>): List<Class<R>> = Services.loadImplementationClassesList(serviceType)
