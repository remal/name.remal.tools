package name.remal

import name.remal.collections.ObservableCollection
import name.remal.collections.ObservableCollections

fun <T> MutableCollection<T>.asObservable(): ObservableCollection<T> = ObservableCollections.observableCollection(this)
