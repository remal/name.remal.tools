package name.remal

import java.util.Collections.synchronizedMap
import java.util.stream.Stream

fun <K, V, M : Map<K, V>> M?.nullIfEmpty(): M? = nullIf(Map<*, *>::isEmpty)

fun <K, V> Map<K, V>.asSynchronized(): Map<K, V> = synchronizedMap<K, V>(this)

inline fun <K, V> Map<K, V>.forEachIndexed(action: (index: Int, key: K, value: V) -> Unit) {
    this.entries.forEachIndexed { index, entry ->
        action(index, entry.key, entry.value)
    }
}

fun <K, V> Map<K?, V>.filterNotNullKeys(): Map<K, V> = filterKeys { it != null }.uncheckedCast()
fun <K, V> Map<K, V?>.filterNotNullValues(): Map<K, V> = filterValues { it != null }.uncheckedCast()

fun <K, V> Map<K, V>.stream(): Stream<Map.Entry<K, V>> = entries.stream()
