@file:ExcludeFromCodeCoverage

package name.remal

import name.remal.gradle_plugins.api.ExcludeFromCodeCoverage
import java.io.InputStream
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets.UTF_8

fun encodeHex(data: ByteArray) = Codecs.encodeHex(data)
fun decodeHex(data: String) = Codecs.decodeHex(data)

fun encodeBase64(data: ByteArray) = Codecs.encodeBase64(data)
fun encodeBase64URLSafe(data: ByteArray) = Codecs.encodeBase64URLSafe(data)
fun decodeBase64(data: String) = Codecs.decodeBase64(data)

fun encodeURIComponent(data: String, charset: Charset = UTF_8) = Codecs.encodeURIComponent(data, charset)
fun decodeURIComponent(data: String, charset: Charset = UTF_8) = Codecs.decodeURIComponent(data, charset)

fun newMd5Digest() = Codecs.newMd5Digest()
fun md5(data: ByteArray) = Codecs.md5(data)
fun md5(data: InputStream) = Codecs.md5(data)
fun md5(data: String, charset: Charset = UTF_8) = Codecs.md5(data, charset)

fun newSha256Digest() = Codecs.newSha256Digest()
fun sha256(data: ByteArray) = Codecs.sha256(data)
fun sha256(data: InputStream) = Codecs.sha256(data)
fun sha256(data: String, charset: Charset = UTF_8) = Codecs.sha256(data, charset)

fun newSha512Digest() = Codecs.newSha512Digest()
fun sha512(data: ByteArray) = Codecs.sha512(data)
fun sha512(data: InputStream) = Codecs.sha512(data)
fun sha512(data: String, charset: Charset = UTF_8) = Codecs.sha512(data, charset)
