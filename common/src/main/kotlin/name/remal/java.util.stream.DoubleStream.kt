package name.remal

import java.util.Arrays
import java.util.stream.DoubleStream

fun FloatArray.stream(): DoubleStream = Arrays.stream(this.toDoubleArray())
fun DoubleArray.stream(): DoubleStream = Arrays.stream(this)


fun DoubleStream.averageOrNull(): Double? = average().let { if (it.isPresent) it.asDouble else null }
