package name.remal

import name.remal.collections.ObservableList
import name.remal.collections.ObservableMap
import name.remal.collections.ObservableSet
import java.util.Collections.newSetFromMap
import java.util.Deque
import java.util.LinkedList
import java.util.NavigableMap
import java.util.NavigableSet
import java.util.Queue
import java.util.SortedMap
import java.util.SortedSet
import java.util.TreeMap
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.CopyOnWriteArrayList

fun <T> queueOf(): Queue<T> = LinkedList()
fun <T> queueOf(vararg elements: T): Queue<T> = LinkedList<T>().apply { addAll(elements) }

fun <T> dequeOf(): Deque<T> = LinkedList()
fun <T> dequeOf(vararg elements: T): Deque<T> = LinkedList<T>().apply { addAll(elements) }

fun <T> concurrentSetOf(): MutableSet<T> = newSetFromMap<T>(ConcurrentHashMap())
fun <T> concurrentSetOf(vararg elements: T): MutableSet<T> = newSetFromMap<T>(ConcurrentHashMap()).apply { addAll(elements) }

fun <T> synchronizedSetOf(): MutableSet<T> = mutableSetOf<T>().asSynchronized()
fun <T> synchronizedSetOf(vararg elements: T): MutableSet<T> = mutableSetOf(*elements).asSynchronized()

fun <T> synchronizedSortedSetOf(): SortedSet<T> = sortedSetOf<T>().asSynchronized()
fun <T> synchronizedSortedSetOf(vararg elements: T): SortedSet<T> = sortedSetOf(*elements).asSynchronized()

fun <T> synchronizedNavigableSetOf(): NavigableSet<T> = sortedSetOf<T>().asSynchronized()
fun <T> synchronizedNavigableSetOf(vararg elements: T): NavigableSet<T> = sortedSetOf(*elements).asSynchronized()

fun <T> synchronizedListOf(): MutableList<T> = mutableListOf<T>().asSynchronized()
fun <T> synchronizedListOf(vararg elements: T): MutableList<T> = mutableListOf(*elements).asSynchronized()

fun <T> copyOnWriteListOf() = CopyOnWriteArrayList<T>()
fun <T> copyOnWriteListOf(vararg elements: T) = CopyOnWriteArrayList<T>().apply { addAll(elements) }


fun <K : Comparable<K>, V> navigableMapOf(): NavigableMap<K, V> = TreeMap<K, V>()
fun <K : Comparable<K>, V> navigableMapOf(vararg pairs: Pair<K, V>): NavigableMap<K, V> = TreeMap<K, V>().apply { putAll(pairs) }

fun <K, V> concurrentMapOf(): ConcurrentMap<K, V> = ConcurrentHashMap()
fun <K, V> concurrentMapOf(vararg pairs: Pair<K, V>): ConcurrentMap<K, V> = ConcurrentHashMap<K, V>().apply { putAll(pairs) }

fun <K, V> synchronizedMapOf(): MutableMap<K, V> = mutableMapOf<K, V>().asSynchronized()
fun <K, V> synchronizedMapOf(vararg pairs: Pair<K, V>): MutableMap<K, V> = mutableMapOf(*pairs).asSynchronized()

fun <K : Comparable<K>, V> synchronizedSortedMapOf(): SortedMap<K, V> = sortedMapOf<K, V>().asSynchronized()
fun <K : Comparable<K>, V> synchronizedSortedMapOf(vararg pairs: Pair<K, V>): SortedMap<K, V> = sortedMapOf(*pairs).asSynchronized()

fun <K : Comparable<K>, V> synchronizedNavigableMapOf(): NavigableMap<K, V> = navigableMapOf<K, V>().asSynchronized()
fun <K : Comparable<K>, V> synchronizedNavigableMapOf(vararg pairs: Pair<K, V>): NavigableMap<K, V> = navigableMapOf(*pairs).asSynchronized()

fun <T> observableListOf(vararg elements: T): ObservableList<T> = mutableListOf(*elements).asObservable()
fun <T> observableSetOf(vararg elements: T): ObservableSet<T> = mutableSetOf(*elements).asObservable()
fun <K, V> observableMapOf(vararg pairs: Pair<K, V>): ObservableMap<K, V> = mutableMapOf(*pairs).asObservable()


inline fun <T> buildList(builderAction: ListBuilder<T>.() -> Unit): List<T> {
    val list = mutableListOf<T>()
    builderAction(object : ListBuilder<T> {
        override fun isEmpty() = list.isEmpty()
        override fun contains(element: T) = list.contains(element)
        override fun add(element: T) = list.add(element)
        override fun clear() = list.clear()
    })
    return list
}

interface ListBuilder<T> {
    fun isEmpty(): Boolean
    fun isNotEmpty(): Boolean = !isEmpty()
    operator fun contains(element: T): Boolean
    fun containsAll(elements: Iterable<T>) = elements.all { contains(it) }
    fun add(element: T): Boolean
    fun addAll(elements: Iterable<T>) = 1 <= elements.count { add(it) }
    fun addAll(vararg elements: T) = 1 <= elements.count { add(it) }
    fun clear()
}


inline fun <T> buildSet(builderAction: SetBuilder<T>.() -> Unit): Set<T> {
    val set = mutableSetOf<T>()
    builderAction(object : SetBuilder<T> {
        override fun isEmpty() = set.isEmpty()
        override fun contains(element: T) = set.contains(element)
        override fun add(element: T) = set.add(element)
        override fun remove(element: T) = set.remove(element)
        override fun clear() = set.clear()
    })
    return set
}

interface SetBuilder<T> {
    fun isEmpty(): Boolean
    fun isNotEmpty(): Boolean = !isEmpty()
    operator fun contains(element: T): Boolean
    fun containsAll(elements: Iterable<T>) = elements.all { contains(it) }
    fun add(element: T): Boolean
    fun addAll(elements: Iterable<T>) = 1 <= elements.count { add(it) }
    fun addAll(vararg elements: T) = 1 <= elements.count { add(it) }
    fun remove(element: T): Boolean
    fun removeAll(elements: Iterable<T>) = 1 <= elements.count { remove(it) }
    fun clear()
}


inline fun <K, V> buildMap(builderAction: MapBuilder<K, V>.() -> Unit): Map<K, V> {
    val map = mutableMapOf<K, V>()
    builderAction(object : MapBuilder<K, V> {
        override fun isEmpty() = map.isEmpty()
        override fun containsKey(key: K) = map.containsKey(key)
        override fun containsValue(value: V) = map.containsValue(value)
        override fun put(key: K, value: V) = map.put(key, value)
        override fun remove(key: K): V? = map.remove(key)
        override fun clear() = map.clear()
    })
    return map
}

interface MapBuilder<K, V> {
    fun isEmpty(): Boolean
    fun isNotEmpty(): Boolean = !isEmpty()
    fun containsKey(key: K): Boolean
    fun containsValue(value: V): Boolean
    fun put(key: K, value: V): V?
    fun putAll(from: Map<K, V>) = from.forEach { key, value -> put(key, value) }
    fun remove(key: K): V?
    fun removeAll(keys: Iterable<K>) = keys.forEach { remove(it) }
    fun clear()
    operator fun contains(key: K) = containsKey(key)
    operator fun set(key: K, value: V) = put(key, value)
}
