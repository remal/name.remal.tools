package name.remal

import org.w3c.dom.Document
import org.w3c.dom.DocumentFragment
import org.w3c.dom.Element
import java.io.File
import java.io.OutputStream
import java.io.Writer
import java.net.URI
import javax.xml.transform.dom.DOMResult
import javax.xml.transform.stream.StreamResult

fun newTransformResult(outputStream: OutputStream, location: URI? = null) = StreamResult(outputStream).apply { systemId = location?.toASCIIString() }
fun newTransformResult(writer: Writer, location: URI? = null) = StreamResult(writer).apply { systemId = location?.toASCIIString() }
fun newTransformResult(file: File) = StreamResult(file)
fun newTransformResult(document: Document, location: URI? = null) = DOMResult(document, location?.toASCIIString() ?: document.documentURI)
fun newTransformResult(documentFragment: DocumentFragment, location: URI? = null) = DOMResult(documentFragment, location?.toASCIIString() ?: documentFragment.ownerDocument?.documentURI)
fun newTransformResult(element: Element, location: URI? = null) = DOMResult(element, location?.toASCIIString() ?: element.ownerDocument?.documentURI)
