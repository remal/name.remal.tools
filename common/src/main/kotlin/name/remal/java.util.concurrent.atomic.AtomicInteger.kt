package name.remal

import java.util.concurrent.atomic.AtomicInteger

operator fun AtomicInteger.inc() = apply { incrementAndGet() }
operator fun AtomicInteger.dec() = apply { decrementAndGet() }

operator fun AtomicInteger.plusAssign(value: AtomicInteger) = plusAssign(value.get())
operator fun AtomicInteger.plusAssign(value: Int) {
    addAndGet(value)
}

operator fun AtomicInteger.minusAssign(value: AtomicInteger) = minusAssign(value.get())
operator fun AtomicInteger.minusAssign(value: Int) {
    addAndGet(-1 * value)
}


fun AtomicInteger.incrementAndGetUnsigned(): Int {
    while (true) {
        val result = incrementAndGet()
        if (0 <= result) return result
        if (compareAndSet(result, 0)) {
            return 0
        }
    }
}

fun AtomicInteger.decrementAndGetUnsigned(): Int {
    while (true) {
        val result = decrementAndGet()
        if (0 <= result) return result
        if (compareAndSet(result, Int.MAX_VALUE)) {
            return Int.MAX_VALUE
        }
    }
}
