package name.remal

import name.remal.reflection.HierarchyUtils
import java.lang.reflect.Method
import java.lang.reflect.Modifier

val Method.isStatic get() = Modifier.isStatic(modifiers)
val Method.isFinal get() = Modifier.isFinal(modifiers)
val Method.isSynchronized get() = Modifier.isSynchronized(modifiers)
val Method.isNative get() = Modifier.isNative(modifiers)
val Method.isAbstract get() = Modifier.isAbstract(modifiers)
val Method.isStrict get() = Modifier.isStrict(modifiers)

val Method.canBeOverridden get() = HierarchyUtils.canBeOverridden(this)
val Method.hasGenericParameters get() = genericParameterTypes.any { it !is Class<*> }

val Method.hierarchy: List<Method> get() = HierarchyUtils.getHierarchy(this)


fun Method.isOverriddenBy(other: Method) = HierarchyUtils.isOverriddenBy(this, other)
