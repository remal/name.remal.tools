package name.remal

val Class<*>.isKotlinClass: Boolean
    get() {
        KotlinMetadataClassHolder.kotlinMetadataClass?.let {
            return null != getDeclaredAnnotation(it)
        }
        return false
    }


private class KotlinMetadataClassHolder {
    companion object {

        @JvmField
        val kotlinMetadataClass: Class<out Annotation>? = try {
            Class.forName("kotlin.Metadata").uncheckedCast()
        } catch (ignored: ClassNotFoundException) {
            null
        }

    }
}
