package name.remal

import java.util.SortedMap

fun <V, K> Iterable<V>.toMutableMap(keyExtractor: (value: V) -> K): MutableMap<K, V> = mutableMapOf<K, V>().also {
    for (value in this) {
        val key = keyExtractor(value)
        it[key] = value
    }
}

fun <V, K> Iterable<V>.toMap(keyExtractor: (value: V) -> K): Map<K, V> = toMutableMap(keyExtractor).toMap()

fun <V, K> Iterable<V>.toHashMap(keyExtractor: (value: V) -> K): MutableMap<K, V> = hashMapOf<K, V>().also {
    for (value in this) {
        val key = keyExtractor(value)
        it[key] = value
    }
}

fun <V, K : Comparable<K>> Iterable<V>.toSortedMap(keyExtractor: (value: V) -> K): SortedMap<K, V> = sortedMapOf<K, V>().also {
    for (value in this) {
        val key = keyExtractor(value)
        it[key] = value
    }
}


fun <T> Iterable<T>.toStream() = iterator().asStream()
