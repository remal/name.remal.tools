package name.remal

import name.remal.reflection.HierarchyUtils
import java.lang.reflect.Parameter

val Parameter.hierarchy: List<Parameter> get() = HierarchyUtils.getHierarchy(this)
