package name.remal

import java.util.Arrays
import java.util.stream.IntStream

fun ByteArray.stream(): IntStream = Arrays.stream(this.toIntArray())
fun ShortArray.stream(): IntStream = Arrays.stream(this.toIntArray())
fun IntArray.stream(): IntStream = Arrays.stream(this)


fun IntStream.averageOrNull(): Double? = average().let { if (it.isPresent) it.asDouble else null }
