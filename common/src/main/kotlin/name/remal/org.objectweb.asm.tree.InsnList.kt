package name.remal

import org.objectweb.asm.tree.AbstractInsnNode
import org.objectweb.asm.tree.InsnList

fun InsnList.isEmpty() = 0 == size()
fun InsnList.isNotEmpty() = !isEmpty()
fun InsnList?.isNullOrEmpty() = this == null || isEmpty()

fun InsnList.addAll(insns: Iterable<AbstractInsnNode>) {
    insns.forEach { add(it) }
}

inline fun InsnList.forEach(action: (node: AbstractInsnNode) -> Unit) {
    var node = first
    while (node != null) {
        action(node)
        node = node.next
    }
}

fun InsnList.toList() = buildList<AbstractInsnNode> {
    this@toList.forEach { add(it) }
}
