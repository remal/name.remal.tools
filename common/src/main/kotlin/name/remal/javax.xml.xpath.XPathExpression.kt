package name.remal

import org.w3c.dom.Node
import org.w3c.dom.NodeList
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathExpression
import javax.xml.xpath.XPathFactory

private val xpathFactory: XPathFactory = XPathFactory.newInstance()

fun newXPathExpression(expression: String): XPathExpression = xpathFactory.newXPath().compile(expression)


fun XPathExpression.evaluateAsBoolean(item: Any): Boolean {
    return evaluate(item, XPathConstants.BOOLEAN) as? Boolean ?: false
}

fun XPathExpression.evaluateAsNumber(item: Any): Number? {
    return evaluate(item, XPathConstants.NUMBER) as Number?
}

fun XPathExpression.evaluateAsString(item: Any): String? {
    return evaluate(item, XPathConstants.STRING) as String?
}

fun XPathExpression.evaluateAsNodeList(item: Any): NodeList {
    return evaluate(item, XPathConstants.NODESET) as NodeList
}

fun XPathExpression.evaluateAsNode(item: Any): Node? {
    return evaluate(item, XPathConstants.NODE) as Node?
}
