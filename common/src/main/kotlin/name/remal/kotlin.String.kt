package name.remal

fun String.replace(fromTo: Map<String, String>, ignoreCase: Boolean = false): String {
    var result = this
    for (entry in fromTo) {
        result = result.replace(entry.key, entry.value, ignoreCase)
    }
    return result
}
