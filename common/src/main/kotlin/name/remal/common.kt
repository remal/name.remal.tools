package name.remal

fun isClassExists(className: String, classLoader: ClassLoader? = null): Boolean {
    try {
        Class.forName(className, false, classLoader)
        return true
    } catch (ignored: ClassNotFoundException) {
        return false
    } catch (ignored: LinkageError) {
        return false
    }
}


inline fun <R> retry(retries: Int, retriableThrowableTypes: List<Class<out Throwable>>, delayMillis: Long, action: () -> R): R {
    if (retries < 1) throw IllegalArgumentException("retries < 1")
    if (delayMillis < 0) throw IllegalArgumentException("delayMillis < 0")

    var retry = 0
    while (true) {
        ++retry
        try {
            return action()

        } catch (throwable: Throwable) {
            if (retry == retries) throw throwable
            if (retriableThrowableTypes.none { throwable.contains(it) }) throw throwable
            if (1 <= delayMillis) Thread.sleep(delayMillis)
        }
    }
}

inline fun <R> retry(retries: Int, retriableThrowableTypes: List<Class<out Throwable>>, action: () -> R) = retry(retries, retriableThrowableTypes, 0L, action)
inline fun <R> retry(retries: Int, retriableThrowableType: Class<out Throwable>, delayMillis: Long, action: () -> R) = retry(retries, listOf(retriableThrowableType), delayMillis, action)
inline fun <R> retry(retries: Int, retriableThrowableType: Class<out Throwable>, action: () -> R) = retry(retries, listOf(retriableThrowableType), action)
inline fun <R> retry(retries: Int, delayMillis: Long, action: () -> R) = retry(retries, Exception::class.java, delayMillis, action)
inline fun <R> retry(retries: Int, action: () -> R) = retry(retries, 0L, action)
