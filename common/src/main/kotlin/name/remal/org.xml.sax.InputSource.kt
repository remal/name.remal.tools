package name.remal

import org.xml.sax.InputSource
import java.io.File
import java.io.InputStream
import java.io.Reader
import java.io.StringReader
import java.net.URI
import java.net.URL

fun newSAXInputSource(url: URL) = InputSource(url.toURI().toASCIIString())
fun newSAXInputSource(file: File) = InputSource(file.toURI().toASCIIString())
fun newSAXInputSource(inputStream: InputStream, location: URI? = null) = InputSource(inputStream).apply { systemId = location?.toASCIIString() }
fun newSAXInputSource(reader: Reader, location: URI? = null) = InputSource(reader).apply { systemId = location?.toASCIIString() }
fun newSAXInputSource(string: String, location: URI? = null) = newSAXInputSource(StringReader(string), location)
