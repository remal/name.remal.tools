package name.remal

import org.slf4j.Logger

fun Logger.trace(throwable: Throwable, msg: String? = null) = trace(msg ?: throwable.toString(), throwable)
fun Logger.debug(throwable: Throwable, msg: String? = null) = debug(msg ?: throwable.toString(), throwable)
fun Logger.info(throwable: Throwable, msg: String? = null) = info(msg ?: throwable.toString(), throwable)
fun Logger.warn(throwable: Throwable, msg: String? = null) = warn(msg ?: throwable.toString(), throwable)
fun Logger.error(throwable: Throwable, msg: String? = null) = error(msg ?: throwable.toString(), throwable)
