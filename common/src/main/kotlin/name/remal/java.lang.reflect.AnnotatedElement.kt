package name.remal

import name.remal.annotation.AnnotationUtils
import java.lang.reflect.AnnotatedElement

fun <T : Annotation> AnnotatedElement.getMetaAnnotations(type: Class<T>): List<T> = AnnotationUtils.getMetaAnnotations(this, type)
fun <T : Annotation> AnnotatedElement.getMetaAnnotation(type: Class<T>) = AnnotationUtils.getMetaAnnotation(this, type)
fun <T : Annotation> AnnotatedElement.hasMetaAnnotation(type: Class<T>) = null != getMetaAnnotation(type)
