package name.remal

import java.util.Collections.synchronizedSortedSet
import java.util.SortedSet

fun <T> SortedSet<T>.asSynchronized(): SortedSet<T> = synchronizedSortedSet<T>(this)
