package name.remal

import org.objectweb.asm.Opcodes.ACC_MANDATED
import org.objectweb.asm.Opcodes.ACC_OPEN
import org.objectweb.asm.tree.ModuleNode

var ModuleNode.isOpen: Boolean
    get() = (access and ACC_OPEN) != 0
    set(value) {
        access = if (value) access or ACC_OPEN else access and ACC_OPEN.inv()
    }

var ModuleNode.isMandated: Boolean
    get() = (access and ACC_MANDATED) != 0
    set(value) {
        access = if (value) access or ACC_MANDATED else access and ACC_MANDATED.inv()
    }
