package name.remal

fun <T, C : Collection<T>> C?.nullIfEmpty(): C? = nullIf(Collection<*>::isEmpty)
