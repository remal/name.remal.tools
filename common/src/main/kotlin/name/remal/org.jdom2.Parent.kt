package name.remal

import org.jdom2.Element
import org.jdom2.Namespace
import org.jdom2.Parent
import org.jdom2.filter.ElementFilter
import org.jdom2.util.IteratorIterable

fun Parent.getDescendants(name: String): IteratorIterable<Element> = getDescendants(ElementFilter(name))
fun Parent.getDescendants(namespace: Namespace): IteratorIterable<Element> = getDescendants(ElementFilter(namespace))
fun Parent.getDescendants(name: String?, namespace: Namespace?): IteratorIterable<Element> = getDescendants(ElementFilter(name, namespace))
