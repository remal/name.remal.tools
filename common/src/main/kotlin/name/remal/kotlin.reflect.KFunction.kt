package name.remal

import kotlin.reflect.KFunction
import kotlin.reflect.jvm.javaConstructor
import kotlin.reflect.jvm.javaMethod

fun <T : Annotation> KFunction<*>.getMetaAnnotations(type: Class<T>): List<T> {
    val result = mutableSetOf<T>()
    javaMethod?.getMetaAnnotations(type)?.forEach { result.add(it) }
    javaConstructor?.getMetaAnnotations(type)?.forEach { result.add(it) }
    return result.toList()
}

fun <T : Annotation> KFunction<*>.getMetaAnnotation(type: Class<T>) = getMetaAnnotations(type).firstOrNull()
fun <T : Annotation> KFunction<*>.hasMetaAnnotation(type: Class<T>) = null != getMetaAnnotation(type)
