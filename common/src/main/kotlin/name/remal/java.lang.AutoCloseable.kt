package name.remal

inline fun <T : AutoCloseable?, R> T.use(block: (T) -> R): R {
    var closed = false
    try {
        return block(this)

    } catch (e: Exception) {
        closed = true
        try {
            this?.close()
        } catch (closeException: Exception) {
            e.addSuppressed(closeException)
        }
        throw e

    } finally {
        if (!closed) {
            this?.close()
        }
    }
}
