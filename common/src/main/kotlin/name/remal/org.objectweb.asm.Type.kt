package name.remal

import org.objectweb.asm.Opcodes.ILOAD
import org.objectweb.asm.Opcodes.IRETURN
import org.objectweb.asm.Opcodes.ISTORE
import org.objectweb.asm.Type
import org.objectweb.asm.Type.VOID_TYPE
import org.objectweb.asm.tree.InsnNode
import org.objectweb.asm.tree.VarInsnNode

val Type.isPrimitive: Boolean get() = descriptor.length == 1


fun Type.toLoadVarInsn(varIndex: Int) = when (this) {
    VOID_TYPE -> throw IllegalArgumentException("There is no load instruction for void type")
    else -> VarInsnNode(getOpcode(ILOAD), varIndex)
}

fun Type.toStoreVarInsn(varIndex: Int) = when (this) {
    VOID_TYPE -> throw IllegalArgumentException("There is no store instruction for void type")
    else -> VarInsnNode(getOpcode(ISTORE), varIndex)
}

fun Type.toReturnInsn() = InsnNode(getOpcode(IRETURN))


fun Iterable<Type>.toLoadVarInsns(firstVarIndex: Int = 0) = buildList<VarInsnNode> {
    var varIndex = firstVarIndex
    for (type in this@toLoadVarInsns) {
        add(type.toLoadVarInsn(varIndex))
        varIndex += type.size
    }
}

fun Array<Type>.toLoadVarInsns(firstVarIndex: Int = 0) = toList().toLoadVarInsns(firstVarIndex)
