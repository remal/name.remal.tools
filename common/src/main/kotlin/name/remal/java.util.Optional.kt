package name.remal

import java.util.Optional

val <T> Optional<T>.orNull: T? get() = orElse(null)
