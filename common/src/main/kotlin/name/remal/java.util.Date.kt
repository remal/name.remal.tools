package name.remal

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.OffsetTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.Date

fun Date.toLocalDateTime(): LocalDateTime = LocalDateTime.ofInstant(toInstant(), ZoneId.systemDefault())
fun Date.toLocalDate(): LocalDate = toLocalDateTime().toLocalDate()
fun Date.toLocalTime(): LocalTime = toLocalDateTime().toLocalTime()

fun Date.toZonedDateTime(): ZonedDateTime = ZonedDateTime.ofInstant(toInstant(), ZoneId.systemDefault())

fun Date.toOffsetDateTime(): OffsetDateTime = OffsetDateTime.ofInstant(toInstant(), ZoneId.systemDefault())
fun Date.toOffsetTime(): OffsetTime = toOffsetDateTime().toOffsetTime()
