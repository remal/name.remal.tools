package name.remal

import org.w3c.dom.Node
import java.io.File
import java.io.InputStream
import java.io.Reader
import java.io.StringReader
import java.net.URI
import java.net.URL
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamSource

fun newTransformSource(url: URL) = StreamSource(url.toURI().toASCIIString())
fun newTransformSource(file: File) = StreamSource(file)
fun newTransformSource(inputStream: InputStream, location: URI? = null) = StreamSource(inputStream, location?.toASCIIString())
fun newTransformSource(reader: Reader, location: URI? = null) = StreamSource(reader, location?.toASCIIString())
fun newTransformSource(node: Node, location: URI? = null) = DOMSource(node, location?.toASCIIString() ?: node.ownerDocument?.documentURI)
fun newTransformSource(string: String, location: URI? = null) = newTransformSource(StringReader(string), location)
