package name.remal

import java.util.Arrays
import java.util.SortedMap
import java.util.stream.Stream

fun <T> Array<T>.stream(): Stream<T> = Arrays.stream(this)


fun <V, K> Array<V>.toMutableMap(keyExtractor: (value: V) -> K): MutableMap<K, V> = mutableMapOf<K, V>().also {
    for (value in this) {
        val key = keyExtractor(value)
        it[key] = value
    }
}

fun <V, K> Array<V>.toMap(keyExtractor: (value: V) -> K): Map<K, V> = toMutableMap(keyExtractor).toMap()

fun <V, K> Array<V>.toHashMap(keyExtractor: (value: V) -> K): MutableMap<K, V> = hashMapOf<K, V>().also {
    for (value in this) {
        val key = keyExtractor(value)
        it[key] = value
    }
}

fun <V, K : Comparable<K>> Array<V>.toSortedMap(keyExtractor: (value: V) -> K): SortedMap<K, V> = sortedMapOf<K, V>().also {
    for (value in this) {
        val key = keyExtractor(value)
        it[key] = value
    }
}
