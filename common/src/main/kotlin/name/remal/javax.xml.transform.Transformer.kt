package name.remal

import org.w3c.dom.Document
import org.w3c.dom.DocumentFragment
import org.w3c.dom.Element
import org.w3c.dom.Node
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.io.Reader
import java.io.StringWriter
import java.io.Writer
import java.net.URI
import java.net.URL
import javax.xml.transform.Result
import javax.xml.transform.Source
import javax.xml.transform.Transformer

fun Transformer.transform(source: Source) = TransformerWithSource(this, source)
fun Transformer.transform(url: URL) = this.transform(newTransformSource(url))
fun Transformer.transform(file: File) = this.transform(newTransformSource(file))
fun Transformer.transform(inputStream: InputStream, location: URI? = null) = this.transform(newTransformSource(inputStream, location))
fun Transformer.transform(reader: Reader, location: URI? = null) = this.transform(newTransformSource(reader, location))
fun Transformer.transform(node: Node, location: URI? = null) = this.transform(newTransformSource(node, location))
fun Transformer.transform(string: String, location: URI? = null) = this.transform(newTransformSource(string, location))

class TransformerWithSource(
    private val transformer: Transformer,
    private val source: Source
) {

    fun into(result: Result) {
        transformer.transform(source, result)
    }

    fun into(outputStream: OutputStream, location: URI? = null) = into(newTransformResult(outputStream, location))
    fun into(writer: Writer, location: URI? = null) = into(newTransformResult(writer, location))
    fun into(document: Document, location: URI? = null) = into(newTransformResult(document, location))
    fun into(documentFragment: DocumentFragment, location: URI? = null) = into(newTransformResult(documentFragment, location))
    fun into(element: Element, location: URI? = null) = into(newTransformResult(element, location))

    fun intoString(location: URI? = null): String = StringWriter().use { into(it, location); it.toString() }

}
