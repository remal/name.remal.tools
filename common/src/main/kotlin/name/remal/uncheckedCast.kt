@file:ExcludeFromCodeCoverage
@file:Suppress("UNCHECKED_CAST", "NOTHING_TO_INLINE")

package name.remal

import name.remal.gradle_plugins.api.ExcludeFromCodeCoverage

inline fun <T> Any.uncheckedCast(): T = this as T
