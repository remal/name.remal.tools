package name.remal

import org.jdom2.Attribute

fun Attribute.setValue(value: Any) = setValue(value.toString())!!
