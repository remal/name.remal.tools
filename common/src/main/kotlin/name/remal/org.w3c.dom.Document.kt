package name.remal

import org.w3c.dom.Document
import org.xml.sax.InputSource
import java.io.File
import java.io.InputStream
import java.io.Reader
import java.net.URI
import java.net.URL
import javax.xml.XMLConstants.FEATURE_SECURE_PROCESSING
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

@Suppress("NOTHING_TO_INLINE")
private inline fun newDocumentBuilder(): DocumentBuilder = DocumentBuilderFactory.newInstance()
    .apply { setFeature(FEATURE_SECURE_PROCESSING, true) }
    .newDocumentBuilder()

fun newDOMDocument(): Document = newDocumentBuilder().newDocument()

fun parseDOMDocument(inputSource: InputSource): Document = newDocumentBuilder().parse(inputSource)
fun parseDOMDocument(file: File) = parseDOMDocument(newSAXInputSource(file))
fun parseDOMDocument(url: URL) = parseDOMDocument(newSAXInputSource(url))
fun parseDOMDocument(inputStream: InputStream, location: URI? = null) = parseDOMDocument(newSAXInputSource(inputStream, location))
fun parseDOMDocument(reader: Reader, location: URI? = null) = parseDOMDocument(newSAXInputSource(reader, location))
fun parseDOMDocument(string: String, location: URI? = null) = parseDOMDocument(newSAXInputSource(string, location))
