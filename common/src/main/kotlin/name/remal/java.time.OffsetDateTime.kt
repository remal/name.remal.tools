package name.remal

import java.time.OffsetDateTime
import java.util.Date

fun OffsetDateTime.toDate(): Date = Date.from(toInstant())
