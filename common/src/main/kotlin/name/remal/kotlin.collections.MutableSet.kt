package name.remal

import name.remal.collections.ObservableCollections
import name.remal.collections.ObservableSet
import java.util.Collections.synchronizedSet

fun <T> MutableSet<T>.asSynchronized(): MutableSet<T> = synchronizedSet<T>(this)

fun <T> MutableSet<T>.asObservable(): ObservableSet<T> = ObservableCollections.observableSet(this)
