package name.remal

import org.objectweb.asm.Opcodes.ACC_ABSTRACT
import org.objectweb.asm.Opcodes.ACC_ANNOTATION
import org.objectweb.asm.Opcodes.ACC_ENUM
import org.objectweb.asm.Opcodes.ACC_FINAL
import org.objectweb.asm.Opcodes.ACC_INTERFACE
import org.objectweb.asm.Opcodes.ACC_MODULE
import org.objectweb.asm.Opcodes.ACC_PRIVATE
import org.objectweb.asm.Opcodes.ACC_PROTECTED
import org.objectweb.asm.Opcodes.ACC_PUBLIC
import org.objectweb.asm.Opcodes.ACC_SUPER
import org.objectweb.asm.Opcodes.ACC_SYNTHETIC
import org.objectweb.asm.tree.AnnotationNode
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.TypeAnnotationNode

var ClassNode.isPublic: Boolean
    get() = (access and ACC_PUBLIC) != 0
    set(value) {
        if (value) {
            access = access or ACC_PUBLIC
            access = access and ACC_PROTECTED.inv()
            access = access and ACC_PRIVATE.inv()
        } else {
            access = access and ACC_PUBLIC.inv()
        }
    }

var ClassNode.isProtected: Boolean
    get() = (access and ACC_PROTECTED) != 0
    set(value) {
        if (value) {
            access = access and ACC_PUBLIC.inv()
            access = access or ACC_PROTECTED
            access = access and ACC_PRIVATE.inv()
        } else {
            access = access and ACC_PROTECTED.inv()
        }
    }

var ClassNode.isPrivate: Boolean
    get() = (access and ACC_PRIVATE) != 0
    set(value) {
        if (value) {
            access = access and ACC_PUBLIC.inv()
            access = access and ACC_PROTECTED.inv()
            access = access or ACC_PRIVATE
        } else {
            access = access and ACC_PRIVATE.inv()
        }
    }

var ClassNode.isPackagePrivate: Boolean
    get() = !isPublic && !isProtected && !isPrivate
    set(value) {
        if (value) {
            isPublic = false
            isProtected = false
            isPrivate = false
        }
    }

var ClassNode.isFinal: Boolean
    get() = (access and ACC_FINAL) != 0
    set(value) {
        access = if (value) access or ACC_FINAL else access and ACC_FINAL.inv()
    }

var ClassNode.isSuper: Boolean
    get() = (access and ACC_SUPER) != 0
    set(value) {
        access = if (value) access or ACC_SUPER else access and ACC_SUPER.inv()
    }

var ClassNode.isInterface: Boolean
    get() = (access and ACC_INTERFACE) != 0
    set(value) {
        access = if (value) access or ACC_INTERFACE else access and ACC_INTERFACE.inv()
    }

var ClassNode.isAbstract: Boolean
    get() = (access and ACC_ABSTRACT) != 0
    set(value) {
        access = if (value) access or ACC_ABSTRACT else access and ACC_ABSTRACT.inv()
    }

var ClassNode.isSynthetic: Boolean
    get() = (access and ACC_SYNTHETIC) != 0
    set(value) {
        access = if (value) access or ACC_SYNTHETIC else access and ACC_SYNTHETIC.inv()
    }

var ClassNode.isAnnotation: Boolean
    get() = (access and ACC_ANNOTATION) != 0
    set(value) {
        access = if (value) access or ACC_ANNOTATION else access and ACC_ANNOTATION.inv()
    }

var ClassNode.isEnum: Boolean
    get() = (access and ACC_ENUM) != 0
    set(value) {
        access = if (value) access or ACC_ENUM else access and ACC_ENUM.inv()
    }

var ClassNode.isModule: Boolean
    get() = (access and ACC_MODULE) != 0
    set(value) {
        access = if (value) access or ACC_MODULE else access and ACC_MODULE.inv()
    }


val ClassNode.isNotStaticInnerClass: Boolean
    get() {
        val innerClass = innerClasses?.firstOrNull { it.name == this.name } ?: return false

        val hasOuterInstanceField = fields?.firstOrNull { it.isSynthetic && it.isFinal && it.desc == "L${innerClass.outerName};" } != null
        if (!hasOuterInstanceField) return false

        val hasCtor = methods?.firstOrNull { it.isConstructor && it.desc == "(L${innerClass.outerName};)V" } != null
        if (!hasCtor) return false

        return true
    }


val ClassNode.isKotlinClass: Boolean
    get() = sequenceOf(visibleAnnotations, invisibleAnnotations)
        .filterNotNull()
        .flatten()
        .any { it.desc == "Lkotlin/Metadata;" }

val ClassNode.allAnnotations: List<AnnotationNode>
    get() = sequenceOf(visibleAnnotations, invisibleAnnotations)
        .filterNotNull()
        .flatten()
        .toList()

val ClassNode.allTypeAnnotations: List<TypeAnnotationNode>
    get() = sequenceOf(visibleTypeAnnotations, invisibleTypeAnnotations)
        .filterNotNull()
        .flatten()
        .toList()
