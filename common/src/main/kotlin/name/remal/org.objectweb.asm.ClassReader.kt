package name.remal

import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassVisitor

fun ClassReader.accept(classVisitor: ClassVisitor) = accept(classVisitor, 0)
