package name.remal

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.nio.channels.SeekableByteChannel
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.CopyOption
import java.nio.file.DirectoryStream
import java.nio.file.FileStore
import java.nio.file.FileVisitOption
import java.nio.file.FileVisitResult
import java.nio.file.FileVisitResult.CONTINUE
import java.nio.file.FileVisitor
import java.nio.file.Files
import java.nio.file.LinkOption
import java.nio.file.OpenOption
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.attribute.FileAttribute
import java.nio.file.attribute.FileAttributeView
import java.nio.file.attribute.FileTime
import java.nio.file.attribute.PosixFilePermission
import java.nio.file.attribute.UserPrincipal
import java.util.function.BiPredicate
import java.util.stream.Stream

fun Path.newInputStream(vararg options: OpenOption): InputStream = Files.newInputStream(this, *options)
fun Path.newOutputStream(vararg options: OpenOption): OutputStream = Files.newOutputStream(this, *options)
fun Path.newByteChannel(options: Set<OpenOption>, vararg attrs: FileAttribute<*>): SeekableByteChannel = Files.newByteChannel(this, options, *attrs)
fun Path.newByteChannel(vararg options: OpenOption): SeekableByteChannel = Files.newByteChannel(this, *options)
fun Path.newDirectoryStream(): DirectoryStream<Path> = Files.newDirectoryStream(this)
fun Path.newDirectoryStream(glob: String): DirectoryStream<Path> = Files.newDirectoryStream(this, glob)
fun Path.newDirectoryStream(filter: (path: Path) -> Boolean): DirectoryStream<Path> = Files.newDirectoryStream(this, filter)
fun Path.createFile(vararg attrs: FileAttribute<*>): Path = Files.createFile(this, *attrs)
fun Path.createDirectory(vararg attrs: FileAttribute<*>): Path = Files.createDirectory(this, *attrs)
fun Path.createDirectories(vararg attrs: FileAttribute<*>): Path = Files.createDirectories(this, *attrs)
fun Path.createTempFile(prefix: String = "temp-", suffix: String = ".temp", vararg attrs: FileAttribute<*>): Path = Files.createTempFile(this, prefix, suffix, *attrs)
fun Path.createTempDirectory(prefix: String = "temp-", vararg attrs: FileAttribute<*>): Path = Files.createTempDirectory(this, prefix, *attrs)
fun Path.createSymbolicLink(target: Path, vararg attrs: FileAttribute<*>): Path = Files.createSymbolicLink(this, target, *attrs)
fun Path.createHardLink(target: Path): Path = Files.createLink(this, target)
fun Path.delete() = Files.deleteIfExists(this)
fun Path.copy(target: Path, vararg options: CopyOption): Path = Files.copy(this, target, *options)
fun Path.move(target: Path, vararg options: CopyOption): Path = Files.move(this, target, *options)
fun Path.readSymbolicLink(): Path = Files.readSymbolicLink(this)
val Path.fileStore: FileStore get() = Files.getFileStore(this)
fun Path.isSameFile(other: Path) = Files.isSameFile(this, other)
val Path.isHidden get() = Files.isHidden(this)
fun Path.probeContentType(): String? = Files.probeContentType(this)
fun <V : FileAttributeView> Path.getFileAttributeView(type: Class<V>, vararg options: LinkOption): V? = Files.getFileAttributeView(this, type, *options)
fun <V : BasicFileAttributes> Path.readAttributes(type: Class<V>, vararg options: LinkOption): V? = Files.readAttributes(this, type, *options)
fun Path.setAttribute(attribute: String, value: Any?, vararg options: LinkOption): Path = Files.setAttribute(this, attribute, value, *options)
fun Path.getAttribute(attribute: String, vararg options: LinkOption): Any? = Files.getAttribute(this, attribute, *options)
fun Path.readAttributes(attributes: String, vararg options: LinkOption): Map<String, Any> = Files.readAttributes(this, attributes, *options)
fun Path.getPosixFilePermissions(vararg options: LinkOption): Set<PosixFilePermission> = Files.getPosixFilePermissions(this, *options)
var Path.posixFilePermissions: Set<PosixFilePermission>
    get() = Files.getPosixFilePermissions(this)
    set(value) {
        Files.setPosixFilePermissions(this, value)
    }

fun Path.getOwner(vararg options: LinkOption): UserPrincipal = Files.getOwner(this, *options)
var Path.owner: UserPrincipal
    get() = Files.getOwner(this)
    set(value) {
        Files.setOwner(this, value)
    }
val Path.isSymbolicLink get() = Files.isSymbolicLink(this)
fun Path.isDirectory(vararg options: LinkOption) = Files.isDirectory(this, *options)
val Path.isDirectory get() = Files.isDirectory(this)
fun Path.isRegularFile(vararg options: LinkOption) = Files.isRegularFile(this, *options)
val Path.isRegularFile get() = Files.isRegularFile(this)
fun Path.getLastModifiedTime(vararg options: LinkOption): FileTime = Files.getLastModifiedTime(this, *options)
var Path.lastModifiedTime: FileTime
    get() = Files.getLastModifiedTime(this)
    set(value) {
        Files.setLastModifiedTime(this, value)
    }

val Path.size get() = Files.size(this)
fun Path.exists(vararg options: LinkOption) = Files.exists(this, *options)
val Path.exists get() = Files.exists(this)
fun Path.notExists(vararg options: LinkOption) = Files.notExists(this, *options)
val Path.notExists get() = Files.notExists(this)
val Path.isReadable get() = Files.isReadable(this)
val Path.isWritable get() = Files.isWritable(this)
val Path.isExecutable get() = Files.isWritable(this)
fun Path.walkFileTree(visitor: FileVisitor<Path>, maxDepth: Int = Int.MAX_VALUE, options: Set<FileVisitOption> = setOf()) = apply {
    if (this.exists) Files.walkFileTree(this, options, maxDepth, visitor)
}

fun Path.newBufferedReader(charset: Charset = UTF_8): BufferedReader = Files.newBufferedReader(this, charset)
fun Path.newBufferedWriter(charset: Charset = UTF_8, vararg options: OpenOption): BufferedWriter = Files.newBufferedWriter(this, charset, *options)
fun Path.readAllBytes(): ByteArray = Files.readAllBytes(this)
fun Path.readAllLines(charset: Charset = UTF_8): List<String> = Files.readAllLines(this, charset)
fun Path.write(bytes: ByteArray, vararg options: OpenOption): Path = Files.write(this, bytes, *options)
fun Path.write(lines: Iterable<CharSequence>, charset: Charset = UTF_8, vararg options: OpenOption): Path = Files.write(this, lines, charset, *options)
fun Path.list(): Stream<Path> = Files.list(this)
fun Path.walk(maxDepth: Int = Int.MAX_VALUE, vararg options: FileVisitOption): Stream<Path> = Files.walk(this, maxDepth, *options)
fun Path.find(maxDepth: Int = Int.MAX_VALUE, matcher: (path: Path, attributes: BasicFileAttributes) -> Boolean = { _, _ -> true }, vararg options: FileVisitOption): Stream<Path> = Files.find(this, maxDepth, BiPredicate { path, attributes -> matcher(path, attributes) }, *options)
fun Path.lines(charset: Charset = UTF_8): Stream<String> = Files.lines(this, charset)


@SuppressFBWarnings("NPMC_NON_PRODUCTIVE_METHOD_CALL")
fun Path.createParentDirectories(vararg attrs: FileAttribute<*>) = apply { this.toAbsolutePath().parent?.createDirectories(*attrs) }

fun Path.readText(charset: Charset = UTF_8): String = String(readAllBytes(), charset)
fun Path.deleteRecursively() = walkFileTree(object : SimpleFileVisitor<Path>() {
    override fun visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult {
        file.delete()
        return CONTINUE
    }

    override fun postVisitDirectory(dir: Path, exc: IOException?): FileVisitResult {
        if (exc != null) throw exc
        dir.delete()
        return CONTINUE
    }
})
