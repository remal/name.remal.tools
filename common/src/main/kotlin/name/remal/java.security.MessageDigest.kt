package name.remal

import java.io.InputStream
import java.security.MessageDigest

fun MessageDigest.update(data: InputStream) {
    val buffer = ByteArray(1024)
    while (true) {
        val read = data.read(buffer, 0, buffer.size)
        if (read < 0) break
        update(buffer, 0, read)
    }
}

fun MessageDigest.digestHex(data: ByteArray) = encodeHex(digest(data))
fun MessageDigest.digestHex() = encodeHex(digest())
