package name.remal

import java.nio.ByteBuffer
import java.nio.channels.WritableByteChannel

fun WritableByteChannel.write(bytes: ByteArray) = write(ByteBuffer.wrap(bytes))
