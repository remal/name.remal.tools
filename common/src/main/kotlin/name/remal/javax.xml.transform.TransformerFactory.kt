package name.remal

import org.w3c.dom.Node
import java.io.File
import java.io.InputStream
import java.io.Reader
import java.net.URI
import java.net.URL
import javax.xml.XMLConstants.ACCESS_EXTERNAL_DTD
import javax.xml.XMLConstants.ACCESS_EXTERNAL_STYLESHEET
import javax.xml.XMLConstants.FEATURE_SECURE_PROCESSING
import javax.xml.transform.Templates
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory

private val transformerFactory: TransformerFactory = TransformerFactory.newInstance().apply {
    setFeature(FEATURE_SECURE_PROCESSING, true)
    setAttribute(ACCESS_EXTERNAL_DTD, "all")
    setAttribute(ACCESS_EXTERNAL_STYLESHEET, "all")
}

fun newTemplates(url: URL): Templates = transformerFactory.newTemplates(newTransformSource(url))
fun newTemplates(file: File): Templates = transformerFactory.newTemplates(newTransformSource(file))
fun newTemplates(inputStream: InputStream, location: URI? = null): Templates = transformerFactory.newTemplates(newTransformSource(inputStream, location))
fun newTemplates(reader: Reader, location: URI? = null): Templates = transformerFactory.newTemplates(newTransformSource(reader, location))
fun newTemplates(node: Node, location: URI? = null): Templates = transformerFactory.newTemplates(newTransformSource(node, location))
fun newTemplates(string: String, location: URI? = null): Templates = transformerFactory.newTemplates(newTransformSource(string, location))

fun newTransformer(): Transformer = transformerFactory.newTransformer()
fun newTransformer(url: URL): Transformer = transformerFactory.newTransformer(newTransformSource(url))
fun newTransformer(file: File): Transformer = transformerFactory.newTransformer(newTransformSource(file))
fun newTransformer(inputStream: InputStream, location: URI? = null): Transformer = transformerFactory.newTransformer(newTransformSource(inputStream, location))
fun newTransformer(reader: Reader, location: URI? = null): Transformer = transformerFactory.newTransformer(newTransformSource(reader, location))
fun newTransformer(node: Node, location: URI? = null): Transformer = transformerFactory.newTransformer(newTransformSource(node, location))
fun newTransformer(string: String, location: URI? = null): Transformer = transformerFactory.newTransformer(newTransformSource(string, location))
