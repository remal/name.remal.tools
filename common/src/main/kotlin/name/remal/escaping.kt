package name.remal

import org.jetbrains.annotations.Contract

fun escapeRegex(string: String) = string
    .replace("\\", "\\\\")
    .replace(".", "\\.")
    .replace("[", "\\[")
    .replace("]", "\\]")
    .replace("(", "\\(")
    .replace(")", "\\)")
    .replace("?", "\\?")
    .replace("*", "\\*")
    .replace("+", "\\+")
    .replace("{", "\\{")
    .replace("}", "\\}")
    .replace("^", "\\^")
    .replace("\$", "\\\$")
    .replace("\n", "\\n")
    .replace("\r", "\\r")
    .replace("\t", "\\t")

@JvmName("escapeRegexNullable")
@Contract(value = "null->null; !null->!null", pure = true)
fun escapeRegex(string: String?) = if (string != null) escapeRegex(string) else null

fun escapeKotlin(string: String) = escapeJava(string).replace("\$", "\\\$")

@JvmName("escapeKotlinNullable")
@Contract(value = "null->null; !null->!null", pure = true)
fun escapeKotlin(string: String?) = if (string != null) escapeKotlin(string) else null
