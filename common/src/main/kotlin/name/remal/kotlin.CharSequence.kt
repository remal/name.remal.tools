package name.remal

fun <T : CharSequence> T?.nullIfEmpty(): T? = nullIf(CharSequence::isEmpty)
