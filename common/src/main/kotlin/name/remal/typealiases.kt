@file:ExcludeFromCodeCoverage
@file:Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package name.remal

import name.remal.gradle_plugins.api.ExcludeFromCodeCoverage

typealias JavaAnnotation = java.lang.annotation.Annotation
typealias JavaAnnotationTarget = java.lang.annotation.Target
typealias JavaRepeatable = java.lang.annotation.Repeatable
