package name.remal

import java.lang.reflect.Member
import java.lang.reflect.Modifier

val Member.isPublic get() = Modifier.isPublic(modifiers)
val Member.isProtected get() = Modifier.isProtected(modifiers)
val Member.isPackagePrivate get() = !Modifier.isPublic(modifiers) && !Modifier.isProtected(modifiers) && !Modifier.isPrivate(modifiers)
val Member.isPrivate get() = Modifier.isPrivate(modifiers)
