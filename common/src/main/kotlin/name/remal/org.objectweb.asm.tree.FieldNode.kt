package name.remal

import org.objectweb.asm.Opcodes.ACC_FINAL
import org.objectweb.asm.Opcodes.ACC_MANDATED
import org.objectweb.asm.Opcodes.ACC_PRIVATE
import org.objectweb.asm.Opcodes.ACC_PROTECTED
import org.objectweb.asm.Opcodes.ACC_PUBLIC
import org.objectweb.asm.Opcodes.ACC_STATIC
import org.objectweb.asm.Opcodes.ACC_SYNTHETIC
import org.objectweb.asm.Opcodes.ACC_TRANSIENT
import org.objectweb.asm.Opcodes.ACC_VOLATILE
import org.objectweb.asm.tree.AnnotationNode
import org.objectweb.asm.tree.FieldNode
import org.objectweb.asm.tree.TypeAnnotationNode

var FieldNode.isPublic: Boolean
    get() = (access and ACC_PUBLIC) != 0
    set(value) {
        if (value) {
            access = access or ACC_PUBLIC
            access = access and ACC_PROTECTED.inv()
            access = access and ACC_PRIVATE.inv()
        } else {
            access = access and ACC_PUBLIC.inv()
        }
    }

var FieldNode.isProtected: Boolean
    get() = (access and ACC_PROTECTED) != 0
    set(value) {
        if (value) {
            access = access and ACC_PUBLIC.inv()
            access = access or ACC_PROTECTED
            access = access and ACC_PRIVATE.inv()
        } else {
            access = access and ACC_PROTECTED.inv()
        }
    }

var FieldNode.isPrivate: Boolean
    get() = (access and ACC_PRIVATE) != 0
    set(value) {
        if (value) {
            access = access and ACC_PUBLIC.inv()
            access = access and ACC_PROTECTED.inv()
            access = access or ACC_PRIVATE
        } else {
            access = access and ACC_PRIVATE.inv()
        }
    }

var FieldNode.isPackagePrivate: Boolean
    get() = !isPublic && !isProtected && !isPrivate
    set(value) {
        if (value) {
            isPublic = false
            isProtected = false
            isPrivate = false
        }
    }

var FieldNode.isStatic: Boolean
    get() = (access and ACC_STATIC) != 0
    set(value) {
        access = if (value) access or ACC_STATIC else access and ACC_STATIC.inv()
    }

var FieldNode.isFinal: Boolean
    get() = (access and ACC_FINAL) != 0
    set(value) {
        access = if (value) access or ACC_FINAL else access and ACC_FINAL.inv()
    }

var FieldNode.isVolatile: Boolean
    get() = (access and ACC_VOLATILE) != 0
    set(value) {
        access = if (value) access or ACC_VOLATILE else access and ACC_VOLATILE.inv()
    }

var FieldNode.isTransient: Boolean
    get() = (access and ACC_TRANSIENT) != 0
    set(value) {
        access = if (value) access or ACC_TRANSIENT else access and ACC_TRANSIENT.inv()
    }

var FieldNode.isSynthetic: Boolean
    get() = (access and ACC_SYNTHETIC) != 0
    set(value) {
        access = if (value) access or ACC_SYNTHETIC else access and ACC_SYNTHETIC.inv()
    }

var FieldNode.isMandated: Boolean
    get() = (access and ACC_MANDATED) != 0
    set(value) {
        access = if (value) access or ACC_MANDATED else access and ACC_MANDATED.inv()
    }


val FieldNode.allAnnotations: List<AnnotationNode>
    get() = sequenceOf(visibleAnnotations, invisibleAnnotations)
        .filterNotNull()
        .flatten()
        .toList()

val FieldNode.allTypeAnnotations: List<TypeAnnotationNode>
    get() = sequenceOf(visibleTypeAnnotations, invisibleTypeAnnotations)
        .filterNotNull()
        .flatten()
        .toList()
