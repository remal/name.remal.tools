package name.remal

import java.util.Collections.synchronizedList

fun <T> List<T>.asSynchronized(): List<T> = synchronizedList<T>(this)

fun <T> List<T>.subList(fromIndex: Int): List<T> = this.subList(fromIndex, this.size)
