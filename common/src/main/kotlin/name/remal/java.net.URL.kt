package name.remal

import java.net.URL

fun URL.withProtocol(protocol: String): URL = URLUtils.withProtocol(this, protocol)
fun URL.withAuthority(authority: String): URL = URLUtils.withAuthority(this, authority)
fun URL.withUserInfo(userInfo: String?): URL = URLUtils.withUserInfo(this, userInfo)
fun URL.withHost(host: String): URL = URLUtils.withHost(this, host)
fun URL.withPort(port: Int): URL = URLUtils.withPort(this, port)
fun URL.withDefaultPort(): URL = URLUtils.withDefaultPort(this)
fun URL.withPath(path: String): URL = URLUtils.withPath(this, path)
fun URL.withQuery(query: String?): URL = URLUtils.withQuery(this, query)
fun URL.withRef(ref: String?): URL = URLUtils.withRef(this, ref)
