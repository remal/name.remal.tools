package name.remal

import name.remal.asm.AsmUtils.fromJavaToBytecodeAnnotationValue
import org.objectweb.asm.Type
import org.objectweb.asm.Type.getDescriptor
import org.objectweb.asm.tree.AnnotationNode
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.jvm.javaMethod

val AnnotationNode.mapValues: Map<String, Any>
    get() = buildMap {
        if (values != null) {
            val iterator = values.iterator()
            while (iterator.hasNext()) {
                val name = iterator.next() as String
                val value = iterator.next()
                put(name, value)
            }
        }
    }


operator fun AnnotationNode.get(fieldName: String): Any? {
    if (values == null) return null
    val iterator = values.iterator()
    while (iterator.hasNext()) {
        val name = iterator.next() as String
        val value = iterator.next()
        if (fieldName == name) {
            return value
        }
    }
    return null
}

private fun AnnotationNode.getImpl(annotationProperty: KProperty1<out Annotation, *>): Any? {
    val method = annotationProperty.getter.javaMethod ?: throw IllegalArgumentException("annotationProperty must be direct annotation method reference")
    if (desc != getDescriptor(method.declaringClass)) throw IllegalArgumentException("$method is not a method of $desc")
    return get(method.name)
}

@JvmName("getByte")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Byte>): Byte? = getImpl(annotationProperty) as? Byte

@JvmName("getBoolean")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Boolean>): Boolean? = getImpl(annotationProperty) as? Boolean

@JvmName("getChar")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Char>): Char? = getImpl(annotationProperty) as? Char

@JvmName("getShort")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Short>): Short? = getImpl(annotationProperty) as? Short

@JvmName("getInt")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Int>): Int? = getImpl(annotationProperty) as? Int

@JvmName("getLong")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Long>): Long? = getImpl(annotationProperty) as? Long

@JvmName("getFloat")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Float>): Float? = getImpl(annotationProperty) as? Float

@JvmName("getDouble")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Double>): Double? = getImpl(annotationProperty) as? Double

@JvmName("getString")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, String>): String? = getImpl(annotationProperty) as? String

@JvmName("getClass")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, KClass<*>>): Type? = getImpl(annotationProperty) as? Type

@JvmName("getEnum")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Enum<*>>): Array<String>? = (getImpl(annotationProperty) as? Array<*>)?.nullIf { isArrayOf<String>() }?.uncheckedCast()

@JvmName("getAnnotation")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Annotation>): AnnotationNode? = getImpl(annotationProperty) as? AnnotationNode

@JvmName("getByteArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<Byte>>): MutableList<Byte>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { it !is Byte } }?.uncheckedCast()

@JvmName("getBooleanArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<Boolean>>): MutableList<Boolean>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { it !is Boolean } }?.uncheckedCast()

@JvmName("getCharArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<Char>>): MutableList<Char>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { it !is Char } }?.uncheckedCast()

@JvmName("getShortArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<Short>>): MutableList<Short>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { it !is Short } }?.uncheckedCast()

@JvmName("getIntArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<Int>>): MutableList<Int>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { it !is Int } }?.uncheckedCast()

@JvmName("getLongArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<Long>>): MutableList<Long>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { it !is Long } }?.uncheckedCast()

@JvmName("getFloatArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<Float>>): MutableList<Float>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { it !is Float } }?.uncheckedCast()

@JvmName("getDoubleArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<Double>>): MutableList<Double>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { it !is Double } }?.uncheckedCast()

@JvmName("getStringArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<String>>): MutableList<String>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { it !is String } }?.uncheckedCast()

@JvmName("getClassArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<KClass<*>>>): MutableList<Type>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { it !is Type } }?.uncheckedCast()

@JvmName("getEnumArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<Enum<*>>>): MutableList<Array<String>>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { !(it is Array<*> && it.isArrayOf<String>()) } }?.uncheckedCast()

@JvmName("getAnnotationArray")
operator fun AnnotationNode.get(annotationProperty: KProperty1<out Annotation, Array<out Annotation>>): MutableList<AnnotationNode>? = (getImpl(annotationProperty) as? MutableList<*>)?.nullIf { any { it !is AnnotationNode } }?.uncheckedCast()


operator fun AnnotationNode.set(fieldName: String, value: Any?) {
    if (value == null) {
        if (values != null) {
            val iterator = values.iterator()
            while (iterator.hasNext()) {
                val name = iterator.next() as String
                if (fieldName == name) iterator.remove()
                iterator.next()
                if (fieldName == name) iterator.remove()
            }
        }

    } else {
        val transformedValue = fromJavaToBytecodeAnnotationValue(value)
        var replaced = false
        if (values == null) values = mutableListOf()
        (0 until values.size step 2).forEach { index ->
            val name = values[index] as String
            if (fieldName == name) {
                values[index + 1] = transformedValue
                replaced = true
            }
        }
        if (!replaced) {
            values.add(fieldName)
            values.add(transformedValue)
        }
    }
}

private fun AnnotationNode.setImpl(annotationProperty: KProperty1<out Annotation, *>, value: Any?) {
    val method = annotationProperty.getter.javaMethod ?: throw IllegalArgumentException("annotationProperty must be direct annotation method reference")
    if (desc != getDescriptor(method.declaringClass)) throw IllegalArgumentException("$method is not a method of $desc")
    set(method.name, value)
}

@JvmName("setByte")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Byte>, value: Byte?) = setImpl(annotationProperty, value)

@JvmName("setBoolean")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Boolean>, value: Boolean?) = setImpl(annotationProperty, value)

@JvmName("setChar")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Char>, value: Char?) = setImpl(annotationProperty, value)

@JvmName("setShort")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Short>, value: Short?) = setImpl(annotationProperty, value)

@JvmName("setInt")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Int>, value: Int?) = setImpl(annotationProperty, value)

@JvmName("setLong")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Long>, value: Long?) = setImpl(annotationProperty, value)

@JvmName("setFloat")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Float>, value: Float?) = setImpl(annotationProperty, value)

@JvmName("setDouble")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Double>, value: Double?) = setImpl(annotationProperty, value)

@JvmName("setString")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, String>, value: String?) = setImpl(annotationProperty, value)

@JvmName("setType")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, KClass<*>>, value: Type?) = setImpl(annotationProperty, value)

@JvmName("setTypeAsClass")
operator fun <T : Any> AnnotationNode.set(annotationProperty: KProperty1<out Annotation, KClass<T>>, value: Class<out T>?) = setImpl(annotationProperty, value)

@JvmName("setTypeAsKClass")
operator fun <T : Any> AnnotationNode.set(annotationProperty: KProperty1<out Annotation, KClass<T>>, value: KClass<out T>?) = setImpl(annotationProperty, value?.java)

@JvmName("setEnum")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Enum<*>>, value: Array<String>?) = setImpl(annotationProperty, value)

@JvmName("setEnumAsEnum")
operator fun <T : Enum<T>> AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Enum<T>>, value: Enum<T>?) = setImpl(annotationProperty, value)

@JvmName("setAnnotation")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Annotation>, value: AnnotationNode?) = setImpl(annotationProperty, value)

@JvmName("setAnnotationAsAnnotation")
operator fun <T : Annotation> AnnotationNode.set(annotationProperty: KProperty1<out Annotation, T>, value: T?) = setImpl(annotationProperty, value)

@JvmName("setByteArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<Byte>>, value: List<Byte>?) = setImpl(annotationProperty, value)

@JvmName("setBooleanArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<Boolean>>, value: List<Boolean>?) = setImpl(annotationProperty, value)

@JvmName("setCharArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<Char>>, value: List<Char>?) = setImpl(annotationProperty, value)

@JvmName("setShortArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<Short>>, value: List<Short>?) = setImpl(annotationProperty, value)

@JvmName("setIntArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<Int>>, value: List<Int>?) = setImpl(annotationProperty, value)

@JvmName("setLongArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<Long>>, value: List<Long>?) = setImpl(annotationProperty, value)

@JvmName("setFloatArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<Float>>, value: List<Float>?) = setImpl(annotationProperty, value)

@JvmName("setDoubleArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<Double>>, value: List<Double>?) = setImpl(annotationProperty, value)

@JvmName("setStringArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<String>>, value: List<String>?) = setImpl(annotationProperty, value)

@JvmName("setTypeArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<KClass<*>>>, value: List<Type>?) = setImpl(annotationProperty, value)

@JvmName("setTypeAsClassArray")
operator fun <T : Any> AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<KClass<T>>>, value: List<Class<out T>>?) = setImpl(annotationProperty, value)

@JvmName("setTypeAsKClassArray")
operator fun <T : Any> AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<KClass<T>>>, value: List<KClass<out T>>?) = setImpl(annotationProperty, value?.map(KClass<*>::java))

@JvmName("setEnumArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<Enum<*>>>, value: List<Array<String>>?) = setImpl(annotationProperty, value)

@JvmName("setEnumAsEnumArray")
operator fun <T : Enum<T>> AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<Enum<T>>>, value: List<Enum<T>>?) = setImpl(annotationProperty, value)

@JvmName("setAnnotationArray")
operator fun AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<out Annotation>>, value: List<AnnotationNode>?) = setImpl(annotationProperty, value)

@JvmName("setAnnotationAsAnnotationArray")
operator fun <T : Annotation> AnnotationNode.set(annotationProperty: KProperty1<out Annotation, Array<out T>>, value: List<T>?) = setImpl(annotationProperty, value)
