package name.remal

import java.util.concurrent.atomic.AtomicLong

operator fun AtomicLong.inc() = apply { incrementAndGet() }
operator fun AtomicLong.dec() = apply { decrementAndGet() }

operator fun AtomicLong.plusAssign(value: Int) = plusAssign(value.toLong())
operator fun AtomicLong.plusAssign(value: AtomicLong) = plusAssign(value.get())
operator fun AtomicLong.plusAssign(value: Long) {
    addAndGet(value)
}

operator fun AtomicLong.minusAssign(value: Int) = minusAssign(value.toLong())
operator fun AtomicLong.minusAssign(value: AtomicLong) = minusAssign(value.get())
operator fun AtomicLong.minusAssign(value: Long) {
    addAndGet(-1 * value)
}


fun AtomicLong.incrementAndGetUnsigned(): Long {
    while (true) {
        val result = incrementAndGet()
        if (0 <= result) return result
        if (compareAndSet(result, 0)) {
            return 0
        }
    }
}

fun AtomicLong.decrementAndGetUnsigned(): Long {
    while (true) {
        val result = decrementAndGet()
        if (0 <= result) return result
        if (compareAndSet(result, Long.MAX_VALUE)) {
            return Long.MAX_VALUE
        }
    }
}
