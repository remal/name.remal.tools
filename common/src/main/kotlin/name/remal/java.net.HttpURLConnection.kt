package name.remal

import java.net.HttpURLConnection
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets.UTF_8

val HttpURLConnection.contentCharset: Charset
    get() {
        contentType
            ?.toUpperCase()
            ?.substringAfter("CHARSET=", "")
            ?.substringBefore(';')
            ?.trim()
            ?.let {
                try {
                    return Charset.forName(it)
                } catch (e: IllegalArgumentException) {
                    // do nothing
                }
            }

        return UTF_8
    }
