package name.remal

import com.google.common.util.concurrent.MoreExecutors
import com.google.common.util.concurrent.Service
import java.time.Duration

fun Service.addListener(listener: Service.Listener) = addListener(listener, MoreExecutors.directExecutor())
