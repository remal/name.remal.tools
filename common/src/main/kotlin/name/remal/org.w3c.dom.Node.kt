package name.remal

import org.w3c.dom.DOMException
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.Text
import org.w3c.dom.bootstrap.DOMImplementationRegistry
import org.w3c.dom.ls.DOMImplementationLS

operator fun Node.plusAssign(node: Node) {
    this.appendChild(node)
}

operator fun Node.plusAssign(text: String) {
    this.appendTextNode(text)
}


private val domImpl: DOMImplementationLS = DOMImplementationRegistry.newInstance().getDOMImplementation("LS").uncheckedCast()
fun Node.asString(doIndent: Boolean = false): String {
    val writer = domImpl.createLSSerializer()
    if (doIndent) {
        try {
            writer.domConfig.setParameter("format-pretty-print", java.lang.Boolean.TRUE)
        } catch (ignored: DOMException) {
            // do nothing
        }
    }
    return writer.writeToString(this)
}

fun <T : Node> T.remove(): T = this.apply {
    this.parentNode.removeChild(this)
}

inline fun Node.getChildNodes(predicate: (node: Node) -> Boolean): List<Node> {
    val result = mutableListOf<Node>()
    this.childNodes.forEach {
        if (predicate(it)) {
            result.add(it)
        }
    }
    return result
}

fun Node.getChildElements(tagName: String): List<Element> {
    val result = mutableListOf<Element>()
    this.childNodes.forEach {
        if (it is Element) {
            if (it.tagName == tagName || it.localName == tagName) {
                result.add(it)
            }
        }
    }
    return result
}

fun Node.appendElement(tagName: String, attrs: Map<String, Any> = mapOf()): Element {
    val ownerDocument: Document = this as? Document ?: this.ownerDocument
    val element = ownerDocument.createElement(tagName)
    attrs.forEach { name, value -> element.setAttribute(name, value.toString()) }
    this.appendChild(element)
    return element
}

fun Node.appendTextNode(data: String): Text {
    val ownerDocument: Document = this as? Document ?: this.ownerDocument
    val textNode = ownerDocument.createTextNode(data)
    this.appendChild(textNode)
    return textNode
}

fun Node.findElement(tagName: String, attrs: Map<String, Any> = mapOf()): Element? {
    return this.getChildElements(tagName).firstOrNull { attrs.all { (name, value) -> it.getAttribute(name) == value.toString() } }
}

fun Node.findOrAppendElement(tagName: String, attrs: Map<String, Any> = mapOf(), onCreate: (element: Element) -> Unit = {}): Element {
    return this.findElement(tagName, attrs) ?: this.appendElement(tagName, attrs).also(onCreate)
}

private fun stripSpacesImpl(node: Node) {
    var child: Node? = node.firstChild
    while (child != null) {
        val nextChild = child.nextSibling
        if (child is Text) {
            if (child.wholeText?.trim().isNullOrEmpty()) {
                node.removeChild(child)
            }
        }
        child = nextChild
    }
}

fun <T : Node> T.stripSpaces() = this.apply {
    normalize()
    stripSpacesImpl(this)
}


fun Node.evaluateXPathAsBoolean(xpathExpression: String) = newXPathExpression(xpathExpression).evaluateAsBoolean(this)
fun Node.evaluateXPathAsNumber(xpathExpression: String) = newXPathExpression(xpathExpression).evaluateAsNumber(this)
fun Node.evaluateXPathAsString(xpathExpression: String) = newXPathExpression(xpathExpression).evaluateAsString(this)
fun Node.evaluateXPathAsNodeList(xpathExpression: String) = newXPathExpression(xpathExpression).evaluateAsNodeList(this)
fun Node.evaluateXPathAsNode(xpathExpression: String) = newXPathExpression(xpathExpression).evaluateAsNode(this)
