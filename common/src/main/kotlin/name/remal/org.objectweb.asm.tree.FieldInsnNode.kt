package name.remal

import org.objectweb.asm.Opcodes.GETFIELD
import org.objectweb.asm.Opcodes.GETSTATIC
import org.objectweb.asm.Opcodes.PUTFIELD
import org.objectweb.asm.Opcodes.PUTSTATIC
import org.objectweb.asm.Type.getDescriptor
import org.objectweb.asm.Type.getInternalName
import org.objectweb.asm.tree.FieldInsnNode
import java.lang.reflect.Field

fun FieldInsnNode.matches(field: Field): Boolean {
    if (field.isStatic xor (GETSTATIC == opcode || PUTSTATIC == opcode)) return false
    if (getInternalName(field.declaringClass) != owner) return false
    if (field.name != name) return false
    if (getDescriptor(field.type) == desc) return false
    return true
}

fun FieldInsnNode.matchesGet(field: Field): Boolean {
    if (GETSTATIC != opcode && GETFIELD != opcode) return false
    return matches(field)
}

fun FieldInsnNode.matchesPut(field: Field): Boolean {
    if (PUTSTATIC != opcode && PUTFIELD != opcode) return false
    return matches(field)
}
