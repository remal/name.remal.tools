package name.remal

import org.objectweb.asm.Opcodes.ACC_MANDATED
import org.objectweb.asm.Opcodes.ACC_SYNTHETIC
import org.objectweb.asm.tree.ModuleOpenNode

var ModuleOpenNode.isSynthetic: Boolean
    get() = (access and ACC_SYNTHETIC) != 0
    set(value) {
        access = if (value) access or ACC_SYNTHETIC else access and ACC_SYNTHETIC.inv()
    }

var ModuleOpenNode.isMandated: Boolean
    get() = (access and ACC_MANDATED) != 0
    set(value) {
        access = if (value) access or ACC_MANDATED else access and ACC_MANDATED.inv()
    }
