package name.remal

fun <T : Any> T?.default(value: T) = this ?: value

fun Boolean?.default(value: Boolean = false) = this ?: value
fun Byte?.default(value: Byte = 0) = this ?: value
fun Short?.default(value: Short = 0) = this ?: value
fun Int?.default(value: Int = 0) = this ?: value
fun Long?.default(value: Long = 0L) = this ?: value
fun Float?.default(value: Float = 0.0F) = this ?: value
fun Double?.default(value: Double = 0.0) = this ?: value
fun Char?.default(value: Char = '\u0000') = this ?: value

private val defaultBooleanArray = booleanArrayOf()
fun BooleanArray?.default(value: BooleanArray = defaultBooleanArray) = this ?: value
private val defaultByteArray = byteArrayOf()
fun ByteArray?.default(value: ByteArray = defaultByteArray) = this ?: value
private val defaultShortArray = shortArrayOf()
fun ShortArray?.default(value: ShortArray = defaultShortArray) = this ?: value
private val defaultIntArray = intArrayOf()
fun IntArray?.default(value: IntArray = defaultIntArray) = this ?: value
private val defaultLongArray = longArrayOf()
fun LongArray?.default(value: LongArray = defaultLongArray) = this ?: value
private val defaultFloatArray = floatArrayOf()
fun FloatArray?.default(value: FloatArray = defaultFloatArray) = this ?: value
private val defaultDoubleArray = doubleArrayOf()
fun DoubleArray?.default(value: DoubleArray = defaultDoubleArray) = this ?: value
private val defaultCharArray = charArrayOf()
fun CharArray?.default(value: CharArray = defaultCharArray) = this ?: value

fun CharSequence?.default(value: CharSequence = "") = this ?: value
fun String?.default(value: String = "") = this ?: value

fun <T> Iterable<T>?.default(value: Iterable<T> = listOf()) = this ?: value
fun <T> Collection<T>?.default(value: Collection<T> = listOf()) = this ?: value
fun <T> List<T>?.default(value: List<T> = listOf()) = this ?: value
fun <T> Set<T>?.default(value: Set<T> = setOf()) = this ?: value

fun <K, V> Map<K, V>?.default(value: Map<K, V> = mapOf()) = this ?: value
