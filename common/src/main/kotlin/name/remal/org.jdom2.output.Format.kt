package name.remal

import org.jdom2.output.Format
import org.jdom2.output.LineSeparator.DOS
import org.jdom2.output.LineSeparator.UNIX
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets.UTF_8
import java.nio.charset.UnsupportedCharsetException

var Format.charset: Charset
    get() {
        try {
            encoding?.also {
                return Charset.forName(it)
            }
        } catch (e: UnsupportedCharsetException) {
            // do nothing
        }
        return UTF_8
    }
    set(value) {
        encoding = value.name()
    }


fun Format.setOsLineSeparator() = apply { setLineSeparator(if (OS.isWindows()) DOS else UNIX) }

