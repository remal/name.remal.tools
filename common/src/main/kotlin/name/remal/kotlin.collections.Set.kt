package name.remal

import java.util.Collections.synchronizedSet

fun <T> Set<T>.asSynchronized(): Set<T> = synchronizedSet<T>(this)
