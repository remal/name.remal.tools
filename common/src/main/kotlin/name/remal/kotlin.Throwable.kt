package name.remal

fun <T : Throwable, R : T> T.findAll(type: Class<R>): List<R> = ThrowableUtils.findAll(this, type)
fun <T : Throwable, R : T> T.findFirst(type: Class<R>): R? = ThrowableUtils.findFirst(this, type)
fun <T : Throwable, R : T> T.findLast(type: Class<R>): R? = ThrowableUtils.findLast(this, type)
fun Throwable.contains(type: Class<out Throwable>) = ThrowableUtils.contains(this, type)
