package name.remal

import org.objectweb.asm.Opcodes.ACC_FINAL
import org.objectweb.asm.Opcodes.ACC_MANDATED
import org.objectweb.asm.Opcodes.ACC_SYNTHETIC
import org.objectweb.asm.tree.ParameterNode

var ParameterNode.isFinal: Boolean
    get() = (access and ACC_FINAL) != 0
    set(value) {
        access = if (value) access or ACC_FINAL else access and ACC_FINAL.inv()
    }

var ParameterNode.isSynthetic: Boolean
    get() = (access and ACC_SYNTHETIC) != 0
    set(value) {
        access = if (value) access or ACC_SYNTHETIC else access and ACC_SYNTHETIC.inv()
    }

var ParameterNode.isMandated: Boolean
    get() = (access and ACC_MANDATED) != 0
    set(value) {
        access = if (value) access or ACC_MANDATED else access and ACC_MANDATED.inv()
    }
