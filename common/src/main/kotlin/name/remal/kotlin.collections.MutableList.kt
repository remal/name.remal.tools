package name.remal

import name.remal.collections.ObservableCollections
import name.remal.collections.ObservableList
import java.util.Collections.synchronizedList

fun <T> MutableList<T>.asSynchronized(): MutableList<T> = synchronizedList<T>(this)

fun <T> MutableList<T>.asObservable(): ObservableList<T> = ObservableCollections.observableList(this)
