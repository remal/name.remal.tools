package name.remal

import java.util.SortedMap

fun <V, K> Sequence<V>.toMutableMap(keyExtractor: (value: V) -> K): MutableMap<K, V> = mutableMapOf<K, V>().also {
    for (value in this) {
        val key = keyExtractor(value)
        it[key] = value
    }
}

fun <V, K> Sequence<V>.toMap(keyExtractor: (value: V) -> K): Map<K, V> = toMutableMap(keyExtractor).toMap()

fun <V, K> Sequence<V>.toHashMap(keyExtractor: (value: V) -> K): MutableMap<K, V> = hashMapOf<K, V>().also {
    for (value in this) {
        val key = keyExtractor(value)
        it[key] = value
    }
}

fun <V, K : Comparable<K>> Sequence<V>.toSortedMap(keyExtractor: (value: V) -> K): SortedMap<K, V> = sortedMapOf<K, V>().also {
    for (value in this) {
        val key = keyExtractor(value)
        it[key] = value
    }
}


fun <T> Sequence<T>.toStream() = iterator().asStream()
