package name.remal

import java.time.ZonedDateTime
import java.util.Date

fun ZonedDateTime.toDate(): Date = Date.from(toInstant())
