package name.remal

import kotlin.reflect.KMutableProperty
import kotlin.reflect.KProperty
import kotlin.reflect.jvm.javaField
import kotlin.reflect.jvm.javaGetter
import kotlin.reflect.jvm.javaSetter

fun <T : Annotation> KProperty<*>.getMetaAnnotations(type: Class<T>): List<T> {
    val result = mutableSetOf<T>()
    javaField?.getMetaAnnotations(type)?.forEach { result.add(it) }
    javaGetter?.getMetaAnnotations(type)?.forEach { result.add(it) }
    if (this is KMutableProperty<*>) javaSetter?.getMetaAnnotations(type)?.forEach { result.add(it) }
    return result.toList()
}

fun <T : Annotation> KProperty<*>.getMetaAnnotation(type: Class<T>) = getMetaAnnotations(type).firstOrNull()
fun <T : Annotation> KProperty<*>.hasMetaAnnotation(type: Class<T>) = null != getMetaAnnotation(type)
