package name.remal

import java.util.Collections.synchronizedNavigableMap
import java.util.NavigableMap

fun <K : Comparable<K>, V> NavigableMap<K, V>.asSynchronized(): NavigableMap<K, V> = synchronizedNavigableMap<K, V>(this)
