package name.remal

import org.jdom2.input.SAXBuilder
import org.jdom2.input.sax.XMLReaders
import org.xml.sax.InputSource
import java.io.StringReader

fun SAXBuilder.setNoValidatingXMLReaderFactory() {
    xmlReaderFactory = XMLReaders.NONVALIDATING
}

fun SAXBuilder.setNoOpEntityResolver() {
    setEntityResolver { _, _ -> InputSource(StringReader("")) }
}
