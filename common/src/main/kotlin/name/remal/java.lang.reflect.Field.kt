package name.remal

import java.lang.reflect.Field
import java.lang.reflect.Modifier

val Field.isStatic get() = Modifier.isStatic(modifiers)
val Field.isFinal get() = Modifier.isFinal(modifiers)
