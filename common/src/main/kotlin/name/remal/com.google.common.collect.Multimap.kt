@file:ExcludeFromCodeCoverage

package name.remal

import com.google.common.collect.Multimap
import name.remal.gradle_plugins.api.ExcludeFromCodeCoverage

val Multimap<*, *>.isNotEmpty get() = !isEmpty

operator fun <K, V> Multimap<K, V>.contains(key: K) = containsKey(key)

operator fun <K, V> Multimap<K, V>.set(key: K, value: V) = put(key, value)
