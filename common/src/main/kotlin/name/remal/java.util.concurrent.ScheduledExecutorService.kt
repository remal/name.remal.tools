package name.remal

import java.time.Duration
import java.time.Duration.ZERO
import java.util.concurrent.Callable
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.TimeUnit.NANOSECONDS
import java.util.concurrent.TimeUnit.SECONDS

fun ScheduledExecutorService.schedule(command: Runnable, delay: Duration) = delay.callAwait(command, this::schedule)!!


fun <V> ScheduledExecutorService.schedule(callable: Callable<V>, delay: Duration) = delay.callAwait(callable, this::schedule)!!

@JvmName("scheduleCallable")
fun <V> ScheduledExecutorService.schedule(delay: Duration, callable: () -> V) = delay.callAwait(callable, this::schedule)!!


fun ScheduledExecutorService.scheduleAtFixedRate(command: Runnable, initialDelay: Duration, period: Duration) = when {
    ZERO == initialDelay || 30 * 24 * 3600 <= initialDelay.seconds || ZERO == period || 30 * 24 * 3600 <= period.seconds -> scheduleAtFixedRate(command, initialDelay.seconds, period.seconds, SECONDS)!!
    24 * 3600 <= initialDelay.seconds || 24 * 3600 <= period.seconds -> scheduleAtFixedRate(command, initialDelay.toMillis(), period.toMillis(), MILLISECONDS)!!
    else -> scheduleAtFixedRate(command, initialDelay.toNanos(), period.toNanos(), NANOSECONDS)!!
}

fun ScheduledExecutorService.scheduleAtFixedRate(command: Runnable, period: Duration) = scheduleAtFixedRate(command, period, period)

fun ScheduledExecutorService.scheduleAtFixedRate(initialDelay: Duration, period: Duration, command: () -> Unit) = when {
    ZERO == initialDelay || 30 * 24 * 3600 <= initialDelay.seconds || ZERO == period || 30 * 24 * 3600 <= period.seconds -> scheduleAtFixedRate(command, initialDelay.seconds, period.seconds, SECONDS)!!
    24 * 3600 <= initialDelay.seconds || 24 * 3600 <= period.seconds -> scheduleAtFixedRate(command, initialDelay.toMillis(), period.toMillis(), MILLISECONDS)!!
    else -> scheduleAtFixedRate(command, initialDelay.toNanos(), period.toNanos(), NANOSECONDS)!!
}

fun ScheduledExecutorService.scheduleAtFixedRate(period: Duration, command: () -> Unit) = scheduleAtFixedRate(period, period, command)


fun ScheduledExecutorService.scheduleWithFixedDelay(command: Runnable, initialDelay: Duration, delay: Duration) = when {
    ZERO == initialDelay || 30 * 24 * 3600 <= initialDelay.seconds || ZERO == delay || 30 * 24 * 3600 <= delay.seconds -> scheduleWithFixedDelay(command, initialDelay.seconds, delay.seconds, SECONDS)!!
    24 * 3600 <= initialDelay.seconds || 24 * 3600 <= delay.seconds -> scheduleWithFixedDelay(command, initialDelay.toMillis(), delay.toMillis(), MILLISECONDS)!!
    else -> scheduleWithFixedDelay(command, initialDelay.toNanos(), delay.toNanos(), NANOSECONDS)!!
}

fun ScheduledExecutorService.scheduleWithFixedDelay(command: Runnable, delay: Duration) = scheduleWithFixedDelay(command, delay, delay)

fun ScheduledExecutorService.scheduleWithFixedDelay(initialDelay: Duration, delay: Duration, command: () -> Unit) = when {
    ZERO == initialDelay || 30 * 24 * 3600 <= initialDelay.seconds || ZERO == delay || 30 * 24 * 3600 <= delay.seconds -> scheduleWithFixedDelay(command, initialDelay.seconds, delay.seconds, SECONDS)!!
    24 * 3600 <= initialDelay.seconds || 24 * 3600 <= delay.seconds -> scheduleWithFixedDelay(command, initialDelay.toMillis(), delay.toMillis(), MILLISECONDS)!!
    else -> scheduleWithFixedDelay(command, initialDelay.toNanos(), delay.toNanos(), NANOSECONDS)!!
}

fun ScheduledExecutorService.scheduleWithFixedDelay(delay: Duration, command: () -> Unit) = scheduleWithFixedDelay(delay, delay, command)
