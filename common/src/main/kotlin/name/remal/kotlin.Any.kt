package name.remal

fun <T> T?.nullIf(predicate: T.() -> Boolean): T? = if (this == null || predicate(this)) null else this
