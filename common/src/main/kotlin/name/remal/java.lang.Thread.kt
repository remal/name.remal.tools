package name.remal

import java.lang.Thread.currentThread

inline fun <R> forThreadContextClassLoader(contextClassLoader: ClassLoader?, action: () -> R): R {
    val currentThread = currentThread()
    val prevContextClassLoader = currentThread.contextClassLoader
    currentThread.contextClassLoader = contextClassLoader
    try {
        return action()

    } finally {
        currentThread.contextClassLoader = prevContextClassLoader
    }
}
