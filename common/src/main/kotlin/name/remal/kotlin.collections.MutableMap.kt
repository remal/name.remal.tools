package name.remal

import name.remal.collections.ObservableCollections
import name.remal.collections.ObservableMap
import java.util.Collections.synchronizedMap

fun <K, V> MutableMap<K, V>.asSynchronized(): MutableMap<K, V> = synchronizedMap<K, V>(this)

fun <K, V> MutableMap<K, V>.removeAll(keys: Iterable<K>) = keys.forEach { remove(it) }

fun <K, V> MutableMap<K, V>.asObservable(): ObservableMap<K, V> = ObservableCollections.observableMap(this)
