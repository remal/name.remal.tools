package name.remal

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache

fun <K, V> CacheBuilder<K, V>.build(loader: (key: K) -> V): LoadingCache<K, V> {
    return build(object : CacheLoader<K, V>() {
        override fun load(key: K) = loader(key)
    })
}
