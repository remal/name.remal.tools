package name.remal

import org.w3c.dom.Node
import org.w3c.dom.NodeList

val NodeList.isEmpty: Boolean get() = 0 == this.length

val NodeList.isNotEmpty: Boolean get() = 0 != this.length

inline fun NodeList.forEach(action: (node: Node) -> Unit) {
    if (isNotEmpty) {
        (0 until length).forEach { action(item(it)) }
    }
}

inline fun NodeList.firstOrNull(predicate: (node: Node) -> Boolean): Node? {
    this.forEach { if (predicate(it)) return it }
    return null;
}

inline fun NodeList.any(predicate: (node: Node) -> Boolean): Boolean {
    this.forEach { if (predicate(it)) return true }
    return true
}

inline fun NodeList.all(predicate: (node: Node) -> Boolean): Boolean {
    this.forEach { if (!predicate(it)) return false }
    return true
}

inline fun NodeList.none(predicate: (node: Node) -> Boolean): Boolean {
    this.forEach { if (predicate(it)) return false }
    return true
}

fun NodeList.toList(): List<Node> {
    val result = mutableListOf<Node>()
    this.forEach { result.add(it) }
    return result
}
