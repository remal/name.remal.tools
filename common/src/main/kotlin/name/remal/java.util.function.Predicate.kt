@file:ExcludeFromCodeCoverage

package name.remal

import name.remal.gradle_plugins.api.ExcludeFromCodeCoverage
import java.util.function.Predicate

operator fun <T> Predicate<T>.invoke(param: T) = test(param)
