package name.remal

import org.jdom2.Element
import org.jdom2.Namespace

fun Element.setAttribute(name: String, value: Any) = apply { setAttribute(name, value.toString()) }
fun Element.setAttribute(name: String, value: Any, ns: Namespace?) = apply { setAttribute(name, value.toString(), ns) }

fun Element.setAttributes(attrs: Map<String, Any>) = apply {
    attrs.forEach { name, value -> setAttribute(name, value) }
}

fun Element.setAttributes(attrs: Map<String, Any>, ns: Namespace?) = apply {
    attrs.forEach { name, value -> setAttribute(name, value, ns) }
}

fun Element.getOrCreateChild(name: String, attrs: Map<String, Any> = mapOf()): Element {
    if (attrs.isEmpty()) {
        getChild(name)?.let { return it }

    } else {
        getChildren(name)
            .firstOrNull { child -> attrs.entries.all { it.value.toString() == child.getAttributeValue(it.key) } }
            ?.let { return it }
    }

    Element(name).setAttributes(attrs).let { child ->
        addContent(child)
        return child
    }
}

fun Element.getOrCreateChild(name: String, ns: Namespace?, attrs: Map<String, Any> = mapOf()): Element {
    if (attrs.isEmpty()) {
        getChild(name, ns)?.let { return it }

    } else {
        getChildren(name, ns)
            .firstOrNull { child -> attrs.entries.all { it.value.toString() == child.getAttributeValue(it.key, ns) } }
            ?.let { return it }
    }

    Element(name, ns).setAttributes(attrs, ns).let { child ->
        addContent(child)
        return child
    }
}
