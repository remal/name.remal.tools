package name.remal.json.data_format;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT;
import static name.remal.json.data_format.DataFormatJSON.JSON_DATA_FORMAT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.time.ZoneId;
import name.remal.json.api.DataFormat;
import org.junit.Test;

public class DataFormatJSONTest {

    @Test
    public void test() {
        DataFormat dataFormat = JSON_DATA_FORMAT;
        assertFalse(dataFormat.getObjectMapper().isEnabled(INDENT_OUTPUT));
        assertFalse(dataFormat.getObjectMapper().isEnabled(FAIL_ON_UNKNOWN_PROPERTIES));

        dataFormat = dataFormat.withIndention();
        assertTrue(dataFormat.getObjectMapper().isEnabled(INDENT_OUTPUT));
        assertFalse(dataFormat.getObjectMapper().isEnabled(FAIL_ON_UNKNOWN_PROPERTIES));
        assertSame(dataFormat, dataFormat.withIndention());

        dataFormat = dataFormat.withFailingOnUnknownProperties();
        assertTrue(dataFormat.getObjectMapper().isEnabled(INDENT_OUTPUT));
        assertTrue(dataFormat.getObjectMapper().isEnabled(FAIL_ON_UNKNOWN_PROPERTIES));
        assertSame(dataFormat, dataFormat.withFailingOnUnknownProperties());

        dataFormat = dataFormat.withoutIndention();
        assertFalse(dataFormat.getObjectMapper().isEnabled(INDENT_OUTPUT));
        assertTrue(dataFormat.getObjectMapper().isEnabled(FAIL_ON_UNKNOWN_PROPERTIES));
        assertSame(dataFormat, dataFormat.withoutIndention());

        dataFormat = dataFormat.withoutFailingOnUnknownProperties();
        assertFalse(dataFormat.getObjectMapper().isEnabled(INDENT_OUTPUT));
        assertFalse(dataFormat.getObjectMapper().isEnabled(FAIL_ON_UNKNOWN_PROPERTIES));
        assertSame(dataFormat, dataFormat.withoutFailingOnUnknownProperties());

        dataFormat = dataFormat.withIndention().withFailingOnUnknownProperties().withTimeZone(ZoneId.of("UTC"));
        assertTrue(dataFormat.getObjectMapper().isEnabled(INDENT_OUTPUT));
        assertTrue(dataFormat.getObjectMapper().isEnabled(FAIL_ON_UNKNOWN_PROPERTIES));
        assertEquals("UTC", dataFormat.getObjectMapper().getSerializationConfig().getTimeZone().getID());
        assertEquals("UTC", dataFormat.getObjectMapper().getDeserializationConfig().getTimeZone().getID());

        assertEquals(JSON_DATA_FORMAT.getMimeType(), dataFormat.getMimeType());
        assertEquals(JSON_DATA_FORMAT.getSupportedMimeTypes(), dataFormat.getSupportedMimeTypes());
        assertEquals(JSON_DATA_FORMAT.getFileExtension(), dataFormat.getFileExtension());
        assertEquals(JSON_DATA_FORMAT.getSupportedFileExtensions(), dataFormat.getSupportedFileExtensions());
    }

}
