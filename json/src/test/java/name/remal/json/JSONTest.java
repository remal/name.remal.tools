package name.remal.json;

import static java.lang.String.format;
import static java.time.temporal.ChronoUnit.CENTURIES;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.DECADES;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MICROS;
import static java.time.temporal.ChronoUnit.MILLENNIA;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.MONTHS;
import static java.time.temporal.ChronoUnit.NANOS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.time.temporal.ChronoUnit.WEEKS;
import static java.time.temporal.ChronoUnit.YEARS;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ValueNode;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import org.junit.Test;

public class JSONTest {

    @Test
    public void nullIsDeserializedAsNullNode() {
        assertEquals(NullNode.instance, JSON.readJson(TreeNode.class, "null"));
        assertEquals(NullNode.instance, JSON.readJson(JsonNode.class, "null"));
        assertEquals(NullNode.instance, JSON.readJson(ValueNode.class, "null"));
    }

    @Test
    public void dateIsSerializedInISO8601Format() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        Date date = new Date();
        assertEquals('"' + format.format(date) + '"', JSON.writeJsonAsString(date));
    }

    @Test
    public void isoLocalDateCanBeDeserialized() {
        int year = 2018;
        int month = 3;
        int day = 7;
        String json = format("\"%4d-%02d-%02d\"", year, month, day);
        assertEquals(LocalDate.of(year, month, day), JSON.readJson(LocalDate.class, json));
    }


    @Test
    public void durationOfDifferentFormatsCanBeDeserialized() {
        assertEquals(Duration.ofSeconds(5), JSON.readJson(Duration.class, "\"5s\""));
        assertEquals(Duration.ofSeconds(5), JSON.readJson(Duration.class, "\"5 sEc\""));
        assertEquals(Duration.ofSeconds(5), JSON.readJson(Duration.class, "\"5seCs\""));
        assertEquals(Duration.ofSeconds(5), JSON.readJson(Duration.class, "\"5 SECOND\""));
        assertEquals(Duration.ofSeconds(5), JSON.readJson(Duration.class, "\"5seconds\""));

        assertEquals(Duration.ofSeconds(0, 500 * 1000 * 1000), JSON.readJson(Duration.class, "\"0.5S\""));
        assertEquals(Duration.ofSeconds(-0, -500 * 1000 * 1000), JSON.readJson(Duration.class, "\"-0.5s\""));

        for (String suffix : asList("ns", "nano", "nanos", "nanosec", "nanosecs", "nanosecond", "nanoseconds")) {
            assertEquals(Duration.ofSeconds(0, 2 * NANOS.getDuration().getNano()), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("us", "micro", "micros", "microsec", "microsecs", "microsecond", "microseconds")) {
            assertEquals(Duration.ofSeconds(0, 2 * MICROS.getDuration().getNano()), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("ms", "milli", "millis", "millisec", "millisecs", "millisecond", "milliseconds")) {
            assertEquals(Duration.ofSeconds(0, 2 * MILLIS.getDuration().getNano()), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("s", "sec", "secs", "second", "seconds")) {
            assertEquals(Duration.ofSeconds(2 * SECONDS.getDuration().getSeconds(), 0), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("m", "min", "mins", "minute", "minutes")) {
            assertEquals(Duration.ofSeconds(2 * MINUTES.getDuration().getSeconds(), 0), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("h", "hour", "hours")) {
            assertEquals(Duration.ofSeconds(2 * HOURS.getDuration().getSeconds(), 0), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("d", "day", "days")) {
            assertEquals(Duration.ofSeconds(2 * DAYS.getDuration().getSeconds(), 0), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("w", "week", "weeks")) {
            assertEquals(Duration.ofSeconds(2 * WEEKS.getDuration().getSeconds(), 0), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("month", "months")) {
            assertEquals(Duration.ofSeconds(2 * MONTHS.getDuration().getSeconds(), 0), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("y", "year", "years")) {
            assertEquals(Duration.ofSeconds(2 * YEARS.getDuration().getSeconds(), 0), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("decade", "decades")) {
            assertEquals(Duration.ofSeconds(2 * DECADES.getDuration().getSeconds(), 0), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("century", "centuries")) {
            assertEquals(Duration.ofSeconds(2 * CENTURIES.getDuration().getSeconds(), 0), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("millennia", "millennias")) {
            assertEquals(Duration.ofSeconds(2 * MILLENNIA.getDuration().getSeconds(), 0), JSON.readJson(Duration.class, "\"2 " + suffix + '"'));
        }
    }

    @Test
    public void periodOfDifferentFormatsCanBeDeserialized() {
        assertEquals(Period.of(0, 0, 5), JSON.readJson(Period.class, "\"5d\""));
        assertEquals(Period.of(0, 0, 5), JSON.readJson(Period.class, "\"5 dAy\""));
        assertEquals(Period.of(0, 0, 5), JSON.readJson(Period.class, "\"5daYs\""));

        assertEquals(Period.of(0, 2, 15), JSON.readJson(Period.class, "\"2.5MONTH\""));
        assertEquals(Period.of(0, -2, -15), JSON.readJson(Period.class, "\"-2.5months\""));

        for (String suffix : asList("ns", "nano", "nanos", "nanosec", "nanosecs", "nanosecond", "nanoseconds")) {
            assertSame(Period.ZERO, JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("us", "micro", "micros", "microsec", "microsecs", "microsecond", "microseconds")) {
            assertSame(Period.ZERO, JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("ms", "milli", "millis", "millisec", "millisecs", "millisecond", "milliseconds")) {
            assertSame(Period.ZERO, JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("s", "sec", "secs", "second", "seconds")) {
            assertSame(Period.ZERO, JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("m", "min", "mins", "minute", "minutes")) {
            assertSame(Period.ZERO, JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("h", "hour", "hours")) {
            assertSame(Period.ZERO, JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("d", "day", "days")) {
            assertEquals(Period.of(0, 0, 2), JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("w", "week", "weeks")) {
            assertEquals(Period.of(0, 0, 14), JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("month", "months")) {
            assertEquals(Period.of(0, 2, 0), JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("y", "year", "years")) {
            assertEquals(Period.of(2, 0, 0), JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("decade", "decades")) {
            assertEquals(Period.of(20, 0, 0), JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("century", "centuries")) {
            assertEquals(Period.of(200, 0, 0), JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
        for (String suffix : asList("millennia", "millennias")) {
            assertEquals(Period.of(2000, 0, 0), JSON.readJson(Period.class, "\"2 " + suffix + '"'));
        }
    }

}
