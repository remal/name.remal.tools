package name.remal.json;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_TIME;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_TIME;
import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;
import static java.time.format.DateTimeFormatter.RFC_1123_DATE_TIME;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class JSONZonedDateTimeTest {

    private static final LocalDateTime LOCAL_DATE_TIME = LocalDateTime.now().withNano(0);

    @Parameters
    @NotNull
    public static List<ZoneId> zoneIds() {
        DateTimeFormatter groupingFormatter = DateTimeFormatter.ofPattern("z");
        List<ZoneId> zoneIds = ZoneId.getAvailableZoneIds().stream()
            .filter(id -> !id.isEmpty())
            .sorted()
            .map(ZoneId::of)
            .map(ZoneId::normalized)
            .filter(zoneId -> !(zoneId instanceof ZoneOffset))
            .collect(groupingBy(zoneId -> groupingFormatter.format(ZonedDateTime.of(LOCAL_DATE_TIME, zoneId))))
            .entrySet().stream()
            .filter(entry -> entry.getValue().size() == 1)
            .map(entry -> entry.getValue().get(0))
            .collect(groupingBy(zoneId -> zoneId.getRules().getOffset(LOCAL_DATE_TIME).getTotalSeconds()))
            .entrySet().stream()
            .filter(entry -> entry.getKey() != 0) // 0 offset fails ISO_OFFSET_DATE_TIME -> LocalDateTime conversion
            .filter(entry -> entry.getValue().size() == 1)
            .map(entry -> entry.getValue().get(0))
            .sorted(comparing(ZoneId::getId))
            .collect(toList());
        assertTrue(zoneIds.size() >= 1);
        return zoneIds;
    }

    @Parameter
    public ZoneId zoneId;

    public ZoneOffset zoneOffset;

    public LocalDateTime localDateTimeZoned;
    public LocalDate localDateZoned;
    public LocalTime localTimeZoned;

    public ZonedDateTime zonedDateTime;
    public OffsetDateTime offsetDateTime;
    public OffsetTime offsetTime;

    @Before
    public void before() {
        zoneOffset = zoneId.getRules().getOffset(LOCAL_DATE_TIME);

        localDateTimeZoned = LocalDateTime.ofInstant(LOCAL_DATE_TIME.toInstant(zoneOffset), ZoneId.systemDefault().getRules().getOffset(LOCAL_DATE_TIME));
        localDateZoned = localDateTimeZoned.toLocalDate();
        localTimeZoned = localDateTimeZoned.toLocalTime();

        zonedDateTime = ZonedDateTime.of(LOCAL_DATE_TIME, zoneId);
        offsetDateTime = OffsetDateTime.of(LOCAL_DATE_TIME, zoneOffset);
        offsetTime = OffsetTime.of(LOCAL_DATE_TIME.toLocalTime(), zoneOffset);
    }


    @Test
    public void localDateTimeOfDifferentFormatsCanBeDeserialized() {
        assertEquals(localDateTimeZoned, JSON.readJson(LocalDateTime.class, '"' + RFC_1123_DATE_TIME.format(zonedDateTime) + '"'));
        assertEquals(localDateTimeZoned, JSON.readJson(LocalDateTime.class, '"' + RFC_1123_DATE_TIME.format(offsetDateTime) + '"'));
        assertEquals(localDateTimeZoned, JSON.readJson(LocalDateTime.class, '"' + ISO_ZONED_DATE_TIME.format(zonedDateTime) + '"'));
        assertEquals(localDateTimeZoned, JSON.readJson(LocalDateTime.class, '"' + ISO_OFFSET_DATE_TIME.format(offsetDateTime) + '"'));
        assertEquals(localDateTimeZoned, JSON.readJson(LocalDateTime.class, '"' + ISO_LOCAL_DATE_TIME.format(localDateTimeZoned) + '"'));
        assertEquals(localDateTimeZoned, JSON.readJson(LocalDateTime.class, '"' + DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss.SSS").format(localDateTimeZoned) + '"'));
    }


    @Test
    public void localDateOfDifferentFormatsCanBeDeserialized() {
        assertEquals(localDateZoned, JSON.readJson(LocalDate.class, '"' + RFC_1123_DATE_TIME.format(zonedDateTime) + '"'));
        assertEquals(localDateZoned, JSON.readJson(LocalDate.class, '"' + RFC_1123_DATE_TIME.format(offsetDateTime) + '"'));
        assertEquals(localDateZoned, JSON.readJson(LocalDate.class, '"' + ISO_ZONED_DATE_TIME.format(zonedDateTime) + '"'));
        assertEquals(localDateZoned, JSON.readJson(LocalDate.class, '"' + ISO_OFFSET_DATE_TIME.format(offsetDateTime) + '"'));
        assertEquals(localDateZoned, JSON.readJson(LocalDate.class, '"' + ISO_LOCAL_DATE_TIME.format(localDateTimeZoned) + '"'));
        assertEquals(localDateZoned, JSON.readJson(LocalDate.class, '"' + ISO_LOCAL_DATE.format(localDateTimeZoned) + '"'));
        assertEquals(localDateZoned, JSON.readJson(LocalDate.class, '"' + ISO_LOCAL_DATE.format(localDateZoned) + '"'));
    }


    @Test
    public void localTimeOfDifferentFormatsCanBeDeserialized() {
        assertEquals(localTimeZoned, JSON.readJson(LocalTime.class, '"' + RFC_1123_DATE_TIME.format(zonedDateTime) + '"'));
        assertEquals(localTimeZoned, JSON.readJson(LocalTime.class, '"' + RFC_1123_DATE_TIME.format(offsetDateTime) + '"'));
        assertEquals(localTimeZoned, JSON.readJson(LocalTime.class, '"' + ISO_ZONED_DATE_TIME.format(zonedDateTime) + '"'));
        assertEquals(localTimeZoned, JSON.readJson(LocalTime.class, '"' + ISO_OFFSET_DATE_TIME.format(offsetDateTime) + '"'));
        assertEquals(localTimeZoned, JSON.readJson(LocalTime.class, '"' + ISO_LOCAL_DATE_TIME.format(localDateTimeZoned) + '"'));
        assertEquals(localTimeZoned, JSON.readJson(LocalTime.class, '"' + ISO_OFFSET_TIME.format(offsetDateTime) + '"'));
        assertEquals(localTimeZoned, JSON.readJson(LocalTime.class, '"' + ISO_LOCAL_TIME.format(localDateTimeZoned) + '"'));
        assertEquals(localTimeZoned, JSON.readJson(LocalTime.class, '"' + ISO_LOCAL_TIME.format(localTimeZoned) + '"'));
    }


    @Test
    public void offsetDateTimeOfDifferentFormatsCanBeDeserialized() {
        assertNormalizedEquals(offsetDateTime, JSON.readJson(OffsetDateTime.class, '"' + RFC_1123_DATE_TIME.format(zonedDateTime) + '"'));
        assertNormalizedEquals(offsetDateTime, JSON.readJson(OffsetDateTime.class, '"' + RFC_1123_DATE_TIME.format(offsetDateTime) + '"'));
        assertNormalizedEquals(offsetDateTime, JSON.readJson(OffsetDateTime.class, '"' + ISO_ZONED_DATE_TIME.format(zonedDateTime) + '"'));
        assertNormalizedEquals(offsetDateTime, JSON.readJson(OffsetDateTime.class, '"' + ISO_OFFSET_DATE_TIME.format(offsetDateTime) + '"'));
        assertNormalizedEquals(offsetDateTime, JSON.readJson(OffsetDateTime.class, '"' + ISO_LOCAL_DATE_TIME.format(localDateTimeZoned) + '"'));
    }

    private static void assertNormalizedEquals(@NotNull OffsetDateTime expected, @Nullable OffsetDateTime actual) {
        assertNotNull(actual);
        ZoneId zoneId = ZoneId.systemDefault();
        assertEquals(
            expected.atZoneSameInstant(zoneId),
            actual.atZoneSameInstant(zoneId)
        );
    }


    @Test
    public void offsetTimeOfDifferentFormatsCanBeDeserialized() {
        assertNormalizedEquals(offsetTime, JSON.readJson(OffsetTime.class, '"' + RFC_1123_DATE_TIME.format(zonedDateTime) + '"'));
        assertNormalizedEquals(offsetTime, JSON.readJson(OffsetTime.class, '"' + RFC_1123_DATE_TIME.format(offsetDateTime) + '"'));
        assertNormalizedEquals(offsetTime, JSON.readJson(OffsetTime.class, '"' + ISO_ZONED_DATE_TIME.format(zonedDateTime) + '"'));
        assertNormalizedEquals(offsetTime, JSON.readJson(OffsetTime.class, '"' + ISO_OFFSET_DATE_TIME.format(offsetDateTime) + '"'));
        assertNormalizedEquals(offsetTime, JSON.readJson(OffsetTime.class, '"' + ISO_OFFSET_TIME.format(offsetDateTime) + '"'));
        assertNormalizedEquals(offsetTime, JSON.readJson(OffsetTime.class, '"' + ISO_OFFSET_TIME.format(offsetTime) + '"'));
        assertNormalizedEquals(offsetTime, JSON.readJson(OffsetTime.class, '"' + ISO_LOCAL_DATE_TIME.format(localDateTimeZoned) + '"'));
        assertNormalizedEquals(offsetTime, JSON.readJson(OffsetTime.class, '"' + ISO_LOCAL_TIME.format(localDateTimeZoned) + '"'));
    }

    private static void assertNormalizedEquals(@NotNull OffsetTime expected, @Nullable OffsetTime actual) {
        assertNotNull(actual);
        ZoneOffset zoneOffset = ZoneId.systemDefault().getRules().getOffset(Instant.now());
        assertEquals(
            expected.withOffsetSameInstant(zoneOffset),
            actual.withOffsetSameInstant(zoneOffset)
        );
    }


    @Test
    public void zonedDateTimeOfDifferentFormatsCanBeDeserialized() {
        assertNormalizedEquals(zonedDateTime, JSON.readJson(ZonedDateTime.class, '"' + RFC_1123_DATE_TIME.format(zonedDateTime) + '"'));
        assertNormalizedEquals(zonedDateTime, JSON.readJson(ZonedDateTime.class, '"' + ISO_ZONED_DATE_TIME.format(zonedDateTime) + '"'));
        assertNormalizedEquals(zonedDateTime, JSON.readJson(ZonedDateTime.class, '"' + DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss.SSS' 'z").format(zonedDateTime) + '"'));
        assertNormalizedEquals(zonedDateTime, JSON.readJson(ZonedDateTime.class, '"' + DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss.SSSz").format(zonedDateTime) + '"'));
        assertNormalizedEquals(zonedDateTime, JSON.readJson(ZonedDateTime.class, '"' + DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss.SSS' 'zzzz").format(zonedDateTime) + '"'));
        assertNormalizedEquals(zonedDateTime, JSON.readJson(ZonedDateTime.class, '"' + DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss.SSSzzzz").format(zonedDateTime) + '"'));
        assertNormalizedEquals(localDateTimeZoned.atZone(ZoneId.systemDefault()), JSON.readJson(ZonedDateTime.class, '"' + ISO_LOCAL_DATE_TIME.format(localDateTimeZoned) + '"'));
    }

    private static void assertNormalizedEquals(@NotNull ZonedDateTime expected, @Nullable ZonedDateTime actual) {
        assertNotNull(actual);
        assertNormalizedEquals(
            expected.toOffsetDateTime(),
            actual.toOffsetDateTime()
        );
    }

}
