package name.remal.json.internal.configurers;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static com.fasterxml.jackson.databind.DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS;
import static com.fasterxml.jackson.databind.DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE;
import static com.fasterxml.jackson.databind.MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS;
import static com.fasterxml.jackson.databind.MapperFeature.PROPAGATE_TRANSIENT_MARKER;
import static com.fasterxml.jackson.databind.MapperFeature.SORT_PROPERTIES_ALPHABETICALLY;
import static com.fasterxml.jackson.databind.SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATE_KEYS_AS_TIMESTAMPS;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS;
import static java.util.Collections.singletonList;
import static name.remal.Services.loadServices;
import static name.remal.Services.loadServicesList;
import static name.remal.log.LogUtils.logDebug;
import static name.remal.log.LogUtils.logError;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceConfigurationError;
import java.util.TimeZone;
import name.remal.gradle_plugins.api.AutoService;
import name.remal.json.api.ModuleCondition;
import name.remal.json.api.ObjectMapperConfigurer;
import org.jetbrains.annotations.NotNull;

@AutoService(ObjectMapperConfigurer.class)
public class DefaultObjectMapperConfigurer implements ObjectMapperConfigurer {

    private static final ClassLoader CLASS_LOADER = DefaultObjectMapperConfigurer.class.getClassLoader();

    private static final Collection<String> PRIORITIZED_MODULE_CLASSES = singletonList(
        "com.fasterxml.jackson.datatype.jsr310.JavaTimeModule"
    );

    private static final List<ModuleCondition> MODULE_CONDITIONS = loadServicesList(ModuleCondition.class);

    @Override
    public void configure(@NotNull ObjectMapper objectMapper) {
        objectMapper.enable(PROPAGATE_TRANSIENT_MARKER);
        objectMapper.enable(SORT_PROPERTIES_ALPHABETICALLY);
        objectMapper.enable(ACCEPT_CASE_INSENSITIVE_ENUMS);

        objectMapper.disable(WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.disable(WRITE_DATE_KEYS_AS_TIMESTAMPS);
        objectMapper.enable(ORDER_MAP_ENTRIES_BY_KEYS);

        objectMapper.disable(FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.enable(ACCEPT_SINGLE_VALUE_AS_ARRAY);
        objectMapper.enable(READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE);
        objectMapper.disable(WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS);
        objectMapper.configure(READ_DATE_TIMESTAMPS_AS_NANOSECONDS, objectMapper.isEnabled(WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS));

        objectMapper.setTimeZone(TimeZone.getDefault());
        objectMapper.setDefaultPropertyInclusion(NON_NULL);

        loadModules(objectMapper);
    }

    @SuppressWarnings("ConstantConditions")
    private void loadModules(@NotNull ObjectMapper objectMapper) {
        for (String moduleClassName : PRIORITIZED_MODULE_CLASSES) {
            try {
                Class<?> javaTimeModuleClass = CLASS_LOADER.loadClass(moduleClassName);
                Module javaTimeModule = (Module) javaTimeModuleClass.newInstance();
                logDebug(this.getClass(), "Loading module: " + javaTimeModule.getClass().getName());
                loadModule(objectMapper, javaTimeModule);
            } catch (@NotNull ClassNotFoundException | LinkageError e) {
                // do nothing
            } catch (Throwable throwable) {
                logError(this.getClass(), throwable.getMessage(), throwable);
            }
        }

        Iterator<Module> modulesIterator = loadServices(Module.class, CLASS_LOADER).iterator();
        while (true) {
            try {
                if (!modulesIterator.hasNext()) break;
                Module module = modulesIterator.next();
                logDebug(this.getClass(), "Loading module: " + module.getClass().getName());
                loadModule(objectMapper, module);

            } catch (Throwable throwable) {
                if (throwable instanceof LinkageError || throwable instanceof ClassNotFoundException || throwable instanceof ServiceConfigurationError) {
                    logDebug(this.getClass(), throwable.getMessage(), throwable);
                } else {
                    logError(this.getClass(), throwable.getMessage(), throwable);
                }
            }
        }
    }

    @SuppressWarnings("ConstantConditions")
    private void loadModule(@NotNull ObjectMapper objectMapper, @NotNull Module module) {
        try {
            boolean canBeRegistered = true;
            for (ModuleCondition moduleCondition : MODULE_CONDITIONS) {
                if (!moduleCondition.canModuleBeRegistered(module)) {
                    canBeRegistered = false;
                    break;
                }
            }

            if (canBeRegistered) {
                objectMapper.registerModule(module);
            }

        } catch (Throwable throwable) {
            if (throwable instanceof LinkageError || throwable instanceof ClassNotFoundException || throwable instanceof ServiceConfigurationError) {
                logDebug(this.getClass(), throwable.getMessage(), throwable);
            } else {
                logError(this.getClass(), throwable.getMessage(), throwable);
            }
        }
    }

}
