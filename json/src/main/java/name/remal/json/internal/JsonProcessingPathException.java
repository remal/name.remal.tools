package name.remal.json.internal;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class JsonProcessingPathException extends JsonProcessingException {

    public JsonProcessingPathException(@NotNull String path, @NotNull Throwable rootCause) {
        super("Error processing JSON: " + path, rootCause);
    }

    @Nullable
    @Override
    public Object getProcessor() {
        Throwable rootCause = getCause();
        if (rootCause instanceof JsonProcessingException) {
            return ((JsonProcessingException) rootCause).getProcessor();
        }
        return null;
    }

}
