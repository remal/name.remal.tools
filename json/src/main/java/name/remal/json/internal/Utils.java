package name.remal.json.internal;

import static name.remal.Services.loadServicesList;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import name.remal.json.api.JsonFactoryConfigurer;
import name.remal.json.api.ObjectMapperConfigurer;
import org.jetbrains.annotations.NotNull;

public class Utils {

    private static final List<JsonFactoryConfigurer> JSON_FACTORY_CONFIGURERS = loadServicesList(JsonFactoryConfigurer.class);

    @NotNull
    public static <T extends JsonFactory> T configure(@NotNull T jsonFactory) {
        for (JsonFactoryConfigurer configurer : JSON_FACTORY_CONFIGURERS) {
            configurer.configure(jsonFactory);
        }
        return jsonFactory;
    }

    private static final List<ObjectMapperConfigurer> OBJECT_MAPPER_CONFIGURERS = loadServicesList(ObjectMapperConfigurer.class);

    @NotNull
    public static <T extends ObjectMapper> T configure(@NotNull T objectMapper) {
        for (ObjectMapperConfigurer configurer : OBJECT_MAPPER_CONFIGURERS) {
            configurer.configure(objectMapper);
        }
        return objectMapper;
    }

    public static boolean isClassExists(@NotNull ClassLoader classLoader, @NotNull String className) {
        try {
            Class.forName(className, false, classLoader);
            return true;
        } catch (@NotNull LinkageError | ClassNotFoundException e) {
            return false;
        }
    }

}
