package name.remal.json.internal.modules;

import static java.lang.Math.ceil;
import static java.lang.Math.floor;
import static java.lang.Math.round;
import static java.lang.Math.toIntExact;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_TIME;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_TIME;
import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;
import static java.time.format.DateTimeFormatter.RFC_1123_DATE_TIME;
import static java.time.format.TextStyle.FULL;
import static java.time.format.TextStyle.SHORT;
import static java.time.temporal.ChronoUnit.CENTURIES;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.DECADES;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MICROS;
import static java.time.temporal.ChronoUnit.MILLENNIA;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.MONTHS;
import static java.time.temporal.ChronoUnit.NANOS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.time.temporal.ChronoUnit.WEEKS;
import static java.time.temporal.ChronoUnit.YEARS;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableSortedMap;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import name.remal.gradle_plugins.api.AutoService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@AutoService(Module.class)
public class DateTimeFormatsModule extends SimpleModule {

    private static final long serialVersionUID = 1L;

    @Override
    public void setupModule(@NotNull SetupContext context) {
        super.setupModule(context);

        context.addDeserializationProblemHandler(new DateTimeFormatsDeserializationProblemHandler());
    }


    @SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
    public static class DateTimeFormatsDeserializationProblemHandler extends DeserializationProblemHandler {

        @Override
        public Object handleWeirdKey(@NotNull DeserializationContext ctxt, @NotNull Class<?> rawKeyType, @NotNull String keyValue, @NotNull String failureMsg) {
            return handleWeirdString(ctxt, rawKeyType, keyValue);
        }

        @Override
        public Object handleWeirdStringValue(@NotNull DeserializationContext ctxt, @NotNull Class<?> targetType, @NotNull String valueToConvert, @NotNull String failureMsg) {
            return handleWeirdString(ctxt, targetType, valueToConvert);
        }


        private static final long NANOS_PER_SECOND = 1000_000_000L;

        @NotNull
        private static final SortedMap<String, Duration> DURATION_SUFFIXES;

        static {
            SortedMap<String, Duration> durationSuffixes = new TreeMap<>((s1, s2) -> {
                int result = Integer.compare(s1.length(), s2.length());
                if (result == 0) result = s1.compareTo(s2);
                return -1 * result;
            });

            durationSuffixes.put("ns", NANOS.getDuration());
            durationSuffixes.put("nano", NANOS.getDuration());
            durationSuffixes.put("nanos", NANOS.getDuration());
            durationSuffixes.put("nanosec", NANOS.getDuration());
            durationSuffixes.put("nanosecs", NANOS.getDuration());
            durationSuffixes.put("nanosecond", NANOS.getDuration());
            durationSuffixes.put("nanoseconds", NANOS.getDuration());

            durationSuffixes.put("us", MICROS.getDuration());
            durationSuffixes.put("micro", MICROS.getDuration());
            durationSuffixes.put("micros", MICROS.getDuration());
            durationSuffixes.put("microsec", MICROS.getDuration());
            durationSuffixes.put("microsecs", MICROS.getDuration());
            durationSuffixes.put("microsecond", MICROS.getDuration());
            durationSuffixes.put("microseconds", MICROS.getDuration());

            durationSuffixes.put("ms", MILLIS.getDuration());
            durationSuffixes.put("milli", MILLIS.getDuration());
            durationSuffixes.put("millis", MILLIS.getDuration());
            durationSuffixes.put("millisec", MILLIS.getDuration());
            durationSuffixes.put("millisecs", MILLIS.getDuration());
            durationSuffixes.put("millisecond", MILLIS.getDuration());
            durationSuffixes.put("milliseconds", MILLIS.getDuration());

            durationSuffixes.put("s", SECONDS.getDuration());
            durationSuffixes.put("sec", SECONDS.getDuration());
            durationSuffixes.put("secs", SECONDS.getDuration());
            durationSuffixes.put("second", SECONDS.getDuration());
            durationSuffixes.put("seconds", SECONDS.getDuration());

            durationSuffixes.put("m", MINUTES.getDuration());
            durationSuffixes.put("min", MINUTES.getDuration());
            durationSuffixes.put("mins", MINUTES.getDuration());
            durationSuffixes.put("minute", MINUTES.getDuration());
            durationSuffixes.put("minutes", MINUTES.getDuration());

            durationSuffixes.put("h", HOURS.getDuration());
            durationSuffixes.put("hour", HOURS.getDuration());
            durationSuffixes.put("hours", HOURS.getDuration());

            durationSuffixes.put("d", DAYS.getDuration());
            durationSuffixes.put("day", DAYS.getDuration());
            durationSuffixes.put("days", DAYS.getDuration());

            durationSuffixes.put("w", WEEKS.getDuration());
            durationSuffixes.put("week", WEEKS.getDuration());
            durationSuffixes.put("weeks", WEEKS.getDuration());

            durationSuffixes.put("month", MONTHS.getDuration());
            durationSuffixes.put("months", MONTHS.getDuration());

            durationSuffixes.put("y", YEARS.getDuration());
            durationSuffixes.put("year", YEARS.getDuration());
            durationSuffixes.put("years", YEARS.getDuration());

            durationSuffixes.put("decade", DECADES.getDuration());
            durationSuffixes.put("decades", DECADES.getDuration());

            durationSuffixes.put("century", CENTURIES.getDuration());
            durationSuffixes.put("centuries", CENTURIES.getDuration());

            durationSuffixes.put("millennia", MILLENNIA.getDuration());
            durationSuffixes.put("millennias", MILLENNIA.getDuration());

            DURATION_SUFFIXES = unmodifiableSortedMap(durationSuffixes);
        }

        @Nullable
        private static Duration parseDuration(@NotNull String value) {
            value = value.toLowerCase();
            for (Entry<String, Duration> entry : DURATION_SUFFIXES.entrySet()) {
                String suffix = entry.getKey();
                Duration suffixDuration = entry.getValue();
                if (value.endsWith(suffix)) {
                    value = value.substring(0, value.length() - suffix.length()).trim();
                    try {
                        if (!value.contains(".")) {
                            long amount = Long.parseLong(value);
                            if (amount == 1) {
                                return suffixDuration;
                            } else {
                                return Duration.ofSeconds(amount * suffixDuration.getSeconds(), amount * suffixDuration.getNano());
                            }
                        } else {
                            final long longSeconds;
                            final long longNanos;
                            double amount = Double.parseDouble(value);
                            if (amount >= 0.0) {
                                double seconds = amount * suffixDuration.getSeconds();
                                longSeconds = round(floor(seconds));
                                double nanos = amount * suffixDuration.getNano() + (seconds - longSeconds) * NANOS_PER_SECOND;
                                longNanos = round(floor(nanos));
                            } else {
                                double seconds = amount * suffixDuration.getSeconds();
                                longSeconds = -round(ceil(seconds));
                                double nanos = amount * suffixDuration.getNano() + (seconds - longSeconds) * NANOS_PER_SECOND;
                                longNanos = round(ceil(nanos));
                            }
                            return Duration.ofSeconds(longSeconds, longNanos);
                        }
                    } catch (Exception ignored) {
                        return null;
                    }
                }
            }
            return null;
        }

        private static final long SECONDS_PER_YEAR = YEARS.getDuration().getSeconds();
        private static final long SECONDS_PER_MONTH = MONTHS.getDuration().getSeconds();
        private static final long SECONDS_PER_DAY = 24 * 3600;

        @NotNull
        private static final List<DateTimeFormatter> ZONED_DATE_TIME_FORMATTERS;

        static {
            List<DateTimeFormatter> zonedDateTimeFormatters = new ArrayList<>();
            zonedDateTimeFormatters.add(RFC_1123_DATE_TIME);
            zonedDateTimeFormatters.add(ISO_ZONED_DATE_TIME);

            {
                DateTimeFormatter baseFormatter = new DateTimeFormatterBuilder()
                    .append(ISO_LOCAL_DATE)
                    .appendLiteral(' ')
                    .append(ISO_LOCAL_TIME)
                    .optionalStart()
                    .appendLiteral(' ')
                    .toFormatter();

                zonedDateTimeFormatters.add(new DateTimeFormatterBuilder().append(baseFormatter).optionalStart().appendZoneOrOffsetId().toFormatter());
                zonedDateTimeFormatters.add(new DateTimeFormatterBuilder().append(baseFormatter).optionalStart().appendZoneId().toFormatter());
                zonedDateTimeFormatters.add(new DateTimeFormatterBuilder().append(baseFormatter).optionalStart().appendZoneText(FULL).toFormatter());
                zonedDateTimeFormatters.add(new DateTimeFormatterBuilder().append(baseFormatter).optionalStart().appendZoneText(SHORT).toFormatter());
            }

            ZONED_DATE_TIME_FORMATTERS = unmodifiableList(zonedDateTimeFormatters);
        }

        @NotNull
        private static final List<DateTimeFormatter> LOCAL_DATE_TIME_FORMATTERS = unmodifiableList(asList(
            ISO_LOCAL_DATE_TIME,
            new DateTimeFormatterBuilder()
                .append(ISO_LOCAL_DATE)
                .appendLiteral(' ')
                .append(ISO_LOCAL_TIME)
                .toFormatter()
        ));

        @NotNull
        private static final List<DateTimeFormatter> OFFSET_TIME_FORMATTERS = unmodifiableList(asList(
            ISO_OFFSET_TIME
        ));

        @NotNull
        private static final List<DateTimeFormatter> LOCAL_DATE_FORMATTERS = unmodifiableList(asList(
            ISO_LOCAL_DATE
        ));

        @NotNull
        private static final List<DateTimeFormatter> LOCAL_TIME_FORMATTERS = unmodifiableList(asList(
            ISO_LOCAL_TIME
        ));

        @Nullable
        private Object handleWeirdString(@NotNull DeserializationContext ctxt, @NotNull Class<?> targetType, @Nullable String value) {
            if (value == null) return NOT_HANDLED;
            value = value.trim();
            if (value.isEmpty()) return NOT_HANDLED;

            if (Duration.class.isAssignableFrom(targetType)) {
                Duration duration = parseDuration(value);
                if (duration != null) return duration;

            } else if (Period.class.isAssignableFrom(targetType)) {
                Duration duration = parseDuration(value);
                if (duration == null) return NOT_HANDLED;
                try {
                    long seconds = duration.getSeconds();
                    long years = seconds / SECONDS_PER_YEAR;
                    seconds -= years * SECONDS_PER_YEAR;
                    long months = seconds / SECONDS_PER_MONTH;
                    seconds -= months * SECONDS_PER_MONTH;
                    long days = seconds / SECONDS_PER_DAY;
                    return Period.of(toIntExact(years), toIntExact(months), toIntExact(days));
                } catch (Exception ignored) {
                    return NOT_HANDLED;
                }
            }

            {
                ZonedDateTime zonedDateTime = null;
                for (DateTimeFormatter formatter : ZONED_DATE_TIME_FORMATTERS) {
                    try {
                        zonedDateTime = ZonedDateTime.from(formatter.parse(value));
                        break;
                    } catch (Exception e) {
                        // do nothing
                    }
                }
                if (zonedDateTime != null) {
                    if (LocalDateTime.class.isAssignableFrom(targetType)) {
                        return LocalDateTime.ofInstant(zonedDateTime.toInstant(), ctxt.getTimeZone().toZoneId());
                    } else if (LocalDate.class.isAssignableFrom(targetType)) {
                        return LocalDateTime.ofInstant(zonedDateTime.toInstant(), ctxt.getTimeZone().toZoneId()).toLocalDate();
                    } else if (LocalTime.class.isAssignableFrom(targetType)) {
                        return LocalDateTime.ofInstant(zonedDateTime.toInstant(), ctxt.getTimeZone().toZoneId()).toLocalTime();
                    } else if (OffsetDateTime.class.isAssignableFrom(targetType)) {
                        return zonedDateTime.toOffsetDateTime();
                    } else if (OffsetTime.class.isAssignableFrom(targetType)) {
                        return zonedDateTime.toOffsetDateTime().toOffsetTime();
                    } else if (ZonedDateTime.class.isAssignableFrom(targetType)) {
                        return zonedDateTime;
                    }
                }
            }

            {
                LocalDateTime localDateTime = null;
                for (DateTimeFormatter formatter : LOCAL_DATE_TIME_FORMATTERS) {
                    try {
                        localDateTime = LocalDateTime.from(formatter.parse(value));
                        break;
                    } catch (Exception e) {
                        // do nothing
                    }
                }
                if (localDateTime != null) {
                    if (LocalDateTime.class.isAssignableFrom(targetType)) {
                        return localDateTime;
                    } else if (LocalDate.class.isAssignableFrom(targetType)) {
                        return localDateTime.toLocalDate();
                    } else if (LocalTime.class.isAssignableFrom(targetType)) {
                        return localDateTime.toLocalTime();
                    } else if (OffsetDateTime.class.isAssignableFrom(targetType)) {
                        return OffsetDateTime.of(localDateTime, ctxt.getTimeZone().toZoneId().getRules().getOffset(Instant.now()));
                    } else if (OffsetTime.class.isAssignableFrom(targetType)) {
                        return OffsetDateTime.of(localDateTime, ctxt.getTimeZone().toZoneId().getRules().getOffset(Instant.now())).toOffsetTime();
                    } else if (ZonedDateTime.class.isAssignableFrom(targetType)) {
                        return ZonedDateTime.of(localDateTime, ctxt.getTimeZone().toZoneId());
                    }
                }
            }

            {
                OffsetTime offsetTime = null;
                for (DateTimeFormatter formatter : OFFSET_TIME_FORMATTERS) {
                    try {
                        offsetTime = OffsetTime.from(formatter.parse(value));
                        break;
                    } catch (Exception e) {
                        // do nothing
                    }
                }
                if (offsetTime != null) {
                    if (LocalTime.class.isAssignableFrom(targetType)) {
                        int offsetSeconds = ctxt.getTimeZone().toZoneId().getRules().getOffset(Instant.now()).getTotalSeconds();
                        offsetSeconds -= offsetTime.getOffset().getTotalSeconds();
                        return offsetTime.toLocalTime().plus(offsetSeconds, SECONDS);
                    } else if (OffsetTime.class.isAssignableFrom(targetType)) {
                        return offsetTime;
                    }
                }
            }

            {
                LocalDate localDate = null;
                for (DateTimeFormatter formatter : LOCAL_DATE_FORMATTERS) {
                    try {
                        localDate = LocalDate.from(formatter.parse(value));
                        break;
                    } catch (Exception e) {
                        // do nothing
                    }
                }
                if (localDate != null) {
                    if (LocalDate.class.isAssignableFrom(targetType)) {
                        return localDate;
                    }
                }
            }

            {
                LocalTime localTime = null;
                for (DateTimeFormatter formatter : LOCAL_TIME_FORMATTERS) {
                    try {
                        localTime = LocalTime.from(formatter.parse(value));
                        break;
                    } catch (Exception e) {
                        // do nothing
                    }
                }
                if (localTime != null) {
                    if (LocalTime.class.isAssignableFrom(targetType)) {
                        return localTime;
                    } else if (OffsetTime.class.isAssignableFrom(targetType)) {
                        return OffsetTime.of(localTime, ctxt.getTimeZone().toZoneId().getRules().getOffset(Instant.now()));
                    }
                }
            }

            return NOT_HANDLED;
        }

    }

}
