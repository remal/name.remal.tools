package name.remal.json.internal;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class JsonGeneratingPathException extends JsonGenerationException {

    @SuppressWarnings("deprecation")
    public JsonGeneratingPathException(@NotNull String path, @NotNull Throwable rootCause) {
        super("Error mapping JSON: " + path, rootCause);
    }

    @Nullable
    @Override
    public JsonGenerator getProcessor() {
        Throwable rootCause = getCause();
        if (rootCause instanceof JsonGenerationException) {
            return ((JsonGenerationException) rootCause).getProcessor();
        }
        return null;
    }

}
