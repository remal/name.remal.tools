package name.remal.json.internal;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class JsonParsePathException extends JsonParseException {

    public JsonParsePathException(@NotNull String path, @NotNull Throwable rootCause) {
        super(null, "Error processing JSON: " + path, rootCause);
    }

    @Nullable
    @Override
    public JsonParser getProcessor() {
        Throwable rootCause = getCause();
        if (rootCause instanceof JsonParseException) {
            return ((JsonParseException) rootCause).getProcessor();
        }
        return null;
    }

}
