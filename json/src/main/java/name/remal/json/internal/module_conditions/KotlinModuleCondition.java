package name.remal.json.internal.module_conditions;

import static name.remal.json.internal.Utils.isClassExists;

import com.fasterxml.jackson.databind.Module;
import name.remal.gradle_plugins.api.AutoService;
import name.remal.json.api.ModuleCondition;
import org.jetbrains.annotations.NotNull;

@AutoService(ModuleCondition.class)
public class KotlinModuleCondition implements ModuleCondition {

    @Override
    public boolean canModuleBeRegistered(@NotNull Module module) {
        Class<?> moduleClass = module.getClass();
        if ("com.fasterxml.jackson.module.kotlin.KotlinModule".equals(moduleClass.getName())) {
            if (!isClassExists(moduleClass.getClassLoader(), "kotlin.Metadata")) return false;
            if (!isClassExists(moduleClass.getClassLoader(), "kotlin.Pair")) return false;
        }
        return true;
    }

}
