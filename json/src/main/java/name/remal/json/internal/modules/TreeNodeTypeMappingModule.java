package name.remal.json.internal.modules;

import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import name.remal.gradle_plugins.api.AutoService;

@AutoService(Module.class)
public class TreeNodeTypeMappingModule extends SimpleModule {

    private static final long serialVersionUID = 1L;

    {
        addAbstractTypeMapping(TreeNode.class, JsonNode.class);
    }

}
