package name.remal.json.internal.configurers;

import static com.fasterxml.jackson.core.JsonFactory.Feature.INTERN_FIELD_NAMES;
import static com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_COMMENTS;
import static com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_SINGLE_QUOTES;
import static com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_TRAILING_COMMA;
import static com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES;

import com.fasterxml.jackson.core.JsonFactory;
import name.remal.gradle_plugins.api.AutoService;
import name.remal.json.api.JsonFactoryConfigurer;
import org.jetbrains.annotations.NotNull;

@AutoService(JsonFactoryConfigurer.class)
public class DefaultJsonFactoryConfigurer implements JsonFactoryConfigurer {

    @Override
    public void configure(@NotNull JsonFactory jsonFactory) {
        jsonFactory.disable(INTERN_FIELD_NAMES);

        jsonFactory.enable(ALLOW_COMMENTS);
        jsonFactory.enable(ALLOW_UNQUOTED_FIELD_NAMES);
        jsonFactory.enable(ALLOW_SINGLE_QUOTES);
        jsonFactory.enable(ALLOW_TRAILING_COMMA);
    }

}
