package name.remal.json.internal;

import static name.remal.Services.loadServicesList;

import java.util.List;
import name.remal.json.api.DataFormat;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataFormats {

    @NotNull
    public static final List<DataFormat> DATA_FORMATS = loadServicesList(DataFormat.class);

    @NotNull
    public static final DataFormat JSON_DATA_FORMAT = getJsonDataFormat();

    @NotNull
    private static DataFormat getJsonDataFormat() {
        for (DataFormat dataFormat : DATA_FORMATS) {
            if ("json".equals(dataFormat.getFileExtension())) {
                return dataFormat;
            }
        }
        throw new IllegalStateException(DataFormat.class.getSimpleName() + " can't be found for *.json files");
    }

    @NotNull
    public static DataFormat getDataFormat(@Nullable String path) {
        if (path != null) {
            int pos;
            pos = path.lastIndexOf('?');
            if (0 <= pos) path = path.substring(0, pos);

            pos = path.lastIndexOf('#');
            if (0 <= pos) path = path.substring(0, pos);

            pos = path.lastIndexOf('.');
            if (0 <= pos) {
                String extension = path.substring(pos + 1).toLowerCase();
                if (!extension.isEmpty()) {
                    for (DataFormat dataFormat : DATA_FORMATS) {
                        if (dataFormat.getSupportedFileExtensions().contains(extension)) {
                            return dataFormat;
                        }
                    }
                }
            }
        }

        return JSON_DATA_FORMAT;
    }

    private DataFormats() {
    }

}
