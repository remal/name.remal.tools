package name.remal.json.internal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class JsonMappingPathException extends JsonMappingException {

    @SuppressWarnings("deprecation")
    public JsonMappingPathException(@NotNull String path, @NotNull Throwable rootCause) {
        super("Error mapping JSON: " + path, rootCause);
    }

    @Nullable
    @Override
    public Object getProcessor() {
        Throwable rootCause = getCause();
        if (rootCause instanceof JsonProcessingException) {
            return ((JsonProcessingException) rootCause).getProcessor();
        }
        return null;
    }

}
