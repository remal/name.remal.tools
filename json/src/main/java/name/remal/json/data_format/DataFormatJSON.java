package name.remal.json.data_format;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static name.remal.json.internal.Utils.configure;
import static name.remal.log.LogUtils.logDebug;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.core.util.Instantiatable;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import name.remal.gradle_plugins.api.AutoService;
import name.remal.json.api.DataFormat;
import org.jetbrains.annotations.NotNull;

@AutoService(DataFormat.class)
public class DataFormatJSON implements DataFormat {

    @NotNull
    public static final DataFormat JSON_DATA_FORMAT = new DataFormatJSON();

    @NotNull
    private static final JsonFactory JSON_FACTORY;

    @NotNull
    private static final ObjectMapper OBJECT_MAPPER;

    static {
        logDebug(DataFormatJSON.class, "Configuring JsonFactory");
        JSON_FACTORY = configure(new JsonFactory());
        logDebug(DataFormatJSON.class, "Configuring ObjectMapper");
        OBJECT_MAPPER = configure(new ObjectMapper(JSON_FACTORY));

        OBJECT_MAPPER.setDefaultPrettyPrinter(new CustomJSONPrettyPrinter());
    }

    @NotNull
    @Override
    public ObjectMapper getObjectMapper() {
        return OBJECT_MAPPER;
    }

    @NotNull
    private static final List<@NotNull String> MIME_TYPES = unmodifiableList(asList(
        "application/json",
        "application/x-json",
        "application/javascript",
        "application/x-javascript",
        "text/json",
        "text/x-json",
        "text/javascript",
        "text/x-javascript"
    ));

    @NotNull
    @Override
    public List<@NotNull String> getSupportedMimeTypes() {
        return MIME_TYPES;
    }

    @NotNull
    private static final List<@NotNull String> FILE_EXTENSIONS = unmodifiableList(asList(
        "json",
        "js"
    ));

    @NotNull
    @Override
    public List<@NotNull String> getSupportedFileExtensions() {
        return FILE_EXTENSIONS;
    }

}


class CustomJSONPrettyPrinter implements PrettyPrinter, Instantiatable<CustomJSONPrettyPrinter>, Serializable {

    private static final long serialVersionUID = 1;

    @NotNull
    @Override
    public CustomJSONPrettyPrinter createInstance() {
        return new CustomJSONPrettyPrinter();
    }

    private static final String INDENT = "    ";

    private transient int depth = 0;

    private void writeIndent(@NotNull JsonGenerator gen) throws IOException {
        for (int i = 0; i < depth; ++i) {
            gen.writeRaw(INDENT);
        }
    }

    @Override
    public void writeRootValueSeparator(@NotNull JsonGenerator gen) throws IOException {
        gen.writeRaw("\n");
    }

    @Override
    public void writeStartObject(@NotNull JsonGenerator gen) throws IOException {
        gen.writeRaw("{");
        ++depth;
    }

    @Override
    public void beforeObjectEntries(@NotNull JsonGenerator gen) throws IOException {
        gen.writeRaw("\n");
        writeIndent(gen);
    }

    @Override
    public void writeObjectFieldValueSeparator(@NotNull JsonGenerator gen) throws IOException {
        gen.writeRaw(": ");
    }

    @Override
    public void writeObjectEntrySeparator(@NotNull JsonGenerator gen) throws IOException {
        gen.writeRaw(",\n");
        writeIndent(gen);
    }

    @Override
    public void writeEndObject(@NotNull JsonGenerator gen, int nrOfEntries) throws IOException {
        --depth;
        if (1 <= nrOfEntries) {
            gen.writeRaw("\n");
            writeIndent(gen);
        }
        gen.writeRaw("}");
    }

    @Override
    public void writeStartArray(@NotNull JsonGenerator gen) throws IOException {
        gen.writeRaw("[");
        ++depth;
    }

    @Override
    public void beforeArrayValues(@NotNull JsonGenerator gen) throws IOException {
        gen.writeRaw("\n");
        writeIndent(gen);
    }

    @Override
    public void writeArrayValueSeparator(@NotNull JsonGenerator gen) throws IOException {
        gen.writeRaw(",\n");
        writeIndent(gen);
    }

    @Override
    public void writeEndArray(@NotNull JsonGenerator gen, int nrOfValues) throws IOException {
        --depth;
        if (1 <= nrOfValues) {
            gen.writeRaw("\n");
            writeIndent(gen);
        }
        gen.writeRaw("]");
    }

}
