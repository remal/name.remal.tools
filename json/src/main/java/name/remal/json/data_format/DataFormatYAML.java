package name.remal.json.data_format;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static name.remal.json.internal.Utils.configure;
import static name.remal.log.LogUtils.logDebug;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import java.util.List;
import name.remal.gradle_plugins.api.AutoService;
import name.remal.json.api.DataFormat;
import org.jetbrains.annotations.NotNull;

@AutoService(DataFormat.class)
public class DataFormatYAML implements DataFormat {

    @NotNull
    public static final DataFormat YAML_DATA_FORMAT = new DataFormatYAML();

    @NotNull
    private static final YAMLFactory JSON_FACTORY;

    @NotNull
    private static final YAMLMapper OBJECT_MAPPER;

    static {
        logDebug(DataFormatJSON.class, "Configuring YAMLFactory");
        JSON_FACTORY = configure(new YAMLFactory());
        logDebug(DataFormatJSON.class, "Configuring YAMLMapper");
        OBJECT_MAPPER = configure(new YAMLMapper(JSON_FACTORY));
    }

    @NotNull
    @Override
    public ObjectMapper getObjectMapper() {
        return OBJECT_MAPPER;
    }

    @Override
    @NotNull
    public DataFormat withIndention() {
        return this;
    }

    @NotNull
    private static final List<@NotNull String> MIME_TYPES = unmodifiableList(asList(
        "text/vnd.yaml",
        "text/yaml",
        "text/x-yaml",
        "application/vnd.yaml",
        "application/yaml",
        "application/x-yaml"
    ));

    @NotNull
    @Override
    public List<@NotNull String> getSupportedMimeTypes() {
        return MIME_TYPES;
    }

    @NotNull
    private static final List<@NotNull String> FILE_EXTENSIONS = unmodifiableList(asList(
        "yaml",
        "yml"
    ));

    @NotNull
    @Override
    public List<@NotNull String> getSupportedFileExtensions() {
        return FILE_EXTENSIONS;
    }

}
