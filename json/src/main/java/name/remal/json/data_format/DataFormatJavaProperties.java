package name.remal.json.data_format;

import static java.util.Collections.singletonList;
import static name.remal.json.internal.Utils.configure;
import static name.remal.log.LogUtils.logDebug;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsFactory;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper;
import java.util.List;
import name.remal.gradle_plugins.api.AutoService;
import name.remal.json.api.DataFormat;
import org.jetbrains.annotations.NotNull;

@AutoService(DataFormat.class)
public class DataFormatJavaProperties implements DataFormat {

    @NotNull
    public static final DataFormat JAVA_PROPERTIES_DATA_FORMAT = new DataFormatJavaProperties();

    @NotNull
    private static final JavaPropsFactory JSON_FACTORY;

    @NotNull
    private static final JavaPropsMapper OBJECT_MAPPER;

    static {
        logDebug(DataFormatJSON.class, "Configuring JavaPropsFactory");
        JSON_FACTORY = configure(new JavaPropsFactory());
        logDebug(DataFormatJSON.class, "Configuring JavaPropsMapper");
        OBJECT_MAPPER = configure(new JavaPropsMapper(JSON_FACTORY));
    }

    @NotNull
    @Override
    public ObjectMapper getObjectMapper() {
        return OBJECT_MAPPER;
    }

    @Override
    @NotNull
    public DataFormat withIndention() {
        return this;
    }

    @NotNull
    private static final List<@NotNull String> MIME_TYPES = singletonList("text/plain");

    @NotNull
    @Override
    public List<@NotNull String> getSupportedMimeTypes() {
        return MIME_TYPES;
    }

    @NotNull
    private static final List<@NotNull String> FILE_EXTENSIONS = singletonList("properties");

    @NotNull
    @Override
    public List<@NotNull String> getSupportedFileExtensions() {
        return FILE_EXTENSIONS;
    }

}
