package name.remal.json.api;

import com.fasterxml.jackson.databind.Module;
import org.jetbrains.annotations.NotNull;

public interface ModuleCondition {

    boolean canModuleBeRegistered(@NotNull Module module);

}
