package name.remal.json.api;

import com.fasterxml.jackson.core.JsonFactory;
import name.remal.Ordered;
import org.jetbrains.annotations.NotNull;

public interface JsonFactoryConfigurer extends Ordered<JsonFactoryConfigurer> {

    void configure(@NotNull JsonFactory jsonFactory);

}
