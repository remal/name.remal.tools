package name.remal.json.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import name.remal.Ordered;
import org.jetbrains.annotations.NotNull;

public interface ObjectMapperConfigurer extends Ordered<ObjectMapperConfigurer> {

    void configure(@NotNull ObjectMapper objectMapper);

}
