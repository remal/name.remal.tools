package name.remal.json.api;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT;
import static java.util.TimeZone.getTimeZone;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.ZoneId;
import java.util.List;
import java.util.TimeZone;
import name.remal.Ordered;
import org.jetbrains.annotations.NotNull;

public interface DataFormat extends Ordered<DataFormat> {

    @NotNull
    ObjectMapper getObjectMapper();

    @NotNull
    default DataFormat withObjectMapper(@NotNull ObjectMapper objectMapper) {
        return new DataFormatWithObjectMapper(this, objectMapper);
    }

    @NotNull
    default DataFormat withIndention() {
        ObjectMapper objectMapper = getObjectMapper();
        if (objectMapper.isEnabled(INDENT_OUTPUT)) {
            return this;
        }
        return withObjectMapper(objectMapper.copy().enable(INDENT_OUTPUT));
    }

    @NotNull
    default DataFormat withoutIndention() {
        ObjectMapper objectMapper = getObjectMapper();
        if (!objectMapper.isEnabled(INDENT_OUTPUT)) {
            return this;
        }
        return withObjectMapper(objectMapper.copy().disable(INDENT_OUTPUT));
    }

    @NotNull
    default DataFormat withFailingOnUnknownProperties() {
        ObjectMapper objectMapper = getObjectMapper();
        if (objectMapper.isEnabled(FAIL_ON_UNKNOWN_PROPERTIES)) {
            return this;
        }
        return withObjectMapper(objectMapper.copy().enable(FAIL_ON_UNKNOWN_PROPERTIES));
    }

    @NotNull
    default DataFormat withoutFailingOnUnknownProperties() {
        ObjectMapper objectMapper = getObjectMapper();
        if (!objectMapper.isEnabled(FAIL_ON_UNKNOWN_PROPERTIES)) {
            return this;
        }
        return withObjectMapper(objectMapper.copy().disable(FAIL_ON_UNKNOWN_PROPERTIES));
    }

    @NotNull
    default DataFormat withTimeZone(@NotNull TimeZone timeZone) {
        return withObjectMapper(getObjectMapper().copy().setTimeZone(timeZone));
    }

    @NotNull
    default DataFormat withTimeZone(@NotNull ZoneId zoneId) {
        return withObjectMapper(getObjectMapper().copy().setTimeZone(getTimeZone(zoneId)));
    }

    @NotNull
    default String getMimeType() {
        return getSupportedMimeTypes().get(0);
    }

    @NotNull
    List<@NotNull String> getSupportedMimeTypes();

    @NotNull
    default String getFileExtension() {
        return getSupportedFileExtensions().get(0);
    }

    @NotNull
    List<@NotNull String> getSupportedFileExtensions();

}


class DataFormatWithObjectMapper implements DataFormat {

    @NotNull
    private static DataFormat unwrapDataFormat(@NotNull DataFormat dataFormat) {
        while (dataFormat instanceof DataFormatWithObjectMapper) {
            dataFormat = ((DataFormatWithObjectMapper) dataFormat).dataFormat;
        }
        return dataFormat;
    }


    @NotNull
    private final DataFormat dataFormat;

    @NotNull
    private final ObjectMapper objectMapper;

    public DataFormatWithObjectMapper(@NotNull DataFormat dataFormat, @NotNull ObjectMapper objectMapper) {
        this.dataFormat = unwrapDataFormat(dataFormat);
        this.objectMapper = objectMapper;
    }

    @Override
    @NotNull
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    @Override
    @NotNull
    public String getMimeType() {
        return dataFormat.getMimeType();
    }

    @Override
    @NotNull
    public List<@NotNull String> getSupportedMimeTypes() {
        return dataFormat.getSupportedMimeTypes();
    }

    @Override
    @NotNull
    public String getFileExtension() {
        return dataFormat.getFileExtension();
    }

    @Override
    @NotNull
    public List<@NotNull String> getSupportedFileExtensions() {
        return dataFormat.getSupportedFileExtensions();
    }

}
