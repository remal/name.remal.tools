package name.remal.mockito;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Answers.RETURNS_SMART_NULLS;

public final class StrictMockAnswer implements Answer<Object> {

    @NotNull
    private final Answer<?> delegate;

    public StrictMockAnswer(@Nullable Answer<?> delegate) {
        this.delegate = delegate != null ? delegate : RETURNS_SMART_NULLS;
    }

    public StrictMockAnswer() {
        this(null);
    }

    @Override
    public Object answer(@NotNull InvocationOnMock invocation) throws Throwable {
        if (isEnabled) throw new AssertionError("Unexpected invocation: " + invocation);
        return delegate.answer(invocation);
    }

    private boolean isEnabled;

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

}
