package name.remal.mockito;

import org.jetbrains.annotations.NotNull;
import org.mockito.Mockito;

import static java.lang.System.identityHashCode;

public class StrictMockito extends Mockito {

    @FunctionalInterface
    interface StrictMockConfigurer<T> {
        void configure(T mock) throws Throwable;
    }

    public <T> T strictMock(@NotNull Class<T> classToMock, @NotNull StrictMockConfigurer<T> configurer) {
        StrictMockAnswer defaultAnswer = new StrictMockAnswer();
        T mock = mock(classToMock, defaultAnswer);

        // We want toString() to be stubbed be default:
        when(mock.toString()).thenReturn(classToMock.getSimpleName() + "@" + identityHashCode(mock));

        try {
            configurer.configure(mock);
        } catch (Throwable throwable) {
            throw sneakyThrow(throwable);
        }

        defaultAnswer.setEnabled(true);

        return mock;
    }

    @NotNull
    private static RuntimeException sneakyThrow(@NotNull Throwable t) {
        StrictMockito.<RuntimeException>sneakyThrow0(t);
        return null;
    }

    @SuppressWarnings("unchecked")
    private static <T extends Throwable> void sneakyThrow0(Throwable t) throws T {
        throw (T) t;
    }

}
