package name.remal.mockito

import com.nhaarman.mockito_kotlin.KStubbing
import com.nhaarman.mockito_kotlin.mock
import org.mockito.Incubating
import org.mockito.Mockito.`when`
import org.mockito.listeners.InvocationListener
import org.mockito.mock.SerializableMode
import java.lang.System.identityHashCode
import kotlin.reflect.KClass

inline fun <reified T : Any> strictMock(
    extraInterfaces: Array<KClass<out Any>>? = null,
    name: String? = null,
    spiedInstance: Any? = null,
    serializable: Boolean = false,
    serializableMode: SerializableMode? = null,
    verboseLogging: Boolean = false,
    invocationListeners: Array<InvocationListener>? = null,
    stubOnly: Boolean = false,
    @Incubating useConstructor: Boolean = false,
    @Incubating outerInstance: Any? = null,
    stubbing: KStubbing<T>.(T) -> Unit
): T {
    val defaultAnswer = StrictMockAnswer()
    val mock = mock(
        extraInterfaces = extraInterfaces,
        name = name,
        spiedInstance = spiedInstance,
        defaultAnswer = defaultAnswer,
        serializable = serializable,
        serializableMode = serializableMode,
        verboseLogging = verboseLogging,
        invocationListeners = invocationListeners,
        stubOnly = stubOnly,
        useConstructor = useConstructor,
        outerInstance = outerInstance,
        stubbing = stubbing
    )

    // We want toString() to be stubbed be default:
    `when`(mock.toString()).thenReturn(T::class.java.simpleName + "@" + identityHashCode(mock))

    defaultAnswer.isEnabled = true
    return mock
}
